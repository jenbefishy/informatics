from turtle import *

screensize(15000, 15000)
c = 50
tracer(0)

left(90)
for _ in range(10):
    forward(30 * c)
    left(120)

up()
answer = 0
for x in range(-100, 100):
    for y in range(-100, 100):
        if (x < 0) and (y < (1 / (3 ** 0.5)) * x + 30) and \
                (y > -(1 / (3 ** 0.5)) * x):
            answer += 1

mainloop()
print(answer)
