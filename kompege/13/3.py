from turtle import *

screensize(15000, 15000)
const = 50
tracer(0)

left(90)
for _ in range(11):
    forward(4 * const)
    right(60)

up()
for x in range(1, 20):
    for y in range(1, 20):
        goto(x * const, y * const)
        dot(5, "blue")
mainloop()
