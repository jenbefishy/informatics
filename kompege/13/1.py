from turtle import *
const = 25
screensize(15000, 15000)
tracer(0)
speed(100000)

left(90)

for _ in range(45):
    forward(10 * const)
    right(8)

mainloop()
