from turtle import *

screensize(15000, 15000)
c = 25
tracer(0)

left(90)
for _ in range(7):
    goto(xcor() + 6   * c, ycor() - 9 * c)
    goto(xcor() - 6   * c, ycor() + 2 * c)
    goto(xcor() + 12  * c, ycor() + 3 * c)

up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
