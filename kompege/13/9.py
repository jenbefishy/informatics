from turtle import *
screensize(15000, 15000)
tracer(0)
const = 25

left(90)
for _ in range(10):
    goto(xcor() + 3 * const, ycor() + 6 * const)
    goto(xcor() + 7 * const, ycor() - 2 * const)
    goto(xcor() - 10 * const, ycor() - 4 * const)

up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * const, y * const)
        dot(5, "blue")

mainloop()
