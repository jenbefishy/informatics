from turtle import *

screensize(15000, 15000)
c = 50
tracer(0)
left(90)

for _ in range(2):
    goto(xcor() + 6 * c, ycor() + 2 * c)
    goto(xcor() + 0 * c, ycor() - 2 * c)

for _ in range(3):
    goto(xcor() + 2 * c, ycor() - 1 * c)
    goto(xcor() - 2 * c, ycor() - 1 * c)

for _ in range(6):
    goto(xcor() - 2 * c, ycor() + 1 * c)

up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()

