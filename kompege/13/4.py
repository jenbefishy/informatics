from turtle import *

screensize(15000, 15000)
tracer(0)
const = 25

left(90)
for _ in range(15):
    forward(3 * const)
    right(40)


up()

for x in range(1, 20):
    for y in range(1, 20):
        goto(x * const, y * const)
        dot(5, "blue")

mainloop()
