from turtle import *

screensize(15000, 15000)
c = 50
tracer(0)

left(90)
for _ in range(15):
    goto(xcor() + 5 * c, ycor() + 4 * c)
    goto(xcor() + 4 * c, ycor() - 4  * c)
    goto(xcor() - 7 * c, ycor() - 2  * c)
    goto(xcor() - 2 * c, ycor() + 2  * c)

up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()

