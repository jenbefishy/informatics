from turtle import *

screensize(15000, 15000)
c = 25
tracer(0)
left(90)

right(45)
for _ in range(25):
    forward(55 * c)
    right(90 * c)

up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * c, y * c)
        dot(5, "blue")

answer = 0
t = 55 * (2 ** 0.5)
for x in range(-1000, 1000):
    for y in range(-1000, 1000):
        if (y < x) and (y < -x + t) and (y > -x) and (y > x - t):
            answer += 1

mainloop()
print(answer)

