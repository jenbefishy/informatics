from turtle import *

screensize(15000, 15000)
const = 25
tracer(0)


left(90)
for _ in range(15):
    forward(7 * const)
    right(30)
    forward(8 * const)
    right(150)


up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * const, y * const)
        dot(5, "blue")


mainloop()
