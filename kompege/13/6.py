from turtle import *

screensize(15000, 15000)
tracer(0)
const = 25

left(90)
forward(9 * const)
right(90)
for _ in range(2):
    forward(3 * const)
    right(90)
    forward(3 * const)
    right(270)

for _ in range(2):
    forward(3 * const)
    right(90)

forward(9 * const)
up()

for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * const, y * const)
        dot(5, "blue")

mainloop()

