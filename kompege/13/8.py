from turtle import *
const = 25
screensize(15000, 15000)
tracer(0)

left(90)
for _ in range(8):
    for __ in range(4):
        forward(5 * const)
        right(30)
        forward(6 * const)
        right(150)
    right(60)


up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * const, y * const)
        dot(5, "blue")

mainloop()
