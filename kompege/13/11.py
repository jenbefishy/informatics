from turtle import *

screensize(15000, 15000)
c = 25
tracer(0)
left(90)

for _ in range(10):
    goto(xcor() + 4  * c, ycor() + 3  * c)
    goto(xcor() - 4  * c, ycor() + 10 * c)
    goto(xcor() + 18 * c, ycor() - 12 * c)
    goto(xcor() - 24 * c, ycor() - 12 * c)

up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * c, y * c)
        dot(5, "blue")


mainloop()

