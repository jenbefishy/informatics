from turtle import *

screensize(15000, 15000)
tracer(0)
const = 25

left(90)
for _ in range(14):
    for __ in range(3):
        forward(3 * const)
        right(90)
    left(180)

up()

for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * const, y * const)
        dot(5, "blue")
mainloop()
