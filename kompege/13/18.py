from turtle import *

screensize(15000, 15000)
c = 25
tracer(0)

left(90)
for _ in range(15):
    goto(xcor() + 5   * c, ycor() + 15 * c)
    goto(xcor() + 111 * c, ycor() - 0  * c)
    goto(xcor() - 60  * c, ycor() - 15  * c)
    goto(xcor() - 56   * c, ycor() + 0  * c)

up()
for x in range(-20, 20):
    for y in range(-20, 20):
        goto(x * c, y * c)
        dot(5, "blue")

answer = 0
for x in range(-200, 200):
    for y in range(-200, 200):
        if (y < 3 * x) and (y < 15) and (y > 0) and (y > 0.25 * x - 14):
            answer += 1

mainloop()
print(answer)

