from turtle import *

screensize(15000, 15000)
c = 25
tracer(0)

left(90)
for _ in range(15):
    goto(xcor() + 90 * c, ycor() + 90 * c)
    goto(xcor() - 60 * c, ycor() + 0  * c)
    goto(xcor() - 30 * c, ycor() - 90  * c)

up()
answer = 0
for x in range(-0, 100):
    for y in range(-0, 100):
        if (x < y) and (y < 90) and (y < 3 * x):
            answer += 1


mainloop()
print(answer)

