from itertools import combinations

def f(x):
    B = 18 <= x <= 52
    C = 16 <= x <= 41
    A = a1 <= x <= a2

    return (B <= A) and ((not C) or A)

Ox = [x /4 for x in range(10 * 4, 70 * 4)]

answer = 10 ** 10
for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        answer = min(a2 - a1, answer)

print(round(answer))
