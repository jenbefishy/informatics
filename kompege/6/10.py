
def f(x, y):
    return (x * y > A) and (x > y) and (x < 8)

for A in range(1, 10000):
    isMatch = 1
    for x in range(1, 1000):
        for y in range(1, 1000):
            if f(x, y):
                isMatch = 0
                break

        if isMatch == 0:
            break

    if isMatch == 1:
        print(A)
        break
