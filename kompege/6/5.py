
def f(x):
    return (A % 7 == 0) and (240 % x == 0) <= ((A % x != 0) <= (780 % x != 0))

for A in range(1, 10000):
    if all(f(x) for x in range(1, 10000)):
        print(A)
        break

