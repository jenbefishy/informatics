
def f(x):
    return ((x % 15 == 0) and (x % 21 != 0)) <= ((x % A != 0) or (x % 15 != 0))

for A in range(1, 10000):
    if all(f(x) for x in range(1, 10000)):
        print(A)
        break
