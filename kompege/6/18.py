from itertools import combinations


def f(x):
    D = 17 <= x <= 58
    C = 29 <= x <= 80
    A = a1 <= x <= a2
    return D <= (((not C) and (not A)) <= (not D))

Ox = [x / 4 for x in range(10 * 4, 100 * 4)]

answer = 10 ** 10
for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        answer = min(answer, a2 - a1)

print(round(answer))
        
