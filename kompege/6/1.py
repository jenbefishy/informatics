
def f(x):
    return ((x % A == 0) and (x % 24 == 0) and (x % 16 != 0)) <= (x % A != 0)

for A in range(1, 1000):
    if all(f(x) for x in range(1, 1000)):
        print(A)
        break
