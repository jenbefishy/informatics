from itertools import product
arr = ["".join(c) for c in product("01", repeat=8)]

a = set()
def f(x):
    P = x.startswith("11")
    Q = x.endswith("0")
    A = x in a

    return (not A) <= (P or (not Q))

for x in arr:
    if f(x) == 0:
        a.add(x)

print(len(a))
