from itertools import combinations

def f(x):
    P = 17 <= x <= 54
    Q = 37 <= x <= 83
    A = a1 <= x <= a2

    return P <= ((Q and (not A)) <= (not P))

Ox = [x / 4 for x in range(10 * 4, 100 * 4)]

answer = 10 ** 10
for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        answer = min(a2 - a1, answer)

print(round(answer))
