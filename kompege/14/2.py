
def f(n):
    while "25" in n or "355" in n or "4555" in n:
        if "25" in n:
            n = n.replace("25", "4", 1)
        if "355" in n:
            n = n.replace("355", "2", 1)
        if "4555" in n:
            n = n.replace("4555", "3", 1)

    return n

print(f("2" + (81 * "5")))
