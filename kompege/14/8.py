
def f(n):
    while "11" in n:
        if "112" in n:
            n = n.replace("112", "7", 1)
        else:
            n = n.replace("11", "3", 1)
    return n


print(f("1" * 4 + "112" * 4))
print(3 * 2 + 7 * 4)
