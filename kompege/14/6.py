
def f(n):
    while ">1" in n or ">2" in n or ">3" in n:
        if ">1" in n:
            n = n.replace(">1", "22>", 1)

        if ">2" in n:
            n = n.replace(">2", "2>", 1)

        if ">3" in n:
            n = n.replace(">3", "1>", 1)
    return n

res = 0
for i in str(f(">" + "1" * 11 + "2" * 12 + "3" * 30)):
    if i == ">":
        continue
    res += int(i)
print(res)
