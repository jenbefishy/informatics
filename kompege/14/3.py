
def f(n):
    while "1" in n or "100" in n:
        if "100" in n:
            n = n.replace("100", "0001", 1)
        else:
            n = n.replace("1", "00", 1)

    return n

print(f("1" + 33 * "0").count("0"))
