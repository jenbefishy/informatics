
def f(n):
    while "01" in n or "02" in n or "03" in n:
        n = n.replace("01", "302", 1)
        n = n.replace("02", "3103", 1)
        n = n.replace("03", "20", 1)

    return n

    
for x in range(1, 100):
    for y in range(1, 100):
        for z in range(1, 100):
            s = f("0" + "1" * x + "2" * y + "3" * z)
            if s.count("1") == 28 and s.count("2") == 34 and s.count("3") == 45:
                print(x)
                exit(0)
