
def f(a, b):
    x = -5 + 5 * 4 + a * 4 + 90
    y = 15 + 1 * 4 + b * 4 + 4
    return (x, y)

for a in range(-1000, 1000):
    for b in range(-1000, 1000):
        if f(a, b) == (5, 3):
            print(a, b)
            exit(0)
