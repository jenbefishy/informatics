
def f(a, b, c):
    x = 4 + 2 * 4 + a * 4
    y = 8 + 4 * (-4) + b * 4
    z = 2 + 10 + 4 * (-5) + c * 4
    return (x, y, z)

for a in range(-100, 100):
    for b in range(-100, 100):
        for c in range(-100, 100):
            if f(a, b, c) == (24, 16, 12):
                print(a, b, c)
                exit(0)

