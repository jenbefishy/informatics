
def f(n, b):
    x = -3 + 2 * n + 8 * n + 2
    y = -5 + b * n - 12 * n + 3
    return (x, y)


for n in range(0, 10000):
    for b in range(0, 10000):
        if f(n, b) == (59, 46):
            print(b)
            exit(0)
    
