
def f(n):
    while "555" in n or "888" in n:
        n = n.replace("555", "8", 1)
        n = n.replace("888", "55", 1)

    return n

res = set()
for i in range(1, 1000):
    res.add(f(i * "5"))
print(len(res))
    
