
def f(n, b):
    x, y = 0, 0

    x += 3
    y -= 6

    x = x + 4 * n + 6 * n
    y = y + b * n - 6 * n

    x -= 53
    y += 26

    return (x, y)

for n in range(1, 1000):
    for b in range(1, 1000):
        if f(n, b) == (0, 0):
            print(b)
            exit(0)
