
def f(n):
    while "66" in n:
        n = n.replace("66", "1", 1)
        n = n.replace("11", "2", 1)
        n = n.replace("22", "6", 1)

    return n

answer = 0
for i in range(1, 50):
    if f(i * "6") == "21":
        answer = i
print(answer)
