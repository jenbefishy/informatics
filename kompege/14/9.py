from itertools import permutations

def f(n):
    while "999" in n or "22" in n:
        if "999" in n:
            n = n.replace("999", "12", 1)
        else:
            n = n.replace("22", "1", 1)
    return n


s = "9992" * 33 + "9" + "1" * 14 + "2" * 15 
print(f(s).count("1"))
