
def f(n):
    while "1111" in n:
        n = n.replace("1111", "2", 1)
        n = n.replace("222", "1", 1)
    return n

print(f("1" * 46 + "2" * 31))
