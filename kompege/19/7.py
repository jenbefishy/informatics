from functools import lru_cache

@lru_cache(None)
def game(x, y, z):
    if x + y + z >= 73:
        return 0

    pos = [
        game(x + 3, y, z), game(x, y + 3, z), game(x, y, z + 3),
        game(x + 13, y, z), game(x, y + 13, z), game(x, y, z + 13),
        game(x + 23, y, z), game(x, y + 23, z), game(x, y, z + 23)
        ]

    negative = [c for c in pos if c <= 0]

    if negative:
        return -max(negative) + 1
    else:
        return -max(pos)

for s in range(1, 24):
    print(s, game(2, s, 2 * s))
