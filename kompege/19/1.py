from functools import lru_cache

@lru_cache(None)
def game(x):
    if x >= 33:
        return 0

    pos = [
        game(x + 1), game(x + 2),
        game(x + 3), game(x * 2)
        ]

    negative = []
    for c in pos:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(pos)


ans1 = 0
ans2 = []
ans3 = 0

for s in range(1, 34):
    t = game(s)
    print(s, t)
    if t == -1:
        ans1 = s

    if t == 2:
        ans2.append(s)


    if t == -2:
        ans3 = s

print(ans1)
print(min(ans2), max(ans2))
print(ans3)


