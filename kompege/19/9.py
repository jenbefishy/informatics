from functools import lru_cache

@lru_cache(None)
def game(x):
    if x >= 2163:
        return 0

    pos = [ game(x + 1), game(x * 3) ]

    negative = [c for c in pos if c <= 0]
    if negative:
        return -max(negative) + 1
    else:
        return -max(pos)

ans1 = 0
ans2 = []
ans3 = 0

for s in range(2163, 0, -1):
    t = game(s)
    if t == -1:
        ans1 = s

    if t == 2:
        ans2.append(s)
    
    if t == -2:
        ans3 = s

print(ans1)
print(ans2)
print(ans3)

