from functools import lru_cache

def moves(pos):
    x, y = pos
    m = []
    if x > 0:
        m.append((x - 1, y))
    if x > 1:
        m.append(((x + 1) // 2, y))
    if y > 0:
        m.append((x, y - 1))
    if y > 1:
        m.append((x, (y + 1) // 2))

    return m

@lru_cache(None)
def game(h):
    if sum(h) <= 20: return "w"

    if any(game(i)=="w" for i in moves(h)):
        return "p1"
    
    if all(game(i)=="p1" for i in moves(h)):
        return "v1"

    if any(game(i)=="v1" for i in moves(h)):
        return "p2"
        
    if all(game(i)=="p2" or game(i) == "p1" for i in moves(h)):
        return "v2"


for s in range(10, 5000):
    h = 10, s
    if game(h):
        print(s, game(h))
