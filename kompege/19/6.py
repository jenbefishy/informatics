from functools import lru_cache

@lru_cache(None)
def game(x, y):

    if x * y >= 63:
        return 0

    pos = [
        game(x + 1, y), game(x, y + 1),
        game(x * 2, y), game(x, y * 2)
        ]

    negative = []
    for c in pos:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(pos)


for s in range(1, 32):
     print(s, game(2, s))
    
