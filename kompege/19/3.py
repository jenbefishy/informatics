from functools import lru_cache

@lru_cache(None)
def game(x, y):

    if x + y >= 59:
        return 0

    pos = [
        game(x + 1, y), game(x, y + 1),
        game(x * 2, y), game(x, y * 2)
        ]

    negative = []
    for c in pos:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(pos)


ans1 = 10 ** 10
ans2 = []
ans3 = 10 ** 10

for s in range(1, 54):
    t = game(5, s)

    if t == 1:
        ans1 = min(ans1, s)

    if t == 2:
        ans2.append(s)

    if t == -2:
        ans3 = min(ans3, s)

print(ans1)
print(ans2)
print(ans3)
    
    
