from functools import lru_cache

@lru_cache(None)
def game(x):
    if 36 <= x <= 60:
        return 0
    elif x > 60:
        return 1 

    pos = [
        game(x + 1), game(x * 2),
        game(x * 3)
        ]

    negative = []
    for c in pos:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(pos)

ans1 = 10 ** 10
ans2 = 0
ans3 = []
for s in range(1, 36):
    t = game(s)

    if t == -1:
        ans1 = min(ans1, s)

    if t == 2:
        ans2 += 1

    if t == -2:
        ans3.append(s)

print(ans1)
print(ans2)
print(min(ans3), max(ans3))
    
