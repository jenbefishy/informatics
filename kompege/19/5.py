from functools import lru_cache

@lru_cache(None)
def game(x, y):
    if x + y >= 68:
        return 0

    pos = [
        game(x + 1, y), game(x, y + 1),
        game(x + y, y), game(x, y + x)
        ]

    negative = [c for c in pos if c <= 0]
    if negative:
        return -max(negative) + 1
    else:
        return -max(pos)

for s in range(1, 60):
    print(s, game(8, s))
