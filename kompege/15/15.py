from itertools import combinations, permutations

def f(n):
    sn = str(n)
    arr = [
        sn[0] + sn[1], sn[1] + sn[0],
        sn[1] + sn[2], sn[2] + sn[1],
        sn[2] + sn[0], sn[0] + sn[2],
        ]

    arr = [int(c) for c in arr if c[0] != "0"]
    return max(arr) - min(arr)


for n in range(100, 1000):
    if f(n) == 5:
        print(n)
        break
    
