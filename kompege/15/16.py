
def f(n):
    sn = str(n)
    arr = [
        sn[0] + sn[1], sn[1] + sn[0],
        sn[1] + sn[2], sn[2] + sn[1],
        sn[2] + sn[0], sn[0] + sn[2]
    ]

    arr = [int(c) for c in arr if c[0] != "0"]
    return max(arr) - min(arr)

answer = 0
for n in range(300, 400 + 1):
    if f(n) == 20:
        answer += 1

print(answer)
