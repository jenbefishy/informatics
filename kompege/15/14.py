
def f(n):
    sn = str(n)
    a = int(sn[0]) ** 2 + int(sn[1]) ** 2
    b = int(sn[1]) ** 2 + int(sn[2]) ** 2

    if a > b:
        tmp = b
        b = a
        a = tmp

    return int(str(b) + str(a))

for n in range(100, 1000):
    if f(n) == 9010:
        print(n)
        break
    
