
def f(n):
    sn = bin(n)[2:]
    if n % 2 == 0:
        sn = "1" + sn + "0"
    else:
        sn = "11" + sn + "11"

    return int(sn, 2)

answer = 10 ** 10
for i in range(1, 10000):
    if f(i) > 52:
        answer = min(f(i), answer)

print(answer)
    
