

def f(n):
    sn = bin(n)[2:]
    sn = sn + str(sn.count("1") % 2)
    sn = sn + str(sn.count("1") % 2)
    return int(sn, 2)

answer = 10 ** 10
for i in range(1, 1000):
    if f(i) > 80:
        answer = min(f(i), answer)

print(answer)

