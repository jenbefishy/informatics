
def f(n):
    sn = bin(n)[2:]
    if sn.count("1") % 2 == 0:
        sn = "10" + sn[2:] + "0"
    else:
        sn = "11" + sn[2:] + "1"

    return int(sn, 2)
        

answer = 0
for n in range(1, 10000):
    if f(n) < 35:
        answer = n
print(answer)
