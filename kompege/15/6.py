
def f(n):
    sn = bin(n)[2:]
    sn = sn[::-1]
    
    while sn[0] == "0":
        sn = sn[1:]

    return int(sn, 2)


answer = 0
for n in range(1, 100):
    if f(n) == 13:
        answer = n
print(answer)
    
