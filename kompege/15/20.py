
def f(n, m):
    s1 = 1
    s2 = 1

    sn = str(n)
    sm = str(m)

    for i in sn:
        if int(i) % 2 == 0 and i != "0":
            s1 *= int(i)
    
    for i in sm:
        if int(i) % 2 == 0 and i != "0":
            s1 *= int(i)

    for i in sn:
        if int(i) % 2 != 0:
            s2 *= int(i)
    
    for i in sm:
        if int(i) % 2 != 0:
            s2 *= int(i)

    return abs(s2 - s1)

for m in range(1, 10000):
    if f(120, m) == 29:
        print(m)
        break
