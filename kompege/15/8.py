
def f(n):
    sn = bin(2 * n)[2:]
    sn += str(sn.count("1") % 2)
    sn += str(sn.count("1") % 2)
    return int(sn, 2)

for n in range(1, 10000):
    if f(n) > 1017:
        print(n)
        break
    
