
def f(n):
    if n % 3 == 0:
        n //= 3
    else:
        n -= 1

    if n % 5 == 0:
        n //= 5
    else:
        n -= 1

    if n % 11 == 0:
        n //= 11
    else:
        n -= 1

    return n

answer = 0
for n in range(1, 10000):
    if f(n) == 8:
        answer += 1

print(answer)
        
