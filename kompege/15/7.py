
def f(n):
    sn = bin(n)[2:]
    sn += str(sn.count("1") % 2)
    sn += str(sn.count("1") % 2)
    return int(sn, 2)

res = set()
for i in range(1, 10000):
    if f(i) < 80:
        res.add(f(i))

print(len(res))
    
