
def f(n):
    sn = bin(n)[2:]
    sn += sn[-1]

    if sn.count("1") % 2 == 0:
        sn += "0"
    else:
        sn += "1"

    if sn.count("1") % 2 == 0:
        sn += "0"
    else:
        sn += "1"

    return int(sn, 2)


answer = 10 ** 10
for n in range(1, 10000):
    if f(n) > 144:
        answer = min(f(n), answer)

print(answer)
