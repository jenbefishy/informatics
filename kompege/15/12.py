
def f(n):
    n = str(n)
    a = int(n[0]) * int(n[1])
    b = int(n[2]) * int(n[3])

    if a > b:
        tmp = b
        b = a
        a = tmp

    return int(str(a) + str(b))

answer = 0
for n in range(1000, 10000):
    if f(n) == 1214:
        answer = n
print(answer)


        
