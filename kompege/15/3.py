
def f(n):
    sn = bin(n)[2:]
    sn = sn + sn[-1]

    if sn.count("1") % 2 == 0:
        sn += "0"
    else:
        sn += "1"
        
    if sn.count("1") % 2 == 0:
        sn += "0"
    else:
        sn += "1"

    return int(sn, 2)

for i in range(1, 1000):
    if f(i) > 130:
        print(i)
        break
        
