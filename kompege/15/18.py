
def f(n):
    s1 = 0
    s2 = 0

    sn = str(n)
    for i in range(len(sn)):
        if int(sn[i]) % 2 == 0:
            s1 += int(sn[i])

        if (i + 1) % 2 == 0:
            s2 += int(sn[i])

    return abs(s2 - s1)

for n in range(1, 10000):
    if f(n) == 9:
        print(n)
        break
