
def f(n):
    sn = str(n)
    a = int(sn[0]) * int(sn[1])
    b = int(sn[1]) * int(sn[2])

    if a > b:
        tmp = b
        b = a
        a = tmp

    return int(str(b) + str(a))

answer = 0
for i in range(100, 1000):
    if f(i) == 240:
        answer = i
print(answer)
