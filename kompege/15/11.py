
def f(n):
    sn = bin(n)[2:]

    one  = 0
    zero = 0
    for i in range(0, len(sn)):
        c = i + 1
        if c % 2 == 0 and sn[i] == "1": one += 1
        if c % 2 != 0 and sn[i] == "0": zero += 1
        
    return abs(one - zero)

for n in range(1, 10000):
    if f(n) == 5:
        print(n)
        break
