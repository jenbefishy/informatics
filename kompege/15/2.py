
def f(n):
    sn = bin(n)[2:]
    if n % 2 == 0:
        sn = sn + "01"
    else:
        sn = sn + "10"

    return int(sn, 2)


answer = 10 ** 10
for i in range(1, 10000):
    if f(i) > 81:
        answer = min(f(i), answer)

print(answer)
