
def f(n):
    arr = []
    for a, b in zip(str(n), str(n)[1:]):
        arr.append(a + b)

    arr = list(map(int, arr))
    return max(arr) - min(arr)


for n in range(10, 10000):
    if f(n) == 44:
        print(n)
        break
