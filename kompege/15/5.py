numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

def f(n):
    sn = toBase(n, 3)
    sn += str(n % 3)
    return int(sn, 3)

answer = 10 ** 10
for i in range(1, 10000):
    if len(str(f(i))) == 3:
        answer = min(f(i), answer)

print(answer)
