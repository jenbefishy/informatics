numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

for x in range(1, 10000):
    if toBase(4 ** 2015 + 2 ** x - 2 ** 2015 + 15, 2).count("1") == 500:
        print(x)
        break
