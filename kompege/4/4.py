numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

def sumStr(s):
    res = 0
    for i in s:
        res += int(i)
    return res


print(sumStr(toBase(51 * 7 ** 12 - 7 ** 3 - 22, 7)))
