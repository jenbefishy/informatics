from string import ascii_uppercase, ascii_lowercase
numbers = "0123456789" + ascii_uppercase + ascii_lowercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

answer = 0
for x in range(1, 10000):
    if len(toBase(x, 5)) <= 4 and len(toBase(x, 2)) >= 5 and toBase(x, 16).endswith("C"):
        answer += 1
print(answer)
