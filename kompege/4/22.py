
for x in range(1, 17):
    a = f"9759{x}"
    b = f"3{x}108"

    res = int(a, 17) + int(b, 17)

    if res % 11 == 0:
        print(res // 11)
        break
