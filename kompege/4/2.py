numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

print(toBase(3 * 16 ** 8 - 4 ** 5 + 3, 4).count("3"))
