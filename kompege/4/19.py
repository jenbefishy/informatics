from string import ascii_uppercase

numbers = "0123456789" + ascii_uppercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

for n in range(1, 10000):
    if len(toBase(n, 7)) == 2 and len(toBase(n, 6)) == 3 and toBase(n, 13).endswith("3"):
        print(n)
        break
