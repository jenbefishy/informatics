from string import ascii_uppercase, ascii_lowercase
numbers = "0123456789" + ascii_uppercase + ascii_lowercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

for n in range(2, len(numbers)):
    s = toBase(68, n)
    if len(s) == 4 and s[-1] == "2":
        print(n)
        break
