from string import ascii_uppercase, ascii_lowercase
numbers = "0123456789" + ascii_uppercase + ascii_lowercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

for x in range(0, 15):
    a = f"123{x}5"
    b = f"1{x}233"
    res = int(a, 15) + int(b, 15)
    if res % 14 == 0:
        print(res // 14)
        break
