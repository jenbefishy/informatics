from string import ascii_uppercase
numbers = "0123456789" + ascii_uppercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res


for x in range(0, 1000):
    if int("33", x + 4) - int("33", 4) == 33:
        print(x)
        break
