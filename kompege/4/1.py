
numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

print(toBase(64 ** 30 + 2 ** 300 - 4, 8).count("7"))
