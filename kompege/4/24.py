from string import ascii_uppercase
numbers = "0123456789" + ascii_uppercase

answer = 0
ansY = "Z"

for x in numbers[0:15]:
    for y in numbers[0:17]:
        a = f"123{x}5"
        b = f"67{y}9"
        res =  int(a, 15) + int(b, 17)
        if res % 131 == 0:
            if ansY > y:
                answer = res // 131
                ansY = y
print(answer)
