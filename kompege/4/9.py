from string import ascii_uppercase
numbers = "0123456789" + ascii_uppercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

print(toBase(6 * 144 ** 26 + 11 * 12 ** 75 - 48, 12).count("B"))


