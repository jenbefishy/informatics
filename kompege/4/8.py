numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

def sumStr(s):
    res = 0
    for i in s:
        res += int(i)
    return res


for x in range(1, 10000):
    if sumStr(toBase(36 ** 17 - 6 ** x + 71, 6)) == 61:
        print(x)
        break
