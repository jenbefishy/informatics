from string import ascii_uppercase
numbers = "0123456789" + ascii_uppercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

s = toBase(5 * 216 ** 1156 - 4 * 36 ** 1147 + 6 ** 1153 - 875, 6)
print(s.count("5") - s.count("0"))