from string import ascii_uppercase, ascii_lowercase
numbers = "0123456789" + ascii_uppercase + ascii_lowercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

for n in range(1, 10000):
    if len(toBase(n, 6)) == 2 and len(toBase(n, 5)) == 3 and toBase(n, 11).endswith("1"):
        print(n)
        break
