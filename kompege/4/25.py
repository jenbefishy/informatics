from string import ascii_uppercase, ascii_lowercase

numbers = "0123456789" + ascii_uppercase + ascii_lowercase

def fromBaseToInt(x, base):
    res = 0
    for i in x:
        res = res * base + numbers.index(i)
    return res

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res


answer = []
for x in numbers[0:21]:

    isMatch = 1
    for y in numbers[0:21]:
        a = f"12{y}{x}9"
        b = f"36{y}99"
        res = fromBaseToInt(a, 21) + fromBaseToInt(b, 21)
        if res % 18 != 0:
            isMatch = 0
            break

    if isMatch:
        a = f"125{x}9"
        b = f"36599"
        res = fromBaseToInt(a, 21) + fromBaseToInt(b, 21)
        print(res // 18)
        break
