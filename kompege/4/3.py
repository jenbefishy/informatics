numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

print(toBase(2 * 27 ** 7 + 3 ** 10 - 9, 3).count("0"))
