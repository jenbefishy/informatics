numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

for x in range(1, 10000):
    if toBase(4 ** 1014 - 2 ** x + 12, 2).count("0") == 2000:
        print(x)
        break
