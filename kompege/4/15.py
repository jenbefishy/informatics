from string import ascii_uppercase
numbers = "0123456789" + ascii_uppercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res


for x in range(20, 31):
    if toBase(x, 3).endswith("11"):
        print(x)
        break
