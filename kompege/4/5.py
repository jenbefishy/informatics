numbers = "0123456789"

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res

for x in range(1, 1000):
    if toBase(125 ** 200 -  5 ** x + 74, 5).count("4") == 100:
        print(x)
        break
