
def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    data = {x:0 for x in range(0, 10)}

    for i in arr:
        for j in str(i):
            data[int(j)] += 1

    print(min(data.values()))
    

file = open("27A_2738.txt")
solve(file)
file.close()

file = open("27B_2738.txt")
solve(file)
file.close()
