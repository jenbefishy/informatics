
def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    k = [[] for _ in range(80)]
    for i in arr:
        k[i % 80] += [i]

    a = []
    for i in k:
        if i != []:
            a += [min(i), max(i)]

    answer = []
    for i in range(len(a)):
        for j in range(len(a)):
            if i == j:
                continue
            
            if abs(a[i] - a[j]) % 80 == 0:
                answer.append(abs(a[i] - a[j]))
    print(max(answer))

file = open("27A_2732.txt")
solve(file)
file.close()

file = open("27B_2732.txt")
solve(file)
file.close()
