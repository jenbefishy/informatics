def getSumOfNumber(a):
    res = 0
    for i in str(a):
        res += int(i)
    return res

def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    data = {x:0 for x in range(0, 10000)}

    for i in arr:
        data[getSumOfNumber(i)] += 1

    print(max(data.values()))


file = open("27A_2734.txt")
solve(file)
file.close()

file = open("27B_2734.txt")
solve(file)
file.close()
