
def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    k = [0] * 70
    for i in arr:
        k[i % 69] += 1

    answer = 0
    for i in range(0, 69):
        answer += (k[i] * (k[i] - 1)) // 2
    print(answer)

file = open("27A_2725.txt")
solve(file)
file.close()

file = open("27B_2725.txt")
solve(file)
file.close()
