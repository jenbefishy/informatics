def solve(file):
    file.readline()
    arr = [int(x) for x in file]

    k = [0] * 2000
    for i in arr:
        if i > 2000:
            continue
        k[i % 2000] += 1

    
    answer = (k[0] * (k[0] - 1)) // 2 + \
             (k[1000] * (k[1000] - 1)) // 2

    for i in range(1, 1000):
        answer += k[i] * k[2000 - i] 

    print(answer)


file = open("27A_2737.txt")
solve(file)
file.close()

file = open("27B_2737.txt")
solve(file)
file.close()
