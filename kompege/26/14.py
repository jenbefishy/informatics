def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    k = [[] for _ in range(12)]
    for i in arr:
        k[i % 12] += [i]

    for i in range(len(k)):
        k[i].sort(reverse=True)

    arr = []
    for i in k:
        arr += i[0:10]

    answer = [k[0][0] + k[0][1] + k[0][2]]
    for a in range(len(arr)):
        for b in range(a + 1, len(arr)):
            for c in range(b + 1, len(arr)):
                for d in range(c + 1, len(arr)):
                    if (arr[a] * arr[b] * arr[c] * arr[d]) % 12 == 0:
                        answer.append(arr[a] + arr[b] + arr[c] + arr[d])
                        
    
    print(max(answer))


file = open("27A_2730.txt")
solve(file)
file.close()

file = open("27B_2730.txt")
solve(file)
file.close()
