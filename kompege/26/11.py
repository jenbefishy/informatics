def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    k31 = []
    k = []
    for i in arr:
        if i % 31 == 0:
            k31.append(i)
        else:
            k.append(i)

    k31.sort()
    answer = k31[0] * min(k)
    if len(k31) > 1:
        answer = min(k31[0] * k31[1], answer)
    
    print(answer)

file = open("27A_2727.txt")
solve(file)
file.close()

file = open("27B_2727.txt")
solve(file)
file.close()
