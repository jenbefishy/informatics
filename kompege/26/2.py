
def solve(file):
    file.readline()
    arr = [int(x) for x in file]

    k7 = 0
    k = 0
    answer = 0

    for i in arr:
        if i % 7 == 0:
            k7 += 1
        else:
            k += 1
        
    print(k7 * k + (k7 * (k7 - 1)) // 2)



file = open("27A_2720.txt")
solve(file)
file.close()

file = open("27B_2720.txt")
solve(file)
file.close()
