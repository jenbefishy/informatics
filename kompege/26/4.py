def solve(file):
    file.readline()
    arr = [int(x) for x in file]

    k5_2 = 0
    k5_1 = 0
    k_2 = 0
    k_1 = 0

    for i in arr:
        if i % 5 == 0:
            if i % 2 == 0:
                k5_2 += 1
            else:
                k5_1 += 1
        else:
            if i % 2 == 0:
                k_2 += 1
            else:
                k_1 += 1
    print(k5_2 * k_1 + k5_1 * k_2 + k5_1 * k5_2)

file = open("27A_2722.txt")
solve(file)
file.close()

file = open("27B_2722.txt")
solve(file)
file.close()
