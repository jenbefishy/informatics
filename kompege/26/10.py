def solve(file):
    file.readline()
    arr = [int(x) for x in file]

    k2 = []
    k1 = []

    for i in arr:
        if i % 2 == 0:
            k2.append(i)
        else:
            k1.append(i)

    print(max(k2) + max(k1))
        

file = open("27A_2726.txt")
solve(file)
file.close()

file = open("27B_2726.txt")
solve(file)
file.close()
