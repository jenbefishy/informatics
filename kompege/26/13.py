def solve(file):
    file.readline()
    arr = [int(x) for x in file]

    k = {x : [] for x in range(0, 11)}

    for i in arr:
        k[i % 11] += [i]

    for i in k.keys():
        k[i] = list(sorted(k[i]))

    answer = [k[0][0] + k[0][1] + k[0][2]]
    for i in range(0, 11):
        for j in range(i + 1, 11):
            c = (11 - j - i) % 11

            a1 = 0
            a2 = 0
            a3 = 0

            if j == c:
                a3 += 1

            if i == j:
                a2 += 1

            if i == c:
                a3 += 1
                

            answer.append(k[i][a1] + k[j][a2] + k[c][a3])
    print(min(answer))
    
        
    

file = open("27A_2729.txt")
solve(file)
file.close()

file = open("27B_2729.txt")
solve(file)
file.close()
