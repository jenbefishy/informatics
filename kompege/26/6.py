def solve(file):
    file.readline()
    arr = [int(x) for x in file.readlines()]
    k = [0] * 132
    answer = 0
    for i in arr:
        k[i % 131] += 1
    
    answer = (k[0] * (k[0] - 1)) // 2 
    for i in range(1, 65 + 1):
        answer += k[i] * k[131 - i]
    print(answer)
        
    

file = open("27A_2724.txt")
solve(file)
file.close()

file = open("27B_2724.txt")
solve(file)
file.close()
