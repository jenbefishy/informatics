
def solve(file):
    n = int(file.readline())

    answer = 0
    k2 = 0
    k1 = 0

    for i in file.readlines():
        i = int(i)
        if i % 2 == 0:
            k2 += 1
        else:
            k1 += 1


    print((k2 * (k2 - 1)) // 2 + (k1 * (k1 - 1)) // 2)
           



file = open("27A_2719.txt")
solve(file)
file.close()

file = open("27B_2719.txt")
solve(file)
file.close()
