
def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    data = {x:0 for x in range(1, 10)}
    for i in arr:
        data[int(str(i)[0])] += 1

    print(min(data.values()))

file = open("27A_2736.txt")
solve(file)
file.close()

file = open("27B_2736.txt")
solve(file)
file.close()
