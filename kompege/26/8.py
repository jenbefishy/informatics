def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    k1 = [0] * 80
    k2 = [0] * 80
    for i in arr:
        if i > 50_000:
            k1[i % 80] += 1
        else:
            k2[i % 80] += 1

    answer = k1[0] * k2[0] + k1[40] * k2[40] + \
        (k1[0] * (k1[0] - 1)) // 2 + (k1[40] * (k1[40] - 1)) // 2

    for i in range(1, 40):
        answer += k1[i] * k2[80 - i] + k1[i] * k1[80 - i] 
        answer += k2[i] * k1[80 - i]

    print(answer)
        

file = open("27A_2733.txt")
solve(file)
file.close()

file = open("27B_2733.txt")
solve(file)
file.close()
