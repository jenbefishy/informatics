def solve(file):
    n = file.readline()
    dots = []

    for line in file.readlines():
        x, y = map(int, line.split())
        if y == 0:
            dots.append([x, y])

    print(len(dots))


file = open("27A_2731.txt")
solve(file)
file.close()

file = open("27B_2731.txt")
solve(file)
file.close()
