
def solve(file):
    file.readline()
    data = [int(x) for x in file]

    k65 = 0
    k13 = 0
    k5  = 0
    k   = 0

    for i in data:
        if i % 65 == 0:
            k65 += 1
        elif i % 13 == 0:
            k13 += 1
        elif i % 5 == 0:
            k5 += 1
        else:
            k += 1

    print(k65 * k + k65 * k13 + k65 * k5 + k13 * k5 + (k65 * (k65 - 1)) // 2)

file = open("27A_2721.txt")
solve(file)
file.close()

file = open("27B_2721.txt")
solve(file)
file.close()
