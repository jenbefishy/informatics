def solve(file):
    file.readline()
    arr = [int(x) for x in file]

    k23_2 = []
    k23_1 = []
    k_2   = []
    k_1   = []
    
    for i in arr:
        if i % 23 == 0:
            if i % 2 == 0:
                k23_2.append(i)
            else:
                k23_1.append(i)
        else:
            if i % 2 == 0:
                k_2.append(i)
            else:
                k_1.append(i)

    
    answer = max(max(k23_2) + max(k_2), max(k23_1) + max(k_1))
    print(answer)

    

file = open("27A_2728.txt")
solve(file)
file.close()

file = open("27B_2728.txt")
solve(file)
file.close()
