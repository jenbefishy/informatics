
def solve(file):
    file.readline()
    arr = [int(x) for x in file]
    k19 = 0
    for i in arr:
        if i % 19 == 0:
            k19 += 1
    print((k19 * (k19 - 1) * (k19 - 2)) // 6)

file = open("27A_2723.txt")
solve(file)
file.close()

file = open("27B_2723.txt")
solve(file)
file.close()
