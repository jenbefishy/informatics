from string import ascii_uppercase
line = open("24_2505.txt").readline()

data = {x:0 for x in ascii_uppercase}

for i in range(len(line) - 2):
    if line[i] == line[i + 2]:
        data[line[i + 1]] += 1

answer = max(data.values())

for i in data.keys():
    if data[i] == answer:
        print(i, answer)
    

