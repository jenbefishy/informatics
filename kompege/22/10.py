lines = open("24_587.txt").readlines()

count = 0
for line in lines:
    aCount = 0
    bCount = 0

    for i in line:
        if i == "A":
            aCount += 1
        elif i == "B":
            bCount += 1

    if bCount / aCount >= 1.05:
        count += 1
            
print(count)
