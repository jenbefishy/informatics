from string import ascii_uppercase
lines = open("24_2508.txt").readlines()

count = max(x.count("Q") for x in lines)
answer = 0
letter = 0

for line in lines:
    if line.count("Q") == count:
        answer = 10 ** 10
        letter = ""
        k = 0

        for i in ascii_uppercase:
            k = line.count(i)
            if k < answer:
                letter = i
                answer = k

print(letter, sum(x.count(letter) for x in lines))

        
    
