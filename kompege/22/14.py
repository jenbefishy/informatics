line = open("24_836.txt").readline()

count = 0
for i in range(len(line) - 4):
    s = line[i] + line[i + 1] + line[i + 2] + line[i + 3] + line[i + 4]
    if s[0] == s[-1] and s[1] == s[-2]:
        count += 1
print(count)
