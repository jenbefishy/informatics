from string import ascii_uppercase
line = open("24_2509.txt").readline()

a = 0
b = 10 ** 10

for i in ascii_uppercase:
    a = max(line.count(i), a)
    b = min(line.count(i), b)

print(a - b)
