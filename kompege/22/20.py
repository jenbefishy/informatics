from string import ascii_uppercase
lines = open("24_2507.txt").readlines()

matchLine = ""
maxSubLineLen = 0
for line in lines:

    count = 0
    prev = ""
    maxLen = 0
    for i in line:
        if prev == i:
            count += 1
            maxLen = max(count, maxLen)
        else:
            count = 1
        prev = i

    if maxLen > maxSubLineLen:
        maxSubLineLen = maxLen
        matchLine = line



letter = 0
count = 0
for i in ascii_uppercase:
    k = matchLine.count(i)
    if k > count:
        letter = i
        count = k

print(letter, sum(x.count(letter) for x in lines))
        

        
