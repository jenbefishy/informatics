from string import ascii_uppercase
line = open("24_2504.txt").readline()

data = {x:0 for x in ascii_uppercase}

for i in range(len(line) - 4):
    if line[i] == line[i + 4] == "C" and \
       line[i + 1] == line[i + 3] == "B":
        data[line[i + 2]] += 1

answer = max(data.values())
for i in data.keys():
    if data[i] == answer:
        print(i, data[i])

