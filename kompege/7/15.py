
data = {
    "A" : "BCD",
    "B" : "EF",
    "C" : "IBDFHG",
    "D" : "I",
    "E" : "LG",
    "F" : "EG",
    "G" : "LMKJ",
    "H" : "G",
    "I" : "HGJ",
    "J" : "K",
    "K" : "M",
    "L" : "M",
    }

def f(s, end):
    if s[-1] == end:
        if "E" in s:
            return 1
        return 0
    return sum(f(s + x, end) for x in data[s[-1]])
print(f("A", "M"))
