
times = {
    "АБ" : 40, 
    "АГ" : 90,
    "АВ" : 60,
    "БД" : 80,
    "БГ" : 30,
    "ВГ" : 20,
    "ВИ" : 80,
    "ГИ" : 50,
    "ГД" : 70,
    "ДЖ" : 100,
    "ДЛ" : 120,
    "ДЕ" : 80,
    "ЕЛ" : 30,
    "ЕК" : 40,
    "ЖЛ" : 50,
    "ИЕ" : 70,
    "ИК" : 30,
    "КЛ" : 50,
    }

data = {
    "А" : "БГВ",
    "Б" : "ДГ",
    "В" : "ГИ",
    "Г" : "ИД",
    "Д" : "ЖЛЕ",
    "Е" : "ЛК",
    "Ж" : "Л",
    "И" : "ЕК",
    "К" : "Л",
    }

res = []
def f(s, end):
    if s[-1] == end:
        res.append(s)
        return 1
    return sum(f(s + x, end) for x in data[s[-1]])

f("А", "Л")
answer = 10 ** 10
for i in res:
    t = 0
    for a, b in zip(i, i[1:]):
        t += times[a + b]
    answer = min(t, answer)

print(answer)
