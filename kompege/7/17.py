
data = {
    "А" : "БВГД",
    "Б" : "ЕВ",
    "В" : "Ж",
    "Г" : "ВЖ",
    "Д" : "ГЖЗ",
    "Е" : "ЖИ",
    "Ж" : "И",
    "З" : "ЖИ",
    "И" : "КЛМ",
    "К" : "М",
    "Л" : "М",
    }

def f(s, end):
    if s[-1] == end:
        if "Ж" in s and "К" not in s:
            return 1
        return 0
    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "М"))
