
data = {
    "А" : "БГ",
    "Б" : "ДЕ",
    "В" : "БАГ",
    "Г" : "ЕЖ",
    "Д" : "ИЛЕ",
    "Е" : "ЛВ",
    "Ж" : "Е",
    "И" : "Л",
    "К" : "Ж",
    "Л" : "ЖК",
    }

def f(s, end):
    if s[-1] == end and len(s) != 1:
        return 1

    res = 0
    for i in data[s[-1]]:
        if i == end:
            res += f(s + i, end)
        if i not in s:
            res += f(s + i, end)
    return res


print(f("Е", "Е"))
