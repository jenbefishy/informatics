
data = {
    "А" : "БВГД",
    "Б" : "Е",
    "В" : "БК",
    "Г" : "ВЗД",
    "Д" : "ЗИ",
    "Е" : "КЖ",
    "Ж" : "К",
    "З" : "КИ",
    "И" : "Ж",
    }

def f(s, end):
    if s[-1] == end:
        if "Е" not in s:
            return 1
        return 0

    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "К"))
