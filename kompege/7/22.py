
data = {
    "А" : "ИБЖ",
    "Б" : "ВЗ",
    "В" : "Г",
    "Г" : "ЗЖД",
    "Д" : "ЖАЕК",
    "Е" : "ИА",
    "Ж" : "З",
    "З" : "АВ",
    "И" : "А",
    "К" : "Е",
    }

def f(s, end):
    if s[-1] == end and len(s) != 1:
        return 1

    res = 0
    for i in data[s[-1]]:
        if i == end:
            res += f(s + i, end)
        if i not in s:
            res += f(s + i, end)
    return res

print(f("З", "З"))
