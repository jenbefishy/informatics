
data = {
    "А" : "БДГ",
    "Б" : "ВГ",
    "В" : "ЗКЕ",
    "Г" : "ВЕЖ",
    "Д" : "Г",
    "Е" : "КЖИМ",
    "Ж" : "И",
    "З" : "ЛК",
    "И" : "М",
    "К" : "ЛНМ",
    "Л" : "Н",
    "М" : "Н",
    }

def f(s, end):
    if s[-1] == end:
        count = 0
        if "Г" in s:
            count += 1
        if "Е" in s:
            count += 1
        if count == 1:
            return 1
        return 0
    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "Н"))
