
data = {
    "А" : "БГДЕ",
    "Б" : "В",
    "В" : "ИЖ",
    "Г" : "ВБДЖ",
    "Д" : "ЖЗ",
    "Е" : "ДЗК",
    "Ж" : "Л",
    "З" : "ЖЛ",
    "И" : "ЖЛ",
    "К" : "ЗЛМ",
    "М" : "Л",
    }

def f(s, end):
    if s[-1] == end:
        if "З" not in s and "Ж" in s:
            return 1
        return 0

    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "Л"))
