
data = {
    "А" : "БВГ",
    "Б" : "ВД",
    "В" : "ДЖ",
    "Г" : "ВЕ",
    "Д" : "ЖИ",
    "Е" : "ЖК",
    "Ж" : "К",
    "И" : "К",
    }


def f(s, end):
    if s[-1] == end:
        return 1

    return  sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "К"))
