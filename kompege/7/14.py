
data = {
    "A" : "BCDE",
    "B" : "FG",
    "C" : "BGH",
    "D" : "CH",
    "E" : "DHI",
    "F" : "J",
    "G" : "FJK",
    "H" : "GK",
    "I" : "HKL",
    "J" : "M",
    "K" : "JML",
    "L" : "M",
    }

def f(s, end):
    if s[-1] == end:
        return 1
    res = 0
    for i in data[s[-1]]:
        if i not in s:
            res += f(s + i, end)
    return res

print(f("A", "M"))
