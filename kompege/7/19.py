
data = {
    "А" : "БВГД",
    "Б" : "ЕВ",
    "В" : "Ж",
    "Г" : "ВЖ",
    "Д" : "ГЖЗ",
    "Е" : "ЖИ",
    "Ж" : "И",
    "З" : "ИЖ",
    "И" : "КЛ",
    "К" : "М",
    "Л" : "КМ",
    }

res = []
def f(s, end):
    if s[-1] == end:
        res.append(s)
        return 0
    return sum(f(s + x, end) for x in data[s[-1]])

f("А", "М")
answer = 0
for i in res:
    answer = max(len(i), answer)
print(answer - 1)

        
