
data = {
    "А" : "БВ",
    "Б" : "ГВ",
    "В" : "Г",
    "Г" : "ДЕЗЖ",
    "Д" : "ЕИ",
    "Е" : "ИКЗ",
    "Ж" : "ЗЛ",
    "З" : "КЛ",
    "И" : "КМ",
    "К" : "М",
    "Л" : "КМ",
    }

def f(s, end):
    if s[-1] == end:
        if "Е" in s:
            return 1
        return 0
    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "М"))
