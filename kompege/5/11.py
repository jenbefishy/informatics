from itertools import product, permutations

def f(x, y, z, w):
    return (y <= x) or (not ((x <= z) and (z <= x))) and (not w)

for a in product([0, 1], repeat=6):
    table = [
        (0, 0, 0, a[0]),
        (a[1], 0, 0, a[2]),
        (a[3], a[4], 0, a[5])
        ]

    F = [0, 0, 0]

    if len(table) != len(set(table)):
        continue

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
