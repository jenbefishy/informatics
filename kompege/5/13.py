from itertools import product, permutations

def f(x, y, z, w):
    return ((w <= y) or (not (y <= z))) and (not x) and (not (x == z))

for a in product([0, 1], repeat=5):
    table = [
        (0, a[0], 1, a[1]),
        (1, a[2], a[3], 1),
        (0, a[4], 1, 1)
        ]

    F = [1, 1, 1]

    if len(table) != len(set(table)):
        continue

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
