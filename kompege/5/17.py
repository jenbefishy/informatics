from itertools import permutations, product

def f(a, b, c, d):
    return ((not a) and (not b)) or (b == c) or d

for a in product([0, 1], repeat=4):
    table = [
        (a[0], a[1], 1, a[2]),
        (1, 0, a[3], 1),
        (0, 0, 1, 1)
    ]

    F = [0, 0, 0]

    if len(table) != len(set(table)):
        continue

    for i in permutations("abcd"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
