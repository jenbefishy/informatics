from itertools import product, permutations

def f(x, y, z, w):
    return (x <= y) or (not (w <= z))

table = [
    (1, 0, 0, 1),
    (0, 0, 0, 1),
    (1, 0, 1, 1)
    ]

F = [0, 0, 0]

for i in permutations("xyzw"):
    if [f(**dict(zip(i, row))) for row in table] == F:
        print(i)
