from itertools import permutations

def f(a, b, c, d):
    return ((a and b) == (not c)) and (b <= d)

table = [
    (1, 0, 0, 0),
    (1, 0, 1, 0),
    (1, 0, 1, 1),
    (1, 1, 0, 0)
    ]

F = [1, 1, 1, 1]

for i in permutations("abcd"):
    if [f(**dict(zip(i, row))) for row in table] == F:
        print(i)
