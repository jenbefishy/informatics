from itertools import product, permutations

def f(x, y, z, w):
    return (x or (not y)) and (not (y == z)) and w

for a in product([0, 1], repeat=4):
    table = [
        (0, 1, a[0], 0),
        (a[1], 1, 1, 0),
        (1, a[2], 0, a[3])
        ]

    if len(table) != len(set(table)):
        continue

    F = [1, 1, 1]

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
            break
