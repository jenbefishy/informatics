from itertools import permutations, product

def f(x, y, z, w):
    return (x == (not z)) <= ((x or w) == y)

for a in product([0, 1], repeat=5):

    table = [
        (0, a[0], 0, a[1]),
        (a[2], a[3], 0, 0),
        (a[4], 0, 0, 0)
        ]

    F = [0, 0, 0]

    if len(table) != len(set(table)):
        continue

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
