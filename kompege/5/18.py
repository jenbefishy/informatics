from itertools import permutations, product

def f(x, y, z, w):
    return ((z <= x) and (x <= w)) or (y == (z or x))

for a in product([0, 1], repeat=7):
    table = [
        (a[0], 1, a[1], a[2]),
        (a[3], a[4], 1, 1),
        (a[5], 1, a[6], 1)
        ]
    F = [0, 0, 0]
    if len(table) != len(set(table)):
        continue

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
