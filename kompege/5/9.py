from itertools import permutations

def f(x, y, z, w):
    return (y <= x or z) and (z <= y)

table = [
    (1, 0, 0, 0),
    (1, 1, 0, 0),
    (1, 1, 0, 1),
    (0, 1, 1, 0)
    ]

F = [0, 0, 0, 0]

for i in permutations("xyzw"):
    if [f(**dict(zip(i, row))) for row in table] == F:
        print(i)
