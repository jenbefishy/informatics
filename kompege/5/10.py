from itertools import permutations

def f(x, y, z):
    return (not (x == (y <= z)))

table = [
    (0, 0, 1),
    (0, 1, 1)
    ]

F = [1, 0]

for i in permutations("xyz"):
    if [f(**dict(zip(i, row))) for row in table] == F:
        print(i)
