from itertools import permutations

def f(x, y, z, w):
    return (not y) and x and ((not z) or w)

table = [
    (0, 1, 0, 0),
    (1, 1, 0, 0),
    (1, 1, 1, 0)
    ]

F = [1, 1, 1]

for i in permutations("xyzw"):
    if [f(**dict(zip(i, row))) for row in table] == F:
        print(i)
