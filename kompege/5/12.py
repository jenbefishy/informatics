from itertools import permutations, product

def f(x, y, z, w):
    return (not w) and ((y or z) <= ((not x) and y))

for a in product([0, 1], repeat=8):
    table = [
        (a[0], a[1], a[2], 1),
        (a[3], a[4], 1, a[5]),
        (a[6], 1, 1, a[7])
    ]

    F = [1, 1, 1]

    if len(table) != len(set(table)):
        continue

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
                         
