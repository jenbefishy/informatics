from itertools import product
arr = ["".join(c) for c in product("01234", repeat=6)]

def isMatch(i):
    if i[0] not in "10" and i[-1] not in "34":
        return 1
    return 0

answer = 0
for i in arr:
    if isMatch(i):
        answer += 1

print(answer)
