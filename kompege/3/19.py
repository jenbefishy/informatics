from itertools import product
arr = ["".join(c) for c in product("АИМРЯ", repeat=4)]

count = 0
for i in arr:
    count += 1
    if i == "АРИЯ":
        print(count)
        exit(0)
