from itertools import product
arr = ["".join(c) for c in product("САЛО", repeat=6)]
answer = 0
for i in arr:
    if 0 < i.count("О") <= 3:
        answer += 1

print(answer)
