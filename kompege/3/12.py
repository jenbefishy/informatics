from itertools import product
arr = ["".join(c) for c in product("ГЕПАРД", repeat=5)]

answer = 0
for i in arr:
    if i.count("Г") == 1 and i[0] != "А" and i[-1] != "Е":
        answer += 1

print(answer)
