from itertools import product

arr = ["".join(c) for c in product("ЕЛМРУ", repeat=4)]

count = 0
for i in arr:
    count += 1
    if i[0] == "Л":
        print(count)
        exit(0)
