from itertools import permutations

arr = ["".join(c) for c in permutations("ИГРОК")]

answer = 0
for i in arr:
    if i[0] != "К" and "РОК" not in i:
        answer += 1

print(answer)
