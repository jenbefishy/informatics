from itertools import product

arr = ["".join(c) for c in product("КРЕСЛО", repeat=4)]

def isMatch(word):
    if word[0] in "КРСЛ" and word[-1] in "ЕО":
        return 1
    return 0


answer = 0
for i in arr:
    if isMatch(i):
        answer += 1

print(answer)
