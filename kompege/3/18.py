from itertools import product

arr = ["".join(c) for c in product("АГИЛМОРТ", repeat=4)]

count = 0
last = 0
for i in arr:
    count += 1
    if i.endswith("ИМ"):
        last = count

print(last)
