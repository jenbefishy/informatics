from itertools import product
arr = ["".join(c) for c in product("ВИШНЯ", repeat=6)]

def isMatch(word):
    if word.count("В") > 1:
        return 0

    if word[0] != "Ш" and word[-1] not in "ИЯ":
        return 1
    return 0

answer = 0
for i in arr:
    if isMatch(i):
        answer += 1

print(answer)
