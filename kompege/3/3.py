from itertools import product

arr = ["".join(c) for c in product("ПУЛЯ", repeat=6)]

answer = 0
for i in arr:
    if i.count("У") == 2:
        answer += 1
print(answer)
