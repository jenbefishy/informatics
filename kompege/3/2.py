from itertools import product
arr = ["".join(c) for c in product("ABCWXYZ", repeat=6)]

def isMatch(word):
    if word[0] in "WXYZ" and word[-1] in "WXYZ":
        for i in range(1, len(word) - 1):
            if word[i] in "WXYZ":
                return 0
        return 1
    return 0


answer = 0
for i in arr:
    if isMatch(i):
        answer += 1

print(answer)
