from itertools import product
arr = ["".join(c) for c in product("0123456789", repeat=3)]

answer = 0
for i in arr:
    if list(sorted(i)) == list(i) and i[0] != "0":
        answer += 1

print(answer)
