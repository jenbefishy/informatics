from itertools import permutations
arr = ["".join(c) for c in permutations("АБИКОЛУН")]

def isMatch(word):
    for i in range(len(word) - 1):
        if word[i] in "БКЛН" and word[i + 1] in "БКЛН":
            return 0
        if word[i] in "АИОУ" and word[i + 1] in "АИОУ":
            return 0
    return 1

answer = 0
for i in arr:
    if isMatch(i):
        answer += 1

print(answer)
