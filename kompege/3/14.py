from itertools import product
arr = ["".join(c) for c in product("ДЕЙКСТРА", repeat=6)]

def isMatch(word):
    if word.count("Й") != 1 or word[-1] == "Й":
        return 0
    
    for i in range(len(word) - 1):
        if word[i] == "Й" and word[i + 1] not in "ДКСТРЙ":
            return 0
        
    for i in word:
        if word.count(i) != 1:
            return 0

    return 1


answer = 0
for i in arr:
    if isMatch(i):
        answer += 1

print(answer)
