from itertools import product
arr = ["".join(c) for c in product("ВАЙФУ", repeat=4)]

def isMatch(word):
    if word[0] == "Й" or "ВФ" in word or "ФВ" in word:
        return 0
    for i in word:
        if word.count(i) != 1:
            return 0
    return 1

answer = 0
for i in arr:
    if isMatch(i):
        answer += 1
print(answer)
