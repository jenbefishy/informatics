from itertools import product
arr = list(product("ABCD", repeat=4))

answer = 0
for i in arr:
    if list(sorted(i)) == list(i):
        answer += 1

print(answer)
