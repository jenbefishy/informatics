from itertools import product

arr = ["".join(c) for c in product("ЛОДКА", repeat=4)]

answer = 0
for i in arr:
    if i.count("О") >= 2:
        answer += 1

print(answer)
