from itertools import product
arr = ["".join(c) for c in product("AB123", repeat=8)]

answer = 0
for i in arr:
    if i.count("B") + i.count("A") == 2:
        answer += 1

print(answer)
