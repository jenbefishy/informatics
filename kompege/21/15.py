line = open("24_1146.txt").readline()

answer = 10 ** 10
count = 0

for i in line:
    if i == "D":
        count += 1
    else:
        if count != 0:
            answer = min(count, answer)
        count = 0

print(answer)
    
