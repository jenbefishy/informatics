file = open("24_2419.txt")
line = file.readline()

answer = 0
count = 0
for i in line:
    if i == "C":
        count += 1
        answer = max(answer, count)
    else:
        count = 0

print(answer)
