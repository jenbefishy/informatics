line = open("24_865.txt").readline()

answer = 0
count = 0
for i in line:
    if i in "CF":
        count = 0
    else:
        count += 1
        answer = max(count, answer)

print(answer)
