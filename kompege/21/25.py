line = open("24_2425.txt").readline()

prev = ""
count = 0
answer = 0

for i in line:
    if (prev == "D" and i == "B") or \
       (prev == "B" and i == "A") or \
       (prev == "A" and i == "C") or \
       (prev == "C" and i == "D"):
        count += 1
        answer = max(count, answer)
    else:
        count = 1
    prev = i

print(answer)
