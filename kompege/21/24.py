line = open("24_5171.txt").readline()

prev = "C"
count = 0
answer = 0

for i in line:
    if (prev == "C" and i == "A") or \
       (prev == "A" and i == "C"):
        count += 1
        answer = max(count, answer)
    else:
        count = 1
    prev = i

print(answer)
