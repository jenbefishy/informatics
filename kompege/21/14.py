line = open("24_1145.txt").readline()

isOpened = False
answer = 10 ** 10
count = 0

for i in line:
    if isOpened:
        count += 1
        
    if i == "D":
        if isOpened == False:
            isOpened = True
            count = 1
        else:
            if count != 2:
                answer = min(count, answer)
            count = 0
            isOpened = False
    
            
print(answer)


                
            
