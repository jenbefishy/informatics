file = open("24_2421.txt")

answer = 0
count = 0
for i in file.readline():
    if i == "D":
        count = 0
    else:
        count += 1
        answer = max(count, answer)

print(answer)
