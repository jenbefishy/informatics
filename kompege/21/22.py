line = open("24_4643.txt").readline()

i = 0
count = 0
answer = 0

while i < len(line) - 2:
    if line[i] in "012" and line[i + 1] in "012" and line[i + 2] in "AB":
        i += 3
        count += 1
        answer = max(count, answer)
    else:
        count = 0
        i += 1

print(answer)
        
       
    
