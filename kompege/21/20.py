line = open("24_4642.txt").readline()

i = 0
answer = 0
count = 0

while i < len(line) - 1:
    if line[i] in "AB" and line[i + 1] in "12":
        i += 2
        count += 1
        answer = max(count, answer)
    else:
        count = 0
        i += 1

print(answer)
