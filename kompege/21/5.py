line = open("24_2426.txt").readline()

count = 0
answer = 0
for i in line:
    if i in "123":
        count += 1
        answer = max(answer, count)
    else:
        count = 0

print(answer)
