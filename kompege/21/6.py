line = open("24_1040.txt").readline()

count = 0
answer = 0
for i in line:
    if i in "0123456789":
        count += 1
        answer = max(count, answer)
    else:
        count = 0

print(answer)
