line = open("24_4627.txt").readline()

arr = ["NPO", "PNO"]
answer = 0
i = 0
count = 0

while i < len(line) - 2:
    if (line[i] + line[i + 1] + line[i + 2]) in arr:
        i += 3
        count += 1
        answer = max(count, answer)
    else:
        count = 0
        i += 1

print(answer)
