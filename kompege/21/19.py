line = open("24_4602.txt").readline()

i = 0
count = 0
answer = 0

while i < len(line) - 1:
    if line[i] in "BCD" and line[i + 1] in "AO":
        count += 1
        answer = max(count, answer)
        i += 2
    else:
        count = 0
        i += 1
print(answer)
