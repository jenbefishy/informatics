line = open("24_2251.txt").readline()
line = line.split("D")

answer = 0
for a, b, c in zip(line, line[1:], line[2:]):
    s = a + "D" + b + "D" + c
    answer = max(len(s), answer)

print(answer)
