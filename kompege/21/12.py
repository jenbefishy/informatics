line = open("24_2423.txt").readline()

prev   = ""
count  = 0
answer = 0

for i in line:
    if i > prev:
        count += 1
        answer = max(count, answer)
    else:
        count = 1
    prev = i
print(answer)
