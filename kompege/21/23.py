line = open("24_4546.txt").readline()

count = 0
answer = 0

for i in range(3):
    for j in range(i, len(line) - 2, 3):
        if line[j] + line[j + 2] == "AA" or \
           line[j] + line[j + 2] == "CC":
            count += 1
            answer = max(count, answer)
        else:
            count = 0

print(answer)
