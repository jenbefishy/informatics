line = open("24_2250.txt").readline()

answer = 0
s = ""

for i in range(len(line)):
    for j in range(i + 1, len(line)):
        if line[i:j+1].count("A") <= 1:
            answer = max(j - i + 1, answer)
        else:
            break

print(answer)
        
