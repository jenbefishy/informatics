line = open("24_21.txt").readline()

count = 0
answer = 0
prev = ""
for i in line:
    if prev == i:
        count = 1
    else:
        count += 1
        answer = max(count, answer)
    prev = i

print(answer)
