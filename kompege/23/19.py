
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))

for n in range(113_000_000, 114_000_000):
    t = n

    if n % 2 != 0 :
        continue

    n //= 2
    p = n ** 0.5

    if int(p) == p and len(getDivs(p)) == 2:
        p = int(p)
        print(t, p * 2)

