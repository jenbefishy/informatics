
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))


count = 0 
for i in range(500_000, 500_000_000):
    s = getDivs(i)
    arr = [x for x in s \
            if str(x).endswith("8") and x != 8]

    if arr:
        print(i, min(arr))
        count += 1

    if count == 5:
        break
