
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(res)

for i in range(81234, 134689 + 1):
    s = getDivs(i)
    if len(s) == 3:
        print(*sorted(s))


