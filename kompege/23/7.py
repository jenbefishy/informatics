
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))

count = 0
for i in range(400_000_000, 400_000_000_000):
    s = getDivs(i)

    if len(s) < 5:
        continue

    P = 1
    for j in range(0, 5):
        P *= s[j]

    if str(P).endswith("17") and P <= i:
        count += 1
        print(P, s[4])

    if count == 5:
        break
