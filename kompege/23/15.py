
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))

count = 0
for i in range(500_000, 0, -1):
    s = getDivs(i)
    arr = []

    for j in s:
        if j != 1 and j != i:
            if len(getDivs(j)) == 2:
                arr += [j]

    if sum(arr) != 0 and sum(arr) % 10 == 0:
        print(i, sum(arr))
        count += 1

    if count == 7:
        break
