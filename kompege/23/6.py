
def getDels(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return list(res)


count = 0
for i in range(550_000, 550_000_000):
    s = getDels(i)

    if len(s) == 0:
        continue

    F = sum(s) // len(s)
    if F % 31 == 13:
        count += 1
        print(i, F)

    if count == 5:
        break


