
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))


for i in range(25317, 51237):
    s = getDivs(i)
    arr = []

    for j in s:
        if j != 1 and j != i:
            if len(getDivs(j)) == 2:
                arr += [j]

    if len(arr) >= 6:
        print(i, max(arr))
