
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))


for i in range(190201, 190260 + 1):
    s = getDivs(i)
    arr = [x for x in s if x % 2 == 0]

    if len(arr) == 4:
        arr.sort()
        print(arr[-1], arr[-2])


