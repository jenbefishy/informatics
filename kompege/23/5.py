
def getDels(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return list(sorted(res))

count = 0
for i in range(250_200, 250_200_000):
    s = getDels(i)

    if len(s) == 0:
        continue

    if (min(s) + max(s)) % 123 == 17:
        print(i, min(s) + max(s))
        count += 1

    if count == 5:
        break
