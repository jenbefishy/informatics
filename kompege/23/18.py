
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))

for n in range(55_000_000, 60_000_000):

    t = n
    while n % 2 == 0:
        n //= 2

    p = n ** 0.25

    if p == int(p) and len(getDivs(int(p))) == 2:
        p = int(p)
        print(t, p ** 4)


