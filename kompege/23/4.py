
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return sorted(list(res))

count = 0
for i in range(150_000, 150_000_000):
    s = getDivs(i)
    if sum(s) % 13 == 10:
        count += 1
        print(i, sum(s))

    if count == 7:
        break


