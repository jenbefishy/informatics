
def getDels(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return list(res)

res = []
for i in range(174457, 174505 + 1):
    s = getDels(i)
    if len(s) == 2:
        res.append([s[0] * s[1], s[0], s[1]])
res.sort()
for i in res:
    ans = sorted([i[1], i[2]])
    print(*ans)
