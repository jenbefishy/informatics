
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))

for i in range(125697, 125721):
    s = getDivs(i)

    if len(s) != 4:
        continue

    if len(getDivs(s[1])) == 2 and \
        len(getDivs(s[2])) == 2:
        print(s[1], s[2])
