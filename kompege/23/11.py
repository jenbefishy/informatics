
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))

count =  0
for i in range(300_000, 300_000_000):
    s = getDivs(i)
    arr = [x for x in s if x % 3 == 0]

    if len(arr) == 5:
        print(i, max(arr))
        count += 1

    if count == 4:
        break
