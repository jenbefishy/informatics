
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))


for i in range(1204300, 1204380 + 1):
    s = getDivs(i)
    arr = [x for x in s if x %  2 == 0]

    if sum(arr) != 0 and sum(arr) % 10 == 0:
        print(i, sum(arr))
