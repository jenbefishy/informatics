
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))

for i in range(6080068, 6080176 + 1):
    s = getDivs(i)
    if len(s) == 2:
        print(i)
