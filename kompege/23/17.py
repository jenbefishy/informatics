
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))


for p in range(0, 1000000 + 1):
    s = getDivs(p)

    if len(s) == 2 and \
            106732567 <= p ** 4 <= 152673836:
        print(p ** 4, p ** 3)

