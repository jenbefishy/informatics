
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return sorted(list(res))

for i in range(154026, 154044):
    s = getDivs(i)
    if len(s) == 4:
        print(s[2], s[3])
