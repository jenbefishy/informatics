
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return list(sorted(res))


count = 0
for i in range(550_000, 550_000_000):
    s = getDivs(i)
    arr = [x for x in s if str(x).endswith("7")]

    if len(arr) == 3:
        count += 1
        print(i, max(arr))

    if count == 5:
        break
