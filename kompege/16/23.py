from functools import lru_cache

@lru_cache(None)
def f(n):
    if n > 100_000:
        return n
    return f(n + 1) + 5 * n + 2

for n in range(100_001, 0, -1):
    f(n)

print(f(3) - f(7))
