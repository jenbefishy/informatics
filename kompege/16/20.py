
def f(n):
    if n < 3:
        return n + 1
    if n % 2 == 0:
        return f(n - 2) + n - 2
    return f(n + 2) + n + 2

answer = 0
for n in range(1, 1000):
    try:
        if len(str(f(n))) == 5:
            answer += 1
    except:
        continue

print(answer)
