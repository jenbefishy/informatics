from functools import lru_cache

@lru_cache(None)
def f(n):
    if n > 30:
        return n * n + 5 * n + 4
    if n % 2 == 0:
        return f(n + 1) + 3 * f(n + 4)
    return 2 * f(n + 2) + f(n + 5)

answer = 0
for n in range(1, 1000 + 1):
    s = f(n)

    res = 0
    for i in str(s):
        res += int(i)

    if res == 27:
        answer += 1


print(answer)
    
