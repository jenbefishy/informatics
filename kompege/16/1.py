
def f(x):
    if x == 0:
        return 1
    if x == 1:
        return 3
    if x == 2:
        return 2

    return f(x - 1) * f(x - 3)

print(f(7))
