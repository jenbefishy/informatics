from functools import lru_cache

@lru_cache(None)
def f(n):
    if n <= 18:
        return n + 3
    if n % 3 == 0:
        return (n // 3) * f(n // 3) + n - 12
    return f(n - 1) + n * n + 5


answer = 0
for n in range(1, 1000 + 1):
    s = str(f(n))

    if all(int(x) % 2 == 0 for x in s):
        answer += 1

print(answer)
