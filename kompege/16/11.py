from functools import lru_cache

@lru_cache(None)
def f(n):
    if n == 1:
        return 1
    return f(n - 1) - 2 * g(n - 1)


@lru_cache(None)
def g(n):
    if n == 1:
        return 1
    return f(n - 1) + g(n - 1) + n

s = str(g(36))
answer = 0
for i in s:
    answer += int(i)

print(answer)
