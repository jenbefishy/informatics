data = [int(x) for x in open("17_2309.txt")]


k11 = 0
k17 = 0
m11 = 10 ** 10
m17 = 10 ** 10

for i in data:
    if i % 11 == 0:
        k11 += 1
        m11 = min(i, m11)

    if i % 17 == 0:
        k17 += 1
        m17 = min(i, m17)

if k11 > k17:
    print(k11, m11)
else:
    print(k17, m17)

