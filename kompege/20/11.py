
data = [int(x) for x in open("17_2002.txt")]

count = 0
answer = 10 ** 10

for a, b, c, d in zip(data, data[1:], data[2:], data[3:]):
    if a>=b>=c>=d and a - d > 1000:
        count += 1
        answer = min(answer, a + b + c + d)

print(count, answer)
