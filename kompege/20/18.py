data = [int(x) for x in open("17_2403.txt")]

count  = 0
answer = 0

for a, b in zip(data, data[1:]):


    if (abs(a) % 9 == 0 and oct(b)[-1] == "3" and abs(b) % 9 != 0) or \
            (abs(b) % 9 == 0 and oct(a)[-1] == "3" and abs(a) % 9 != 0):
        count +=  1
        answer = max(answer, a, b)


print(count, answer)
