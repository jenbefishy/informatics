data = [int(x) for x in open("17_2017.txt")]

count = 0
answer = 0

for i in data:
    if hex(i).endswith("b") and (i % 7 == 0) and \
            (i % 6 != 0) and (i % 13 != 0) and (i % 19 != 0):
        count += 1
        answer += i

print(answer, count)

