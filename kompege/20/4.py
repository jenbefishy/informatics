
data = [int(x) for x in open("17_2016.txt")]

count = 0
minV = 10 ** 10
maxV = 0

for i in data:
    if (i % 13 == 7) and (i % 7 != 0) and (i % 11 != 0):
        count += 1
        minV = min(i, minV)
        maxV = max(i, maxV)

print(maxV - minV, count)
