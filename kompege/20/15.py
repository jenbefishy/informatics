data = [int(x) for x in open("17_2239.txt")]

count = 0
answer = 10 ** 10
maxV = max(x for  x in data if x % 19 == 0)

for a, b in zip(data, data[1:]):
    if a > maxV or b > maxV:
        count += 1
        answer = min(a + b, answer)

print(count, answer)

