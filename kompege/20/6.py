
data = [int(x) for x in open("17_1993.txt")]

count = 0
answer = 0

for a, b in zip(data, data[1:]):
    if ((a + b) % 3 == 0) and ((a + b) % 6 != 0) and (abs(a * b) % 10 == 8):
        count += 1
        answer = max(answer, a + b)

print(count, answer)

