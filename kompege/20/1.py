data = [int(x) for x in open("17_2003.txt")]


count = 0
answer = 0

for i in data:
    if (i % 3 == 0) and \
            (i % 7 != 0) and (i % 17 != 0) and (i % 19) != 0 and (i % 27 != 0):
        count += 1
        answer = max(answer, i)

print(count, answer)
