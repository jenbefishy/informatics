data = [int(x) for x in open("17_2402.txt")]

count = 0
answer = 0

numbers = "0123456789"
def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res


for a, b, c in zip(data[0:], data[1:], data[2:]):
    if toBase(a, 3).endswith("2") or toBase(b, 3).endswith("2") \
            or toBase(c, 3).endswith("2"):
        count += 1
        answer += min(a, b, c)

print(count, answer)

