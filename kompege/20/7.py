
data = [int(x) for x in open("17_1994.txt")]

count = 0
answer = 10 ** 10

for a, b in zip(data, data[1:]):
    if (a * b > 0) and (abs(a + b) % 7 == 0):
        count += 1
        answer = min(a * b, answer)

print(count, answer)
