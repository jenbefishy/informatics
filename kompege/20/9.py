data = [int(x) for x in open("17_1999.txt")]

count = 0
answer = 10 ** 10

for a, b, c in zip(data, data[1:], data[2:]):


    if ((abs(a) % 12 == 0) or (abs(b) % 12 == 0) or (abs(c) % 12 == 0)) and \
            ((abs(a) % 3 == 0) and (abs(b) % 3 == 0) and (abs(c) % 3 == 0)):
            count += 1
            answer = min(answer, (a + b + c) // 3)

print(count, answer)

