data = [int(x) for x in open("17_2310.txt")]

count = 0

k4 = []
for i in data:
    if i % 10 == 4:
        k4.append(i)

s = min(k4) + max(k4)
answer = 0

for a, b in zip(data, data[1:]):
    if a + b < s:
        count += 1
        answer = max(answer, a + b)

print(count, answer)
