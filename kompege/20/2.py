data = [int(x) for x in open("17_2013.txt")]

count  = 0
answer = 10 ** 10

for i in data:
    if (i % 10 == 2 or i % 10 == 7) and (i % 3 == 0) and (i % 11) == 0:
        count += 1
        answer = min(answer, i)

print(count, answer)

