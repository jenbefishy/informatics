data = [int(x) for x in open("17_2398.txt")]

count = 0
answer = 0

def isMatch(a):
    return (abs(a) % 10 == 9) and (a > 0)

for a, b, c in zip(data, data[1:], data[2:]):
    if (not isMatch(a)) and isMatch(b) and (not isMatch(c)):
        count += 1
        answer = max(answer, a + b + c)

print(count, answer)
