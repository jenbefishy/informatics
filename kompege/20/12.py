data = [int(x) for x in open("17_2400.txt")]

count = 0
answer = -10 ** 10

for a, b in zip(data, data[1:]):
    if (a + b >= 100) and (a < 0 or b < 0):
        count += 1
        answer = max(answer, a * b)

print(count, answer)
