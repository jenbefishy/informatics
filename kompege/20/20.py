data = [int(x) for x in open("17_2399.txt")]

s = 0
for i in data:
    if i % 35 == 0:
        for j in str(i):
            s += int(j)

answer = 10 ** 10
count = 0
for a, b in zip(data, data[1:]):
    if (a > s and hex(b).endswith("ef") and b <= s) or \
            (b > s and hex(a).endswith("ef") and a <= s):
        count += 1
        answer = min(answer, a + b)

print(count, answer)
