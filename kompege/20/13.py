
data = [int(x) for x in open("17_2401.txt")]

count = 0
answer = 10 ** 10

for a, b in zip(data, data[1:]):
    if 50 <= (abs(a) + abs(b)) <= 200:
        count += 1
        answer = min(answer, a, b)

print(count, answer)
