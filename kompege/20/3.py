
data = [int(x) for x in open("17_2015.txt")]

count  = 0
maxV = 0 
minV = 10 ** 10

for i in data:
    if (i % 10 == 5 or i % 10 == 7) and (i % 9 != 0) and (i % 11 != 0):
        count += 1
        maxV = max(i, maxV)
        minV = min(i, minV)

print(count, maxV + minV)

