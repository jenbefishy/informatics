
data = [int(x) for x in open("17_2238.txt")]


average = sum(data) / len(data)

count = 0
answer = 0

for a, b, c in zip(data, data[1:], data[2:]):
    t = 0
    if a > average: t += 1
    if b > average: t += 1
    if c > average: t += 1

    if t >= 2:
        count += 1
        answer = max(answer, a + b + c)

print(count, answer)
