
data = [int(x) for x in open("17_1998.txt")]

count  = 0
answer = 0

for a, b, c in zip(data, data[1:], data[2:]):
    if (abs(a * b * c) % 7 == 0) and (abs(a + b + c) % 10 == 5):
        count += 1
        answer = max(a + b + c, answer)

print(count, answer)
