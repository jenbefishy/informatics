from functools import lru_cache

@lru_cache(None)
def f(x, end, count):

    if x % 2 != 0:
        count += 1

    if x > end or count > 1:
        return 0

    if x == end :
        return count == 1

    return f(x + 1, end, count) + f(x + 2, end, count) + f(x * 2, end, count)
    
print(f(2, 40, 0))

    
