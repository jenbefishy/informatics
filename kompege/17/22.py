
def f(x, end, s):
    if x > end:
        return 0
    if x == end:
        if s.count("3") == 1:
            return 1
        return 0

    return f(x + 1, end, s + "1") + \
           f(x + 2, end, s + "2") + \
           f(x * 2, end, s + "3")

print(f(2, 12, ""))
