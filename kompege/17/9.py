
def f(x, end):
    if x < end:
        return 0
    if x == end:
        return 1

    return f(x - 8, end) + f(x // 2, end)

print(f(102, 43) * f(43, 5))
