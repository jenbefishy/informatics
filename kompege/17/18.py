
def f(x, end, path):
    if x > end:
        return 0

    if x == end:
        if len(path) == 7:
            return 1
        return 0

    return f(x + 1, end, path + [x + 1]) + \
        f(x + 4, end, path + [x + 4]) + f(x * 2, end, path + [x * 2])

print(f(3, 27, []))
