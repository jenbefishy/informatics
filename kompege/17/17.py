
def f(x, end):
    if x > end:
        return 0
    if x == end:
        return 1
    return f(x + 2, end) + f(x + 4, end)+ f(x + 5, end)

for n in range(32, 10000):
    if f(31, n) == 1001:
        print(n)
        break
