
def f(x, end):
    if x > end:
        return 0

    if x == end:
        return 1

    if x % 2 == 0:
        return f(x + 1, end) + f(int(x * 1.5), end)
    else:
        return f(x + 1, end)

print(f(1, 20))
