
def f(x, end):
    if x > end:
        return 0
    if x == end:
        return 1

    s1 = 0
    for i in str(x):
        if int(i) == 9:
           s1 = s1 * 10 + int(i) 
        else:
            s1 = s1 * 10 + int(i) + 1

    return f(x + 1, end) + f(s1, end)

print(f(25, 51))
