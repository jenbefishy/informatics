
def f(x, end, s):
    if x > end:
        return 0

    if x == end:
        return "33" not in s

    return f(x + 1, end, s + "1") + \
           f(x + 2, end, s + "2") + \
           f(x * 2, end, s + "3") 

print(f(1, 15, ""))
    
