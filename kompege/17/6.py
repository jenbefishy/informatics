
def f(x, end, path):
    if x > end:
        return 0
    if x == end:
        if 10 in path:
            return 1
        return 0
    return f(x + 1, end, path + [x + 1]) + f(x * 2, end, path + [x * 2])


print(f(1, 20, []))


    
