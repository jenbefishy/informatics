
def f(x, end, s):
    if x > end:
        return 0

    if x == end:
        if s.count("2") <= 3:
            return 1
        return 0

    return f(x + 2, end, s + "1") + \
           f(x * 3, end, s + "2") + \
           f(x * 5, end, s + "2")

print(f(2, 200, ""))
    
