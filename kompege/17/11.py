
def f(x, end):

    if x == 11 or x == 18:
        return 0

    if x > end:
        return 0
    if x == end:
        return 1

    return f(x + 1, end) + f(x + 2, end) + f(x * 3, end)


print(f(4, 8) * f(8, 23))
