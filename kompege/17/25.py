
def f(x, end, count):
    if x % 2 == 0:
        count += 1
    
    if x > end or count > 6:
        return 0 

    if x == end:
        return count == 6

    return f(x + 1, end, count) + f(x + 3, end, count) + f(x + 5, end, count)

print(f(3, 25, 0))
