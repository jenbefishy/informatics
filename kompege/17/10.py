
def f(x, end, path):
    if x > end:
        return 0
    if x == end:
        if 25 in path and 6 not in path:
            return 1

        return 0
    return f(x + 2, end, path + [x + 2]) + f(x * 3, end, path + [x * 3])

print(f(1, 63, []))


