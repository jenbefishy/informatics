n, k = map(int, input().split())
a = list(map(int, input().split()))
b = list(map(int, input().split()))

for value in b:

    l = 0
    r = n - 1

    while l < r - 1:
        mid = (l + r) // 2
        if a[mid] < value:
            l = mid
        else:
            r = mid

    if abs(value - a[l]) <= abs(value - a[r]):
        print(a[l])
    else:
        print(a[r])
