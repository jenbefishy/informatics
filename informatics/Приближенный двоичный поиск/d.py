n, m = map(int, input().split())

a = list(map(int, input().split()))
b = list(map(int, input().split()))

for value in b:
    l = 0
    r = n
    while l < r - 1:
        mid = (l + r) // 2
        if value < a[mid]:
            r = mid
        else:
            l = mid

    if a[l] == value:
        if l - 1 >= 0 and a[l - 1] == value:
            print(l, l + 1)
        elif l + 1 < n and a[l + 1] == value:
            print(l, l + 1)
        else:
            print(l + 1, l + 1)
    else:
        print(0)

