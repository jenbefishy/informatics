n = int(input())

k = 2

res = []

if n == 1:
    res.append(1)
    
while n > 1:
    while n % k == 0:
        res.append(k)
        n //= k
    k += 1


print(*res)