n, m = map(int, input().split())


def getSum(n):
    res = 0
    while n:
        res += n % 10
        n //= 10
    return res


minValue = 10 ** (n - 1)
maxValue = 10 ** n - 1

res = 0
numberSum = 0

r = -1
if n * 9 > m:
    while minValue <= maxValue:
        numberSum += 1
        r += 1

        if numberSum > m:
            r += r % 10

        if r % 10 == 0:
            numberSum = getSum(minValue)
            r = 0

        if numberSum == m:
            res += 1
        minValue += 1

print(res)
