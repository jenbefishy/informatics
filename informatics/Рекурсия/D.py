n = str(input())

res = ''
if n[0] == '-':
    res = '-'
    n = n[1:]

nReverse = n[::-1]
parts = []


while nReverse:
    value = nReverse[0:3]
    if len(value) < 3:
        value = (3 - len(value)) * '0' + value[::-1]
    else:
        value = value[::-1]

    parts.append(value)
    nReverse = nReverse[3:]
parts.reverse()

for i in parts:
    if i == '000':
        res += '0'
    if i == '001':
        res += '1'
    if i == '010':
        res += '2'
    if i == '011':
        res += '3'
    if i == '100':
        res += '4'
    if i == '101':
        res += '5'
    if i == '110':
        res += '6'
    if i == '111':
        res += '7'

print(res)
