a, b = map(int, input().split())

res = []


def check(n):
    compare = sum([int(char) for char in str(n)])
    for i in range(2, 10):
        s = sum([int(char) for char in str(n * i)])
        if s != compare:
            return False

    return True


for i in range(a, b + 1):
    if check(i):
        res.append(i)

if len(res) != 0:
    print(*res)
else:
    print(0)