l, r = map(int, input().split())

res = []


def checkSimple(n):
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return False
    return n != 1


def check(n):
    while n > 0:
        if checkSimple(n):
            n //= 10
        else:
            return False
    return True
for i in range(l, r+1):
    if check(i):
        res.append(i)

if len(res) > 0:
    print(*res)
else:
    print(0)