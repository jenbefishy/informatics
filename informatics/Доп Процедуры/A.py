n = 0 - int(input())
res = ''
if n == 0:
    res = '0'

while n != 0:
    res += str(abs(n % -2))
    n //= -2

print(res[::-1])