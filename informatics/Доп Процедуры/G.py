a, b = map(int, input().split())

count = 0

while a * b != 0:
    k = a // b
    a -= b * k
    count += k
    a, b = max(a, b), min(a, b)

print(count)