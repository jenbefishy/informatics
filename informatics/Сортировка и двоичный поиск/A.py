n = int(input())
arr = list(map(int, input().split()))

found = False
for i in range(n):
    for j in range(n - 1):
        if arr[j+1] < arr[j]:
            arr[j], arr[j+1] = arr[j+1], arr[j]
            found = True
            print(*arr)

if not found: print(0)
