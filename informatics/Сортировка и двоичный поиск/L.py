v, d = map(int, input().split())
n = int(input())
resultTime = 0

for _ in range(n):
    arr = list(map(str, input().split()))
    dis = arr[0]
    hours = arr[1]
    hours, minutes = map(int, hours.split(':'))
    minutes += hours * 60
    resultTime += int(arr[0]) // v + d




print("{:2d}:{:2d}".format(resultTime * 2 // 60, resultTime * 2 % 60), resultTime)
"""

3 1 
1
100 00:01

"""