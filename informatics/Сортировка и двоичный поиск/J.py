n, k = map(int, input().split())

arr = []
for _ in range(n):
    arr.append(int(input()))

maxValue = sum(arr) // k

l = 1
r = maxValue + 1

while l < r:
    mid = (r + l) // 2
    count = 0
    for i in arr:
        count += i // mid
    if count >= k:
        l = mid + 1
    else:
        r = mid

print(l - 1)

