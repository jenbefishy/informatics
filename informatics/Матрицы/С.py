n, m = map(int, input().split())

arr = []
for i in range(n):
    mas = []
    for j in range(m):
        mas.append(0)
    arr.append(mas)

value = 1
row = 0

for i in range(n):
    for j in range(m):
        arr[i][j] = value
        value += 1

for i in range(1, n, 2):
    arr[i] = list(reversed(arr[i]))

for i in range(n):
    for j in range(m):
        print('{:3d}'.format(arr[i][j]), end=" ")
    print()
