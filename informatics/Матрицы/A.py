n, m = map(int, input().split())

minV = 99999999
maxV = -99999999

minR = 0
minC = 0

maxR = 0
maxC = 0

for i in range(n):
    inp = list(map(int, input().split()))
    for j in range(m):
        if inp[j] < minV:
            minR = i + 1
            minC = j + 1
            minV = inp[j]
        if inp[j] > maxV:
            maxR = i + 1
            maxC = j + 1
            maxV = inp[j]

print(minR, minC, minV)
print(maxR, maxC, maxV)