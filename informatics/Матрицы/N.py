n, m = map(int, input().split())
matrix = []
for i in range(n):
    arr = list(map(int, input().split()))
    matrix.append(arr)

count = 0

# n = 4
# m = 5
# matrix = [
#     [1, 1, 1, 1, 1],
#     [0, 0, 0, 0, 0],
#     [0, 0, 1, 1, 0],
#     [1, 0, 1, 1, 0],
# ]
for i in range(n):
    for j in range(m):
        if matrix[i][j] == 1:
            count += 1
            dots = [[i, j]]
            for k in dots:
                dotX = k[1]
                dotY = k[0]
                if dotX + 1 < m and matrix[dotY][dotX + 1] == 1:
                    matrix[dotY][dotX + 1] = 0
                    dots.append([dotY, dotX + 1])
                if dotY + 1 < n and matrix[dotY + 1][dotX] == 1:
                    matrix[dotY + 1][dotX] = 0
                    dots.append([dotY + 1, dotX])

# for i in range(n):
#     for j in range(m):
#         print('{:3d}'.format(matrix[i][j]), end=" ")
#     print()

print(count)
