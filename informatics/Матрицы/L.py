n, m = map(int, input().split())

matrix = []

for i in range(m):
    arr = []
    for _ in range(n):
        arr.append(0)
    matrix.append(arr)

for i in range(n):
    arr = list(map(int, input().split()))
    el = 0
    for j in range(m):
        matrix[j][i] = arr[el]
        el += 1

for i in range(m):
    for j in range(n):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
