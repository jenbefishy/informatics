n, m = map(int, input().split())

matrix = []

for i in range(n):
    arr = []
    for _ in range(m):
        arr.append(0)
    matrix.append(arr)

step = 1
value = 2
positionX = 0
positionY = 0
matrix[0][0] = 1
while value < m * n:
    if step == 1:
        while positionX < m - 1 and matrix[positionY][positionX + 1] == 0:
            positionX += 1
            matrix[positionY][positionX] = value
            value += 1
        step = 2
    if step == 2:
        while positionY < n - 1 and matrix[positionY + 1][positionX] == 0:
            positionY += 1
            matrix[positionY][positionX] = value
            value += 1
        step = 3

    if step == 3:
        while positionX > 0 and matrix[positionY][positionX - 1] == 0:
            positionX -= 1
            matrix[positionY][positionX] = value
            value += 1
        step = 4

    if step == 4:
        while positionY > 0 and matrix[positionY - 1][positionX] == 0:
            positionY -= 1
            matrix[positionY][positionX] = value
            value += 1
        step = 1
    else:
        break

for i in range(n):
    for j in range(m):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
