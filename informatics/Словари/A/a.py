data = {}
file = open('input.txt')

for line in file:
    command = line.split()
    script = command[0]

    if script == 'DEPOSIT':
        name = command[1]
        money = int(command[2])

        if name in data:
            data[name] += money
        else:
            data[name] = money

    elif script == 'WITHDRAW':
        name = command[1]
        money = int(command[2])
        if name in data:
            data[name] -= money
        else:
            data[name] = -money

    elif script == 'BALANCE':
        name = command[1]
        if name in data:
            print(data[name])
        else:
            print('ERROR')

    elif script == 'TRANSFER':
        name1 = command[1]
        name2 = command[2]
        money = int(command[3])

        if name1 in data:
            data[name1] -= money
        else:
            data[name1] = -money

        if name2 in data:
            data[name2] += money
        else:
            data[name2] = money

    elif script == 'INCOME':
        procent = int(command[1])
        for key in data:
            if data[key] > 0:
                data[key] = int(data[key] * ( 1 + procent / 100))