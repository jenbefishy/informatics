file = open('input.txt')

data = {}


for line in file:
    command = line.split()
    name = command[0]
    votes = int(command[1])
    if name in data:
        data[name] += votes
    else:
        data[name] = votes

for name in sorted(data):
    print(f"{name} {data[name]}")