n, m = map(int, input().split())

words = []
teams = []
teamResults = [0] * n

data = {}

for _ in range(m):
    command = input().split()
    team = int(command[0])
    word = command[1]

    if team in data:
        data[team] += 1
    else:
        data[team] = 1

    if word in data:
        data[data[word]] -= 1
        data[word] = team
    else:
        data[word] = team


for i in range(1, n):
    if i in data:
        print(f"{data[i]} ", end='')
    else:
        print('0 ', end="")

if n in data:
    print(data[n])
else:
    print(0)