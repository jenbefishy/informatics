n = int(input())
arr = list(map(int, input().split()))

minValue = min(arr)

res = []
for i in range(len(arr)):
    if arr[i] == minValue:
        res.append(i+1)

print(*res)