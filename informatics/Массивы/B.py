from random import randrange
a, b, n = map(int, input().split())

res = []

for _ in range(n):
    res.append(randrange(a, b+1))

print(*res)
print('{:.3f}'.format(sum(res) / n))