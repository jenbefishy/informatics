n = int(input())
arr = list(map(int, input().split()))

minValue = 9999999
maxValue = -1

for i in arr:
    if i % 2 == 0 and i > 0:
        minValue = min(i, minValue)
        maxValue = max(i, maxValue)

if minValue == 9999999:
    print(-1, -1)
else:
    print(minValue, maxValue)