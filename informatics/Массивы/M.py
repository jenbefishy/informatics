n = int(input())
arr = list(map(int, input().split()))

zero = []
big = []
small = []

for i in arr:
    if i < 0:
        small.append(i)
    elif i == 0:
        zero.append(i)
    else:
        big.append(i)

print(*big, *zero, *small)