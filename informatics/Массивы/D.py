from random import randint
a, b, n = map(int, input().split())

res = []
ans = []
for _ in range(n):
    res.append(randint(a,b))

pare = 1
minValue = 999999999
for i in range(n - 1):
    if minValue >= res[i+1] + res[i]:
        minValue = res[i+1] + res[i]
        pare = i+1

print(*res)
print(pare, pare+1)