n = int(input())

arr = [0] * n

arr[0] = 1
if n > 1:
    arr[1] = 2
if n > 2:
    arr[2] = 4

for i in range(3, n):
    arr[i] = arr[i-1] + arr[i-2] + arr[i-3]

if n == 1:
    print(1)
elif n == 2:
    print(2)
elif n == 3:
    print(4)
else:
    print(arr[-1])
