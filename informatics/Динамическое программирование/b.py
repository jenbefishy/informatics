n = int(input())
arr = list(map(int, input().split()))


steps = [0] * (n + 1)
if n >= 1:
    steps[1] = arr[0]
if n >= 2:
    steps[2] = arr[1]


for i in range(1, n):
    minVal = min(steps[i] + arr[i], steps[i-1] + arr[i])
    steps[i+1] = minVal

print(steps[-1])