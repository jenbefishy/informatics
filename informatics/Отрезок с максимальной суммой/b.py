n = int(input())

arr = []
p = [0] * (n + 1)

index = 1
maxValue = -999999
minIndex = 0
l = 0
r = 0

minValue = 9999999
leftArr = [0]

for i in range(n):
    number = int(input())
    p[index] = p[index - 1] + number

    if maxValue < p[index] - minValue:
        maxValue = p[index] - minValue
        l = minIndex
        r = index

    if p[index] <= minValue:
        minValue = p[index]
        minIndex = index

    leftArr.append(p[index])

    index += 1

print(l + 1, '\n', r, sep='')


"""
5
3
5
-10
-10
-10

"""