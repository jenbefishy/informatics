s = str(input())

res = 0

l = 0

symbol = '+'

for i in range(len(s)):
    if s[i] == '+' or s[i] == '-':
        if symbol == '+':
            res += int(s[l:i])
        else:
            res -= int(s[l:i])
        symbol = s[i]

        l = i+1

if symbol == '+':
    res += int(s[l:len(s)])
else:
    res -= int(s[l:len(s)])

print(res)