s = str(input())

numbers = '0123456789'

while ('*' in s) or ('/' in s):
    for i in range(len(s)):
        if s[i] == '*' or s[i] == '/':
            symbol = s[i]
            l1 = i - 1
            r1 = i - 1
            l2 = i + 1
            r2 = i + 1
            while l1 > 0 and s[l1 - 1] in numbers:
                l1 -= 1
            while r2 < len(s) - 1 and s[r2 + 1] in numbers:
                r2 += 1

            if symbol == '*':
                d = int(s[l1:r1 + 1]) * int(s[l2:r2 + 1])
            else:
                d = int(s[l1:r1 + 1]) // int(s[l2:r2 + 1])

            s = s[0: l1] + str(d) + s[r2 + 1:]
            break

res = 0
l = 0
symbol = '+'

for i in range(len(s)):
    if s[i] == '+' or s[i] == '-':
        if symbol == '+':
            res += int(s[l:i])
        else:
            res -= int(s[l:i])
        symbol = s[i]

        l = i + 1

if symbol == '+':
    res += int(s[l:len(s)])
else:
    res -= int(s[l:len(s)])

print(res)
