n, b = map(int, input().split())


def myFunc(n, b):
    value = ''
    if n < 0:
        value += '-'
    res = ''
    n = abs(n)
    while n > 0:
        res += str(n % b)
        n //= b
    return value + res[::-1]


print(myFunc(n, b))
