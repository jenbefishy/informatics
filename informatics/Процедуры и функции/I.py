arr = list(map(int, input().split()))

maxV = max(arr)
minV = min(arr)

arr.remove(maxV)
arr.remove(minV)

print(minV, maxV)
print(sum(arr) / 3)


