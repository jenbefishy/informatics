n = int(input())
symbols = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'III', 'II', 'I']
values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 3, 2, 1]

def myFunc(n):
    res = ''
    while n > 0:
        for i in range(len(values)):
            if n >= values[i]:
                res += symbols[i]
                n -= values[i]
                break;
    return res
print(myFunc(n))
                