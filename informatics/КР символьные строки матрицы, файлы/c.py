n = int(input())

res = ''
maxValue = 0

for _ in range(n):
    line = str(input()).split()
    if int(line[-1]) > maxValue:
        res = line[0] + ' ' + line[1] + '\n'
        maxValue = int(line[-1])
    elif int(line[-1]) == maxValue:
        res += line[0] + ' ' + line[1] + '\n'


print(res, end='')
print(maxValue)