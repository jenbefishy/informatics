n = int(input())
a = list(map(int, input().split()))
m = int(input())
b = list(map(int, input().split()))

resL = 0
resR = 0
ansDifference = 99999999

changed = False
if n > m:
    a, b = b, a
    n, m = m, n
    changed = True

for i in range(n):
    value = a[i]
    l = 0
    r = m - 1
    while l < r - 1:
        mid = (l + r) // 2
        if b[mid] < value:
            l = mid
        else:
            r = mid

    if abs(value - b[l]) < abs(value - b[r]):
        ans = l
    else:
        ans = r

    if abs(value - b[ans]) < ansDifference:
        ansDifference = abs(value - b[ans])
        resL = value
        resR = b[ans]

if changed:
    print(resR, resL)
else:
    print(resL, resR)


"""

3
1 2 3
2
4 5


3 
1 2 3
5 
7 8 9 10 11


"""