n, k = map(int, input().split())
trees = list(map(int, input().split()))


def isMatch(arr):
    return len(set(arr)) == k


l = 0
r = n

while True:
    if isMatch(trees[l + 1:r]):
        l += 1
        found = True
    if isMatch(trees[l:r - 1]):
        r -= 1
        found = True

    if not found:
        break
print(l + 1, r)
