n, dis = map(int, input().split())
arr = list(map(int, input().split()))

res = 0
l = 0
r = 1

while l < n - 1 and r < n:
    if arr[r] - arr[l] <= dis:
        r += 1
    else:
        l += 1
        res += n - r

print(res)
