from turtle import *
from math import *

const = 20

tracer(0)


def print_cord():
	k = tan(pi / 3)
	for x in range(-22, 22):
		for y in range(-22, 22):
			goto(x * const, y * const)
			dot(3, "blue")

for _ in range(15):
	forward(15 * const)
	right(120)
	
up()
print_cord()
mainloop()
