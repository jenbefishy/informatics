from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(2):
    goto(xcor() + 6 * c, ycor() + 2 * c)
    goto(xcor(), ycor() - 2 *c)

for _ in range(3):
    goto(xcor() + 2 * c, ycor() - 1 * c)
    goto(xcor() - 2 * c, ycor() - 1 * c)

for _ in range(6):
    goto(xcor() - 2 * c, ycor() + 1 * c)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
