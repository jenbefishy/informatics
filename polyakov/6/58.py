from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)
left(90)
c = 50

right(90)
forward(3 * c)
right(270)
for _ in range(12):
    forward(10 * c)
    right(216)
up()
for x in range(0, 20):
    goto(x * c, 0)
    dot(10, "blue")

for y in range(0, 20):
    goto(0, y * c)
    dot(10, "blue")

mainloop()