from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25

left(90)

for _ in range(11):
    forward(8 * c)
    left(120)



up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
