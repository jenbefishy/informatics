from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)
left(90)
c = 50
dot(15, "red")

for _ in range(2):
    forward(7 * c)
    right(60)
    forward(12 * c)
    right(120)
up()
forward(7 * c)
right(60)
down()
for _ in range(2):
    forward(5 * c)
    right(120)
    forward(10 * c)
    right(60)


up()
for x in range(-25, 25):
    for y in range(-25, 25):
        goto(x * c, y * c)
        dot(10, "blue")

mainloop()