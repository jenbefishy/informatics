from turtle import *
from math import tan, radians, sqrt

tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(10):
    left(60)
    forward(300)
    left(60)


up()

count = 0
k = tan(radians(30))
n = round(sqrt(300**2 - 150**2))
for x in range(n):
    for y in range(-300, 300):
        if y <= k * x  and y >= -k * x:
            count += 1
print(count)

mainloop()
