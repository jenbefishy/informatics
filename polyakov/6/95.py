from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)

c = 20
dot(15, "red")

for _ in range(2):
    left(120)
    down()
    for __ in range(10):
        right(30)
        forward(4 * c)
        right(60)
    up()
    left(150)
    backward(2 * c)
    left(90)
    backward(2 * c)

up()
for x in range(-20, 40):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()