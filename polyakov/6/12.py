from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25
left(90)

for _ in range(200):
    right(90)
    forward(50 * c)

up()

for x in range(-10, 100):
    for y in range(-100, 100):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
