from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

count = 0
for _ in range(151):
    forward(10 * c)
    right(300)
    forward(20 * c)
    right(300)

up()



for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

goto(0, 0)
dot(10, "red")

mainloop()
