from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

right(90)

for _ in range(10):
    forward(8 * c)
    left(72)

up()

for x in range(-50, 50):
    for y in range(0, 13):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
