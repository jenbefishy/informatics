from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(13):
    goto(xcor() + 6 * c, ycor() + 3 * c)
    goto(xcor() - 6 * c, ycor() + 2 * c)
    goto(xcor() - 4 * c, ycor() - 1 * c)
    goto(xcor() + 4 * c, ycor() - 4 * c)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
