from turtle import *
tracer(0)
const = 25

right(270)
def print_cord():
	for x in range(-25, 25):
		for y in range(-25, 25):
			goto(x * const, y * const)
			dot(3, "blue")

for _ in range(10):
	right(60)
	forward(10 * const)
	right(60)

up()
print_cord()

mainloop()
