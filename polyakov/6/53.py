from turtle import *
from math import tan, radians, sqrt

tracer(0)
screensize(15000, 15000)
c = 25
left(90)

dot(10, "red")
left(135)
for _ in range(25):
    forward(250)
    right(90)


up()

count = 0
k = tan(radians(45))
n = round(sqrt(250**2 + 250**2))
for x in range(-450, 450):
    for y in range(-450, 450):
        if y > x and y < -x and y < x + n and y > -x - n:
            count += 1
            
print(count)

mainloop()
