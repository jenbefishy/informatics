from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25

left(90)

forward(9 * c)
right(90)
for _ in range(2):
    forward(3 * c)
    right(90)
    forward(3 * c)
    right(270)

for _ in range(2):
    forward(3 * c)
    right(90)

forward(9 * c)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
