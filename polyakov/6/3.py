from turtle import *

const = 20
tracer(0)

right(270)

def print_cord():
	for x in range(-25, 25):
		for y in range(-25, 25):
			goto(x * const, y * const)
			if x > 0 and y > 0:
				dot(5, "red")
			else:
				dot(5, "blue")



for _ in range(15):
	forward(4 * const)
	right(60)
	
up()
print_cord()

mainloop()
