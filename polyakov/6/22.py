from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25

left(90)

right(30)
for _ in range(30):
    forward(7 * c)
    right(120)
    forward(7 * c)
    right(60)
    



up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
