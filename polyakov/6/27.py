from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(7):
    right(90)
    forward(4 * c)
    for __ in range(2):
        left(90)
        forward(4 * c)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
