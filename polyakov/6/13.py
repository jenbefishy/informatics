from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25

left(90)

forward(200 * c)
for _ in range(4):
    right(90)
    forward(100 * c)

up()

for x in range(-10, 205):
    for y in range(-10, 205):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
