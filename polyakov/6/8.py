from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25


for _ in range(15):
    goto(xcor() + 10 * c, ycor() + 10 * c)
    goto(xcor() + 3 * c, ycor() - 6 * c)
    goto(xcor() - 9 * c, ycor() + 3 * c)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
