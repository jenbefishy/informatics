from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)
left(90)
c = 50
dot(15, "red")

left(20)
for _ in range(27):
    left(70)
    forward(27 * c)
    left(200)

up()
for x in range(-30, 25):
    for y in range(-30, 30):
        goto(x * c, y * c)
        dot(10, "blue")

mainloop()