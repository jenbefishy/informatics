from turtle import *
tracer(0)
screensize(15000, 15000)
c = 10
left(90)

for _ in range(100):
    forward(10 * c)
    # 360 / 8 = 45 lines
    right(8)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
