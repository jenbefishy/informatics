from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)
left(90)
c = 20
dot(15, "red")

left(140)
for _ in range(23):
    right(230)
    forward(22 * c)
    right(90)
    forward(23 * c)
    right(220)
dot(15, "yellow")
up()
left(40)
backward(10 * c)
left(90)
backward(8 * c)
down()
dot(15, "green")
for _ in range(24):
    forward(24 * c)
    right(90)
    forward(90 * c)
    right(90)

up()
for x in range(-20, 40):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()