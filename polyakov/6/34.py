from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(5):
    goto(xcor() + 6 * c, ycor() + 8 * c)
    goto(xcor() - 8 * c, ycor() + 4 * c)
    goto(xcor() + 2 * c, ycor() - 12 * c)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
