from turtle import *
tracer(0)
const = 25

def print_cord():
	for x in range(-25, 25):
		for y in range(-25, 25):
			goto(x * const, y * const)
			dot(3, "blue")

for _ in range(36):
	right(60)
	forward(const)
	right(60)
	forward(const)
	right(270)

up()
print_cord()

mainloop()
