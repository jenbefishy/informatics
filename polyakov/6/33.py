from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(6):
    goto(xcor() - 5 * c, ycor() + 3 * c)
    goto(xcor() + 7 * c, ycor() + 4 * c)
    goto(xcor() + 8 * c, ycor() - 5 * c)
    goto(xcor() - 10 * c, ycor() - 2 * c)

up()

for x in range(-50, 50):
    for y in range(-50, 8):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
