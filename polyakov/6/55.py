from turtle import *
from math import tan, radians, sqrt

tracer(0)
screensize(15000, 15000)
c = 25
left(90)

dot(10, "red")
for _ in range(10):
    goto(xcor() + 200, ycor() + 100)
    goto(xcor() - 50, ycor() - 150)
    goto(xcor() - 150, ycor() + 50)



up()
k1 = 0.5
k2 = 3
k3 = -1/3


dots = set()
for x in range(-1000, 1000):
    for y in range(-1000, 1000):

        if y == k1 * x and y >= k2 * x - 500 and y >= k3 * x:
            dots.add((x, y))

        if y <= k1 * x and y == k2 * x - 500 and y >= k3 * x:
            dots.add((x, y))

        if y <= k1 * x and y >= k2 * x - 500 and y == k3 * x:
            dots.add((x, y))

print(len(dots))

mainloop()
