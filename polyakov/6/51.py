from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

dot(10, "red")
for _ in range(20):
    goto(xcor() + 10 * c, ycor() + 20 * c)
    goto(xcor() + 5 * c, ycor() - 15 * c)
    goto(xcor() - 12 * c, ycor() - 9 * c)

dot(10, "yellow")

up()
for x in range(-10, 125):
    for y in range(-125, 10):
        goto(x * c, y * c)
        dot(5, "blue")


mainloop()
