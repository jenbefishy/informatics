from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

left(30)
for _ in range(15):
    for __ in range(2):
        forward(5 * c)
        left(60)
    forward(5 * c)
    left(120)
    forward(10 * c)
    left(120)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
