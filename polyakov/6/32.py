from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(8):
    goto(xcor() + 3 * c, ycor() + 6 * c)
    goto(xcor() + 8 * c, ycor() - 5 * c)
    goto(xcor() - 5 * c, ycor() - 3 * c)
    goto(xcor() - 6 * c, ycor() + 2 * c)

up()

for x in range(-50, 11):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
