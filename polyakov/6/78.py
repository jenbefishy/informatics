from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)
left(90)
c = 50
dot(15, "red")

for _ in range(3):
    forward(10 * c)
    right(120)

up()
forward(10 * c)
right(90)
forward(3 * c)
down()
for _ in range(4):
    forward(10 * c)
    right(90)




up()
for x in range(-25, 25):
    for y in range(-25, 25):
        goto(x * c, y * c)
        dot(10, "blue")

mainloop()