from turtle import *
tracer(0)
hideturtle()
const = 50

def print_coord():
    for x in range(-22, 22):
        for y in range(-22, 22):
            goto(x * const, y * const)
            dot(5, "blue")

for _ in range(10):
    forward(const * 6)
    right(120)

up()
print_coord()

mainloop()

