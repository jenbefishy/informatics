from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(10):
    forward(15 * c)
    right(60)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
