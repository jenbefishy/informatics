from turtle import *
const = 25
tracer(0)


def print_cord():
	for x in range(-25, 25):
		for y in range(-25, 25):
			goto(x * const, y * const)
			dot(3, "blue")
left(90)

for _ in range(8):
	forward(12 * const)
	right(90)

up()
print_cord()

mainloop()
		
