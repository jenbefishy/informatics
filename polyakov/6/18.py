from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25

left(90)

right(30)
for _ in range(30):
    right(30)
    forward(3 * c)
    right(30)


up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
