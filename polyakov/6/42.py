from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(10):
    for __ in range(3):
        forward(10 * c)
        right(90)
        forward(10 * c)
        right(270)
    right(90)

up()

for x in range(-10, 80):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
