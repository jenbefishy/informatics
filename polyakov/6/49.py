from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

dot(10, "red")
for _ in range(13):
    goto(xcor() + 4 * c, ycor() + 3 * c)
    goto(xcor() - 5 * c, ycor() + 10 * c)
    goto(xcor() + 6 * c, ycor() - 6 * c)
    goto(xcor() - 5 * c, ycor() - 8 * c)

dot(10, "yellow")

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
