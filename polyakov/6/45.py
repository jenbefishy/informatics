from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(13):
    forward(10 * c)
    right(90)
    forward(10 * c)
    right(90)
    forward(30 * c)
    right(90)


up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
