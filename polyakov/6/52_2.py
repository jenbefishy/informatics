from turtle import *
from math import *
speed(0)

right(90)
for i in range(10):
    left(60)
    forward(300)
    left(60)
up()

ans = 0
k = tan(radians(30))
katet2 = round((300**2-150**2)**0.5)
for x in range(katet2):
    for y in range(-150,151):
        if y <= k*x and y>=-k*x:
            ans+=1
print('№ 52:',ans)
