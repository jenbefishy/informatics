from turtle import *
screensize(15000, 15000)
tracer(0)
const = 25

def print_cord():
	for x in range(0, 100):
		for y in range(-25, 100):
			goto(x * const, y * const)
			dot(3, "blue")

for _ in range(15):
	goto(xcor() + 10 * const, ycor() + 10 * const)
	goto(xcor() + 3 * const, ycor() - 6 * const)
	goto(xcor() - 9 * const, ycor() + 3 * const)

up()
print_cord()

mainloop()
