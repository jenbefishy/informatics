from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

dot(10, "red")

for _ in range(36):
    forward(10 * c)
    right(50)

dot(10, "yellow")

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
