from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)
left(90)
c = 50
dot(15, "red")

for _ in range(6):
    forward(5 * c)
    right(60)

up()
forward(5 * c)
right(90)

down()
for _ in range(2):
    forward(15 * c)
    right(90)
    forward(5 * c)
    right(90)


up()
for x in range(-25, 25):
    for y in range(-25, 25):
        goto(x * c, y * c)
        dot(10, "blue")

mainloop()