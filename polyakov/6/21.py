from turtle import *
screensize(15000, 15000)
tracer(0)
c = 25

left(90)

for _ in range(7):
    left(60)
    forward(5 * c)
    left(120)
    forward(5 * c)
    



up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
