from turtle import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)

for _ in range(5):
    forward(8 * c)
    right(120)
    for __ in range(2):
        forward(4 * c)
        right(60)

    forward(4 * c)
    right(120)

up()

for x in range(-50, 50):
    for y in range(-50, 50):
        goto(x * c, y * c)
        dot(5, "blue")

mainloop()
