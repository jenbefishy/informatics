from turtle import *
speed(150000)
tracer(0)
screensize(15000, 15000)
left(90)
c = 50

for _ in range(5):
    for __ in range(3):
        forward(2 * c)
        right(270)
    forward(4 * c)

up()
for x in range(-25, 25):
    for y in range(-25, 25):
        goto(x * c, y * c)
        dot(10, "blue")

mainloop()