from turtle import *
from math import *
tracer(0)
screensize(15000, 15000)
c = 25
left(90)


for _ in range(40):
    left(45)
    forward(400)
    right(90)

for y in range(0, 1000):
    goto(0, y)
    dot(5, "blue")

for x in range(0, 1000):
    goto(x, 0)
    dot(5, "blue")

count = 0
n = round(400 / sqrt(2))
h = 400 + 2 * n

dots = set()
for x in range(0, 3000):
    for y in range(0, 3000):
        if 0 < y < h  and 0 < x < 400 + n and y > x - 400 and y < -x + h + 400:
            dots.add((x, y))
print(len(dots))
up()

mainloop()
