
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase(8 ** 415 - 4 ** 162 + 2 ** 543 - 25, 2).count("1"))
