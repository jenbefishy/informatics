from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


answer = 0
for x in range(1, 1000):
    if fnmatch(toBase(x, 2), "10??????") and fnmatch(toBase(x, 8), "?4?") and \
        fnmatch(toBase(x, 16), "?2"):
        answer = x

print(answer)
            
