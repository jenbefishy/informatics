
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase(8 ** 1341 - 4 ** 1342 + 2 ** 1343 - 1344, 2).count("1"))
