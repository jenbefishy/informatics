numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []
for i in range(2, 37):
    if  len(toBase(256, i)) == 3 and toBase(256, i).endswith("4"):
        res.append(i)

print(min(res))
        
