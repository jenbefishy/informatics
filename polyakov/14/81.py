numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res
        
res = 0

for i in range(2, 1000):
    if len(toBase(i, 7)) == 2 and len(toBase(i, 6)) == 3 and toBase(i, 13).endswith("3"):
        res = i

print(res)
    
    

