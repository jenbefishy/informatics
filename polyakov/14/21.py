
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []

for i in range(1, 26):
    if toBase(i, 6).startswith("4"):
        res.append(i)

print(res)
