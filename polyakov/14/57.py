numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []
for i in range(2, 37):
    if  len(toBase(338, i)) == 3 and toBase(338, i).endswith("2"):
        res.append(i)

print(max(res))
        
