from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


print(len(toBase(2 ** 299 + 2 ** 298 + 2 ** 297 + 2 ** 296, 8)))
