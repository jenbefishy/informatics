from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


answer = 0

for x in range(0, 1000):
    if fnmatch(toBase(x, 16), "3?9") and fnmatch(toBase(x, 8), "1??"):
        answer += 1


print(answer)
            
