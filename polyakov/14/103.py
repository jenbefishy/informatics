
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

answer = 0
for i in range(5, 32):
    if int("441", i) + 14 == int("252", 7):
        answer = i
        break
        

print(toBase(answer, 2))
