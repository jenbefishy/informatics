
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase(4 ** 2015 + 8 ** 2016 - 2 ** 2017 - 150, 2).count("0"))
