
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase(4 ** 1024 + 8 ** 1025 - 2 ** 1026 - 140, 2).count("0"))
