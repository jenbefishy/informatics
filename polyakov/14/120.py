
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase(8 ** 740 - 2 ** 900 + 7, 2).count("0"))
