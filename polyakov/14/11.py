
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []

for i in range(1, 18):
    if len(toBase(i, 3)) >= 2 and toBase(i, 3)[-1] == toBase(i, 3)[-2]:
        res.append(i)

print(res)
