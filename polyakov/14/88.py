numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res
        
res = 0

for i in range(2, 37):
    if toBase(58, i).endswith("2") and toBase(108, i).endswith("3"):
        res = i
        break

print(res)
    

