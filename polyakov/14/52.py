numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []
for i in range(2, 37):
    if  toBase(int("34", 8), i).endswith("20"):
        res.append(i)

print(res)
        
