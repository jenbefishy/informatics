
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        if n % base < len(numbers):
            res = numbers[int(n % base)] + res
        else:
            res = "0" + res
        n //= base
    return res

res = []

for i in range(2, 76):
    if toBase(75, i).endswith("13"):
        res.append(i)

print(res)
