
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

answer = 0
for i in range(0, 32):
    if int("33", i + 4) - int("33", 4)  == int("33", 10):
        answer = i
        break
        

print(toBase(answer, 10))
