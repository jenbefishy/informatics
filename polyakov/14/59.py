numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []
for i in range(2, 37):
    if  len(toBase(325, i)) == 3 and toBase(325, i).endswith("1"):
        res.append(i)

print(min(res))
        
