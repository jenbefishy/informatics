
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = 0

for i in range(5, 36):
    if int("14", i) + int("42", i) == int("100", i):
        res = i

print(res)
