
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

answer = 0
for i in range(6, 32):
    if int("145", i) + 24 == int("127", 9):
        answer = i
        break
        

print(toBase(answer, 5))
