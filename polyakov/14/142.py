from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


for x in range(2, 1000):
    if int("101", x) + 13 == int("101", x + 1):
        print(x)
        break
