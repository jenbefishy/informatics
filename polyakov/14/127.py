from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


answer = 0
for x in range(1, 1000):
    if fnmatch(toBase(x, 16), "?7?") and fnmatch(toBase(x, 8), "5?6") and \
        fnmatch(toBase(x, 4), "???1?"):
        answer = x

print(answer)
            
