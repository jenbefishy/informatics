from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


print(str(100 ** 20 - 10 ** 15 + 10).count("0"))
