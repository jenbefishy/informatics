numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res
        
res = 0

for i in range(2, 1000):
    if len(toBase(i, 6)) == 2 and len(toBase(i, 5)) == 3 and toBase(i, 11).endswith("1"):
        res = i

print(res)
    
    

