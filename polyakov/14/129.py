from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


if int("A0", 16) > int("103", 8):
    print(">")
else:
    print("<")

            
