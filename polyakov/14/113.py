
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase(4 ** 350 + 8 ** 340 - 2 ** 320 - 12, 2).count("0"))
