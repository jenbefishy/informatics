
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase(8 ** 148 - 4 ** 123 + 2 ** 654 - 17, 2).count("1"))
