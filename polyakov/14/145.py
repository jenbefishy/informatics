from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


for x in range(9, 32):
    for y in range(8, 32):
        if int("87", x) == int("73", y):
            print(str(y) + str(x))
            exit(0)
            
