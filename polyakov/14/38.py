
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = 0

for i in range(5, 36):
    if int("144", i) + int("24", i) == int("201", i):
        res = i

print(res)
