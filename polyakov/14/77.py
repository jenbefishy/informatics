numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res
        
res = 0

for i in range(2, 37):
    if toBase(56, i).endswith("5") and toBase(124, i).endswith("5"):
        res = i
        break

print(res)
