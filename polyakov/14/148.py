from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


print(toBase(27 ** 4 - 9 ** 5 + 3 ** 8 - 25, 3).count("2"))
