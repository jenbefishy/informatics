numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []
for i in range(1, 1000):
    if toBase(i, 4).endswith("0") and toBase(i, 6).endswith("0"):
        res = i
        break

print(res)
        
