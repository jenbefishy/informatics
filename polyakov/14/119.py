
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

answer = 0
for i in range(3, 32):
    if int("121", i) + 1 == int("101", 9):
        answer = i

print(answer)
