
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []

for i in range(1, 1000):
    if toBase(i, 8)[-1] == toBase(i, 9)[-1] == "0":
        res = i
        break

print(res)
