
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

answer = 0
for i in range(3, 32):
    if int("222", i) + 4 == int("1100", 5):
        answer = i
        break
        

print(toBase(answer, 3))
