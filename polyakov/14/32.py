
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []

for i in range(1, 41):
    if toBase(i, 2).endswith("1011"):
        res.append(i)

print(res)
