
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

answer = 0
for i in range(5, 32):
    if int("224", i) + 1 == int("101", 8):
        answer = i

print(answer)
