numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []
for i in range(2, 37):
    if  len(toBase(180, i)) == 3 and toBase(180, i).endswith("0"):
        res.append(i)

print(res)
        
