
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res

print(toBase((2 ** 4400 - 1) * (4 ** 2200 + 2), 2).count("1"))
