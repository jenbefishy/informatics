
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        if n % base < len(numbers):
            res = numbers[int(n % base)] + res
        else:
            res = "0" + res
        n //= base
    return res

res = []

for i in range(20, 30):
    if toBase(i, 3).endswith("11"):
        res = i

print(res)
