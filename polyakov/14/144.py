from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


for x in range(5, 1000):
    if int("104", x) + int("20", x)  == 84:
        print(toBase(x, 2))
        break
