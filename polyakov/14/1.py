
numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[int(n % base)] + res
        n //= base
    return res

res = []

for i in range(2, 36):
    if toBase(22, i)[-1] == "4":
        res.append(i)

print(res)
