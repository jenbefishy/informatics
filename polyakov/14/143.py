from fnmatch import fnmatch

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
    res = ""
    while n:
        res = numbers[n % base] + res
        n //= base
    return res


for x in range(4, 1000):
    if int("103", x) + 11 == int("103", x + 1):
        print(x)
        break
