file = open("9-178.csv")

def isMatch(a):
    k = 1
    while k ** 3 < a:
        k += 1
        if k ** 3 == a:
            return 1
    return 0

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    repeats = [x for x in arr if arr.count(x) != 1]

    if len(repeats) != 0:
        continue

    a, b, c = arr[0], arr[1], arr[2]

    if isMatch(a * b) or isMatch(b * c) or isMatch(a * c):
        answer += 1

print(answer)
