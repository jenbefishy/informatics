file = open("9-170.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    repeat   = [x for x in arr if arr.count(x) == 2]
    noRepeat = [x for x in arr if arr.count(x) == 1]

    try:
        if len(repeat) >= 2 and min(repeat) > max(noRepeat):
            answer += 1
    except:
        answer += 1
print(answer)
