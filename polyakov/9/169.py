file = open("9-169.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    s = (max(arr) + min(arr))/2

    if s in arr:
        answer += 1

print(answer)
