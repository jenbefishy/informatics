file = open("9-176.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    repeats = [x for x in arr if arr.count(x) != 1]
    noRepeat = [x for x in arr if arr.count(x) == 1]

    if len(repeats) != 0 and sum(noRepeat) % 2 != 0:
        answer += 1

print(answer)
