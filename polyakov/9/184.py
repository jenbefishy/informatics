
file = open("9-183.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    arr.sort()
    a, b, c = arr[0], arr[1], arr[2]

    if c ** 2 == a ** 2 + a * b + b ** 2 and c < a + b:
        answer += 1

print(answer)
