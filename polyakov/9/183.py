file = open("9-183.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    arr.sort()
    
    a, b, c = arr[0], arr[1], arr[2]
    p = (a + b + c) / 2

    if c < a + b and a < b + c and c < a + b:
        S = (p * (p - a) * (p - b) * (p - c)) ** 0.5
        if int(S) == S:
            answer += 1

print(answer)
