file = open("9-170.csv")

def mulArr(arr):
    res = 1
    for i in arr:
        res *= i
    return res

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]

    repeat = [x for x in arr if arr.count(x) == 3]
    noRepeat = [x for x in arr if arr.count(x) == 1]

    if len(noRepeat) + len(repeat) == len(arr) and 3 * sum(noRepeat) <= mulArr(repeat):
        answer += 1

print(answer)
