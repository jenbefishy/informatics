file = open("9-181.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    arr.sort()

    a, b, c, d = arr[0], arr[1], arr[2], arr[3]

    if a * b * c % d == 0:
        answer += 1

print(answer)
