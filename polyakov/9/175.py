file = open("9-170.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    repeats = [x for x in arr if arr.count(x) != 1]
    noRepeats = [x for x in arr if arr.count(x) == 1]

    if len(repeats) == 2 and max(noRepeats) + min(noRepeats) <= sum(repeats):
        answer += 1

print(answer)
