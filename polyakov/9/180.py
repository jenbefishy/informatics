file = open("9-180.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    repeats = [x for x in arr if arr.count(x) > 1]

    if repeats == []:
        continue
    
    a, b, c, d = arr[0], arr[1], arr[2], arr[3]
    answer = max(answer, \
            a + b + c +  \
            a + b + d +  \
            b + c + d +  \
            d + c + a)

print(answer)
