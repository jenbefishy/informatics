file = open("9-182.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    a, b, c = arr[0], arr[1], arr[2]

    D = b ** 2 - 4 * a * c
    if D > 0 and a != 0:
        answer += 1

print(answer)
