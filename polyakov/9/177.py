file = open("9-177.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    arr.sort()
    s1 = (arr[0] + arr[7]) ** 2
    s2 = arr[1] ** 2 + arr[2] ** 2 \
            + arr[3] ** 2 + arr[4] ** 2 + arr[5] ** 2 + arr[6] ** 2
    
    repeats = [x for x in arr if arr.count(x) != 1]
    
    if s1 > s2 and len(repeats) != 0:
        answer += 1

print(answer)
