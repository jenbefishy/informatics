file = open("9-187.csv")

answer = 0
for i in file:
    arr = [x.replace("\n", "") for x in i.split(",")]

    a1 = [x for x in arr if arr.count(x) == 2]
    a2 = [x for x in arr if arr.count(x) != 2]

    if len(a1) == 2:
        answer += 1
print(answer)
    

