file = open("9-188.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    a1 = arr[0] * arr[1]
    a2 = arr[0] * arr[2]
    a3 = arr[1] * arr[2]

    if a1 % 10 == 4 or a2 % 10 == 4 or a3 % 10 == 4:
        answer += 1

print(answer)


