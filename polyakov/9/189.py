file = open("9-189.csv")

def mulArr(arr):
    res = 1
    for i in arr:
        res *= i
    return res


answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    maxValue = max(arr)
    arr.remove(maxValue)

    if maxValue ** 2 > mulArr(arr):
        answer += 1

print(answer)
