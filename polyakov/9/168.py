file = open("9-168.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    average = sum(arr) / len(arr)
    count = 0
    for i in arr:
        if i > average:
            count += 1

    if count >= 3:
        answer += 1

print(answer)
