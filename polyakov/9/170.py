file = open("9-170.csv")

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]

    repeat = [x for x in arr if arr.count(x) == 2]
    noRepeat = [x for x in arr if arr.count(x) == 1]

    if len(repeat) + len(noRepeat) == len(arr) and \
            len(repeat) == 2 and sum(noRepeat) / len(noRepeat) <= sum(repeat):
        answer += 1

print(answer)
