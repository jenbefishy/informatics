
file = open("9-183.csv")


answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]
    arr.sort()

    a1, a2, a3 = arr[0], arr[1], arr[2]

    if a3 < a2 + a1 and (a1 ** 2 + a2 ** 2 - a3 ** 2) / (2 * a1 * a2) > 0:
        answer += 1

print(answer)
