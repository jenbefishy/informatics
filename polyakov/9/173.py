file = open("9-170.csv")

def mulArr(arr):
    res = 1
    for i in arr:
        res *= i
    return res

def geomAvg(arr):
    res = 1
    for i in arr:
        res *= i
    return res ** (1/len(arr))

answer = 0
for i in file:
    arr = [int(x) for x in i.split(",")]

    repeat   = [x for x in arr if arr.count(x) != 1]
    noRepeat = [x for x in arr if arr.count(x) == 1]

    if len(noRepeat) == 2 and geomAvg(repeat) >= mulArr(noRepeat):
        answer += 1

print(answer)
