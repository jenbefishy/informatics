from csv import reader
answer = 0
with open("9-97.csv", encoding="cp1251") as F:
    rdr = reader(F, delimiter=";", quotechar="\"")
    for s in rdr:
        s = s[0]
        sides = list(map(int, s.split(",")))
        sides.sort()
        if sides[0] ** 2 +  sides[1] ** 2 == sides[2] ** 2:

            del1 = []
            del2 = []
            del3 = []
            
            k = 2
            while k * k < sides[2] + 1:
                if sides[0] != 1 and sides[0] % k == 0:
                    sides[0] //= k
                    del1.append(k)

                if sides[1] != 1 and sides[1] % k == 0:
                    sides[1] //= k
                    del1.append(k)

                if sides[2] != 1 and sides[2] % k == 0:
                    sides[2] //= k
                    del1.append(k)
                k += 1

            res = set()

            for i in del1:
                res.add(i)

            for i in del2:
                res.add(i)

            for i in del3:
                res.add(i)

            if len(res) == len(del1) + len(del2) + len(del3):
                answer += 1

    print(answer)
