
data = {
        "A" : "HL",
        "B" : "CA",
        "C" : "DA",
        "D" : "E",
        "E" : "F",
        "F" : "LG",
        "G" : "AB",
        "H" : "CI",
        "I" : "DJ",
        "J" : "E",
        "K" : "JA",
        "L" : "K",
        }

def f(s, end):
    if s[-1] == end and len(s) != 1:
        return 1
    res = 0
    for x in data[s[-1]]:
        if x == end:
            res += f(s + x, end)
        if x not in s:
            res += f(s + x, end)
    return res

print(f("A", "A"))
