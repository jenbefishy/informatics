data = {
    "А" : "ЗВ",
    "Б" : "АГ",
    "В" : "ЗКЕБ",
    "Г" : "А",
    "Д" : "АГ",
    "Е" : "Б",
    "Ж" : "И",
    "З" : "ИЖК",
    "И" : "Д",
    "К" : "ЕЖ",
    }

def f(s, end):
    if s[-1] == end and len(s) != 1:
        return 1

    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "А"))
