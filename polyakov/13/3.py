data = {
        "А" : "БВГ",
        "Б" : "Д",
        "В" : "БДЕЗЖГ",
        "Г" : "Ж",
        "Д" : "Е",
        "Е" : "З",
        "Ж" : "З",
        "З" : ""
        }

def f(s, end):
    if s[-1] == end:
        return 1
    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "З"))
