
data = {
    "А" : "БД",
    "Б" : "В",
    "Д" : "Ж",
    "Г" : "ВЕЖ",
    "В" : "ИЕ",
    "Ж" : "Е",
    "И" : "М",
    "Е" : "ИМК",
    "К" : "М"   
    }

def f(s, end):
    if s[-1] == end:
        return 1

    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "М"))
