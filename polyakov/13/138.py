
data = {
        "А" : "Б",
        "Б" : "Д",
        "В" : "А",
        "Г" : "БВЕ",
        "Д" : "Ж",
        "Е" : "ВК",
        "Ж" : "Г",
        "З" : "ЖД",
        "К" : "З",
        }

def f(s, end):
    if s[-1] == end and len(s) != 1:
        return 1
    
    res = 0
    for x in data[s[-1]]:
        if x == end:
            res += f(s + x, end)
        elif x not in s:
            res += f(s + x, end)

    return res

print(f("Е", "Е"))
