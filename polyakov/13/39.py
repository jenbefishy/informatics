
data = {
    "А" : "БГД",
    "Б" : "ВГ",
    "В" : "ИЕ",
    "Г" : "ВЕЖ",
    "Д" : "ГЖ",
    "Е" : "ИМК",
    "Ж" : "ЕК",
    "И" : "М",
    "К" : "М",
    }

def f(s, end):
    if s[-1] == end:
        return "Г" in s

    return sum(f(s + x, end) for x in data[s[-1]])

print(f("А", "М"))
