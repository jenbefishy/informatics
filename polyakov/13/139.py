from functools import lru_cache

data = {
        "А" : "БЕД",
        "Б" : "В",
        "В" : "ЖЗГ",
        "Г" : "АБ",
        "Д" : "И",
        "Е" : "ЗД",
        "Ж" : "ЗЛ",
        "З" : "ЛГ",
        "И" : "ЗЕК",
        "К" : "З",
        "Л" : "К"
        }

def f(s, end):

    if s[-1] == end and len(s) != 1:
        return 1

    res = 0
    for x in data[s[-1]]:
        if x == end:
            res += f(s + x, end)

        elif x not in s:
            res += f(s + x, end)
    return res

print(f("Г", "Г"))
