
data = {
        "А" : "БВ",
        "Б" : "ВГ",
        "В" : "ДГ",
        "Г" : "ВД",
        "Д" : "А",
        }


def f(s, end):
    if s[-1] == end and len(s) != 1:
        return 1
    
    res = 0
    for x in data[s[-1]]:
        if x == end:
            res += f(s + x, end)
        elif x not in s:
            res += f(s + x, end)

    return res

print(f("Г", "Г"))

