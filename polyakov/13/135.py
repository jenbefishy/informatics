
data = {
        "А" : "БВГ",
        "Б" : "Д",
        "В" : "ДЕ",
        "Г" : "ВЕЖ",
        "Д" : "ЗК",
        "Е" : "ДЗИЖ",
        "Ж" : "ИМН",
        "З" : "КМ",
        "И" : "МЗ",
        "К" : "ЛМ",
        "Л" : "П",
        "М" : "ЛПОН",
        "Н" : "О",
        "О" : "П",
        }



def f(s, end):

    if s == end:
        return 0

    if s[-1] == end:
        return 1

    res = 0
    for x in data[s[-1]]:
        if x not in s:
            res += f(s + x, end)
    return res

answer = 0
for i in data.keys():
    answer += f(i, "П")
print(answer)



