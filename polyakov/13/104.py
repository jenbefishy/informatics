
data = {
    "А" : "БГД",
    "Б" : "ГВ",
    "В" : "ЗК",
    "Г" : "ВЖ",
    "Д" : "Г",
    "Е" : "ЖИМК",
    "Ж" : "И",
    "З" : "ЛК",
    "И" : "М",
    "К" : "МЛН",
    "Л" : "Н",
    "М" : "Н",
    }

res = []
def f(s, end):
    if s[-1] == end:
        if len(s) == 8:
            res.append(s)
        return 1
    return sum(f(s + x, end) for x in data[s[-1]])

f("А", "Н")
res = sorted(res)
print(res[-1])
