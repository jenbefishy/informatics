
data = {
    "А" : "БВО",
    "Б" : "ДЕГ",
    "В" : "БГО",
    "Г" : "ДИЕЖМ",
    "Д" : "И",
    "Е" : "ЙЗ",
    "Ж" : "ЙЕНМ",
    "З" : "ЙК",
    "И" : "ЗЕ",
    "Й" : "КЛ",
    "К" : "Л",
    "Л" : "Н",
    "М" : "О",
    "Н" : "О",
    }

answer = 0

def f(s, end):
    if s[-1] == end:
        global answer
        answer = max(answer, len(s))
        return 1
    return sum(f(s + x, end) for x in data[s[-1]])

f("А", "О")
print(answer - 1)
