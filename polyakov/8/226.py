from itertools import product

cmb = list(set(product("КУСАТЬ", repeat=5)))

answer = 0

def is_match(word):
    if word[0] == "Ь":
        return 0

    if word.count("СУК"):
        return 0

    for i in word:
        if word.count(i) > 1:
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№226:", answer)
