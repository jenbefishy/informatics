from itertools import product
cmb = list(product("ABCDE", repeat=4))

answer = 0

for i in cmb:
    start = []
    end = []
    skip = 0 
    for j in range(len(i)):
        if i[j] in "AE":
            if end != []:
                skip = 1
            start.append(i[j])
        if i[j] in "BCD":
            end.append(i[j])

    if skip:
        continue

    if sorted(start) == start and sorted(end) == end:
        answer += 1

print("№199:", answer)
