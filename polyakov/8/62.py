from itertools import product
cmb = list(product("АБВГДЯ", repeat=3))
answer = 0

def is_match(word):
    if word.count("Я") > 1:
        return 0

    if word.count("Я") == 0:
        return 1

    return (word[0] == "Я" or word[-1] == "Я")
        

for i in cmb:
    word = i[0] + i[1] + i[2] 
    if is_match(word):
        answer += 1

print("№62:", answer)
