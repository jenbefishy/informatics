from itertools import permutations
cmb = list(permutations("КРОЙ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Й" and not ("ОЙ" in word):
        answer += 1

print("№90:", answer)

