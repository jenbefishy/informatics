from itertools import product
cmb = list(product("ОБЪЁМ", repeat=4))

answer = 0

def is_match(word):
    if word.count("О") != 1:
        return 0
    if word[0] == "Ъ" or word[-1] == "Ъ":
        return 0
        
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№167:", answer)
