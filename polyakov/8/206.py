from itertools import product
cmb = list(product("ЯСНОВИДЕЦ", repeat=5))

answer = 0

def is_match(word):
    if not (word[0] in "СНВДЦ") or word.count(word[0]) > 1:
        return 0
    if not (word[-1] in "ЯОИЕ") or word.count(word[-1]) > 1:
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№206:", answer)
