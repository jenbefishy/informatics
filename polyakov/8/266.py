from itertools import product

cmb = list("".join(word) for word in product("ДЕЙНПТЬЯ", repeat=4))


answer = 0

def is_match(word):
    found = 0
    for i in word:
        if i in "ЕЯ":
            found = 1
        if word.count(i) != 1:
            return 0
    return found == 0

for i in range(len(cmb)):
    word = cmb[i]
    if is_match(word):
        answer = i + 1

print("№266:", answer)
