from itertools import permutations
cmb = list(permutations("НИГРОЛ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "О" and not("ОИГ" in word):
        answer += 1

print("№102:", answer)
