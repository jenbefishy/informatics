from itertools import product

cmb = list(set(product("ОГЭИНФ", repeat=6)))

answer = 0

def is_match(word):
    if not (word[0] in "ЭО"):
        return 0

    if not word.endswith("ФИ"):
        return 0
    
    if word.count("ОГЭ"):
        return 0

    if word.count("ИГ") < 1:
        return 0

    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№253:", answer)
