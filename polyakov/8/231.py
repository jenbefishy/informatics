

def to_base(number, base):
    result = ""
    while number:
        result = str(number % base) + result
        number //= base
    return result

def is_match(number):
    s = str(number)
    if s[-1] in "347":
        return 0
    
    for i in range(2, len(s)):
        if s[i-2] == s[i-1] == s[i]:
            return 0 
    return 1
        

answer = 0

for i in range(int("1000000", 9), int("10000000", 9)):
    if is_match(to_base(i, 9)):
        answer += 1

print("№231:", answer)
