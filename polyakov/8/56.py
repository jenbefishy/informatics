from itertools import product
cmb = list(product("SIROP", repeat=5))
answer = 0

def is_match(word):
    if word.count("O") != 1:
        return 0

    i = word.index("O")
    if i == 0:
        return 0

    return word[i - 1] in "SRP"
    

for i in cmb:
    word = i[0] + i[1] + i[2] + i[3] + i[4]
    if is_match(word):
        answer += 1

print("№56:", answer)
