answer=0
mas0 = [] # массив, где две чётные цифры стоят рядом
mas1 = [] # массив, где две нечётные цифры стоят рядом
from itertools import product
arr = product('02468', repeat=2)
for x in arr:
    mas0.append(''.join(x))

arr = product('13579', repeat=2)
for x in arr:
    mas1.append(''.join(x))

def f(word, mas): # подходит ли число? (  две чётные/две нечётные цифры не стоят рядом )
    for x in mas:
        if x in word:
            return False
    else:   # если цикл прошёл весь, то вернуть True
        return True


le = '0123456789'
for a in '123456789':
    for b in le:
        for c in le:
            for d in le:
                for e in le:
                    word = a+b+c+d+e
                    if (e in '05') and word.count(a)==1 and word.count(b)==1 and word.count(c)==1 and word.count(d)==1 \
                    and word.count(e)==1 and f(word, mas0) and f(word, mas1):
                        answer+=1
print('№ 156:',answer)
