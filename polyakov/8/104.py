from itertools import permutations
cmb = list(permutations("НАДПИСЬ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Ь" and not("ЬИА" in word):
        answer += 1

print("№104:", answer)
