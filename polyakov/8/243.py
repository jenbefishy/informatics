from itertools import permutations

cmb = list(set(permutations("ВОЛКОДАВ")))

answer = 0


def is_match(word):
    found = 0
    for i in range(1, len(word)):
        if word[i-1] in "ОА" and word[i] in "ОА":
            found = 1
            break

        if word[i-1] in "ВЛКД" and word[i] in "ВЛКД":
            found = 1
            break
    return found

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("243:", answer)

