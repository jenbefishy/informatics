from itertools import permutations

cmb = list(set(permutations("ПРЕПАРАТ")))

answer = 0


def is_match(word):
    found = 0
    for i in range(1, len(word)):
        if word[i-1] in "ЕА" and word[i] in "ЕА":
            found = 1
            break

        if word[i-1] in "ПРТ" and word[i] in "ПРТ":
            found = 1
            break
    return found

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("245:", answer)

