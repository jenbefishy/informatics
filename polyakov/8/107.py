from itertools import permutations
cmb = list(permutations("КОЛУН"))

answer = 0

def is_match(word):
    for i in range(1, len(word)):
        if word[i-1] in "КЛН" and word[i] in "КЛН":
            return 0
        if word[i-1] in "ОУ" and word[i] in "ОУ":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№107:", answer)
