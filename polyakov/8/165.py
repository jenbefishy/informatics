
odd  = "13579bdf"
even = "02468ace"

answer = 0

def is_match(n):
    number = str(n)[2:]
    found = number[0]
    for i in range(1, len(number)):
        if number[i] in odd and number[i - 1] in odd:
            return 0
        if number[i] in even and number[i - 1] in even:
            return 0
        if number[i] in found:
            return 0
        else:
            found += number[i]
    return 1

for i in range(4096, 65536):
    if is_match(hex(i)):
        answer += 1

print("№165:", answer)
