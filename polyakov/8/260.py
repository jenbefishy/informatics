from itertools import product

phrase = "ТИХО"
cmb = list("".join(word) for word in product("ТИХОРЕЦК", repeat=4))

answer = 0

def is_match(word):
    found = 0
    vowels = 0
    for i in range(len(word)):
        if word[i] == phrase[i]:
            found += 1

        if word.count(word[i]) != 1:
            return 0

        if word[i] in "ИОЕ":
            vowels += 1

        
    return (found == 2 and vowels == 2)


for i in cmb:
    if is_match(i):
        answer += 1


print("№260:", answer)
