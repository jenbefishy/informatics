from itertools import permutations
cmb = list(permutations("АБРИКОС"))

answer = 0

def is_match(word):
    for i in range(1, len(word)):
        if word[i-1] in "БРКС" and word[i] in "БРКС":
            return 0
        if word[i-1] in "АИО" and word[i] in "АИО":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№109:", answer)
