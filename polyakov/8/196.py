from itertools import product

def is_match(word):
    if word.count("Ы") > 2:
        return 0
    if word.count("А") > 2:
        return 0

    for i in range(1, len(word)):
        if word[i] in "КРШ":
            return 0
    return 1

def get_result(length):
    answer = 0
    cmb = list(product("КРЫША", repeat=length))
    for i in cmb:
        word = ""
        for j in i:
            word += j
        if is_match(word):
            answer += 1
    return answer

count = 0
for i in range(3, 10):
    found = get_result(i)
    if found == 0:
        break
    count += found

print("№196:", count)
    
