from itertools import permutations
cmb = list(permutations("КОМЕТА"))

answer = 0

def is_match(word):
    for i in range(1, len(word)):
        if word[i-1] in "КМТ" and word[i] in "КМТ":
            return 0
        if word[i-1] in "ОЕА" and word[i] in "ОЕА":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№108:", answer)
