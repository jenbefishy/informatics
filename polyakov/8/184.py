from functools import lru_cache

numbers = "0123456789ABCDEF"

words = []

def is_match(c1, c2, numbers):
    index1, index2 = 0, 0
    for i in range(len(numbers)):
        if numbers[i] == c1:
            index1 = i
        if numbers[i] == c2:
            index2 = i
    if index2 < index1:
        return 0
    if index1 % 2 == index2 % 2:
        return 0
    return 1
            
@lru_cache(None)
def recursion(word, lenght, symbols):
    if len(word) == lenght:
        if word in words:
            return 0
        words.append(word)
        return 1
    
    count = 0 
    for i, c in enumerate(symbols):
        if (word == "" or is_match(c, word[-1], symbols)):
            count += recursion(word+c, lenght, symbols)
    return count

print("№184:", recursion("", 12, numbers))
