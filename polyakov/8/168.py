from itertools import product
cmb = list(product("КЛЕЙ", repeat=6))

answer = 0

def is_match(word):
    if word.count("Й") > 1:
        return 0

    if word[0] == "Й" or word[-1] == "Й":
        return 0

    if word.count("Й") == 0:
        return 1

    i = word.index("Й")
    if word[i-1] and word[i - 1] == "Е":
        return 0
        
    if word[i+1] and word[i + 1] == "Е":
        return 0
    return 1
        
for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№168:", answer)
