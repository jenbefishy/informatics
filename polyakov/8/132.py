from itertools import permutations
cmb = list(set(permutations("АСПЕКТ")))
answer = 0

def is_match(word):
    
    for i in range(1, len(word)):
        if word[i-1] in "ЕА" and word[i] in "ЕА":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№132:", answer)

