from itertools import product
cmb = list(product("АГИЛМОРТ", repeat=4))

answer = 0

for i in range(len(cmb)):
    el = cmb[i]
    if el[-1] == "М" and el[-2] == "И":
        answer = i + 1

print("№84:", answer)
