from itertools import product
cmb = list(product("012345", repeat=5))

answer = 0

def is_match(word):
    if word[0] == "0":
        return 0

    for i in range(1, len(word)):
        if word[i - 1] in "024" and word[i] in "024":
            return 0
        if word[i - 1] in "135" and word[i] in "135":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№205:", answer)
