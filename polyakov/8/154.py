from itertools import product
# up - 1 right - 2 down - 3 left - 4
cmb = list(product("1234", repeat=5))

answer = 0

def is_match(word):
    if word[0] == "1":
        return 0

    if word[0] == word[-1] and word[1] == word[-2]:
        return 0
    
    return 1


for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№154:", answer)
