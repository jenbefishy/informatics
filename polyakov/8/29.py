from itertools import product
cmb = list(product("KANT", repeat=6))
answer = 0 
for i in cmb:
    if i.count("K") == 2:
        answer += 1

print("№29:", answer)
