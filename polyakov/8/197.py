from itertools import product

def is_match(word):
    if word.count("Е") > 2:
        return 0
    if word.count("И") > 2:
        return 0
    if word.count("Я") > 2:
        return 0

    for i in range(1, len(word)):
        if word[i] in "КСН":
            return 0
    return 1

def get_result(length):
    answer = 0
    cmb = list(product("КСЕНИЯ", repeat=length))
    for i in cmb:
        word = ""
        for j in i:
            word += j
        if is_match(word):
            answer += 1
    return answer

count = 0
for i in range(3, 10):
    found = get_result(i)
    if found == 0:
        break
    count += found

print("№197:", count)
    
