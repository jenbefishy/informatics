from itertools import product
cmb = list(product("0123456789", repeat=7))

answer = 0

def is_match(word):
    if word[0] == "0" or not word[-1] in "05":
        return 0
    for i in range(1, len(word)):
        if int(word[i - 1]) % 2 == int(word[i]) % 2:
            return 0
        if word.count(word[i]) > 1:
            return 0
        
    return 1
        
    

for i in cmb:
    word = ""
    for j in i:
        word += j 
    if is_match(word):
        answer += 1

# Wait a little bit
print("№158:", answer)
