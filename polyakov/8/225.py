from itertools import product

cmb = list(set(product("01234", repeat=5)))

answer = 0

def is_match(word):
    found = 0
    if word[0] == "0":
        return 0

    for i in word:
        if i in "024":
            found += 1
    return found <= 3

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№225:", answer)
