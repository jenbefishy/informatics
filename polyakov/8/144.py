from itertools import product
cmb = list(set(product("АЗИМУТ", repeat=6)))
answer = 0

def is_match(word):
    
    count = 0
    for i in word:
        if i in "ЗМТ":
            count += 1
    
    return count >= 3

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№144:", answer)
