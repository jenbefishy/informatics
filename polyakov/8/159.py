
answer = 0

odd  = "13579"
even = "02468"

def is_match(n):
    number = str(n)
    found = number[0] 
    for i in range(1, 8):
        if (number[i - 1] in odd) and (number[i] in odd):
            return 0
        if (number[i - 1] in even) and (number[i] in even):
            return 0

        if number[i] in found:
            return 0
        else:
            found += number[i]
    return 1

for i in range(10000000, 99999999, 5):
    if is_match(i):
        answer += 1
# Wait 25 sec
print("№159:", answer)
