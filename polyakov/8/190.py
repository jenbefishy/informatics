from itertools import product
cmb = list(set(product("КАЛЬКА", repeat=5)))

answer = 0

for i in cmb:
    if i.count("А") <= 1:
        answer += 1

print("№190:", answer)
