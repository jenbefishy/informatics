from itertools import product
cmb = list(product("АВГДИНОР", repeat=4))
count = 0
for i in cmb:
    count += 1
    if i[0] == "Г" and i[1] == "О":
        break

print("№81:", count)
