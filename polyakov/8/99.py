from itertools import permutations
cmb = list(permutations("НИЧЬЯ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Ь" and not ("ЬИЯ" in word):
        answer += 1

print("№99:", answer)

