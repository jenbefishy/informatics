from itertools import product
cmb = list(product("БАЛОН", repeat=6))

answer = 0

for i in cmb:
    if i.count("А") <= 1 and i.count("О") <= 1:
        answer += 1

print("№186:", answer)
