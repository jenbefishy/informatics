from itertools import product

cmb = list(set(product("СОЛНЦЕ", repeat=6)))

answer = 0

def is_match(word): 
    
    if word.count("О") > 2:
        return 0
    
    if word.count("Ц") != 1:
        return 0
        

    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("216:", answer)
