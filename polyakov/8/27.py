from itertools import product
cmb = list(product("KPOT", repeat=6))

answer = 0

for i in cmb:
    if i.count("O") == 1:
        answer += 1

print("№27", answer)
