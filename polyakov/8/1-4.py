from itertools import product
cmb = list(product('АОУ', repeat=5))

# Answers
print("№1:", *cmb[100])
print("№2:", *cmb[124])
print("№3:", *cmb[169])
print("№4:", *cmb[209])


