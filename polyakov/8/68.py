from itertools import permutations

# A,B - books, 1,2,3,4 - "War and peace"
cmb = list(set(permutations("123AB4")))

answer = 0

def is_match(word):
    line = 0
    for i in word:
        if i in "1234":
            line += 1
            if line == 4:
                return 1
        else:
            line = 0
    return 0

for i in cmb:
    stack = ""
    for j in i:
        stack += j

    if is_match(stack):
        answer += 1

print("№68:", answer)
