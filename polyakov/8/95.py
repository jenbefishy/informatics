from itertools import permutations
cmb = list(permutations("КАБИНЕТ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Б" and not ("ЕА" in word):
        answer += 1

print("№95:", answer)

