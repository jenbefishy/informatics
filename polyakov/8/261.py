from itertools import product

cmb = list("".join(word) for word in product("АЕЖЙМУ", repeat=5))

def is_match(word):
    for i in range(1, len(word)):
        if word[i-1] == word[i]:
            return 0
    return 1

answer = 0
for i in range(0, len(cmb), 2):
    if is_match(cmb[i]):
        answer += 1

print("№261:", answer)
