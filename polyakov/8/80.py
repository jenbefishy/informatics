from itertools import product
cmb = list(product("АВГЕН", repeat=4))
count = 0
for i in cmb:
    count += 1
    if i.count("А") == 0:
        break

print("№80:", count)
