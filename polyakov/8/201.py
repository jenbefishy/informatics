from itertools import product
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
cmb = list(product(numbers, repeat=5))

answer = 0

for i in cmb:
    if sorted(i) == list(i):
        answer += 1

print("201:", answer)
