from itertools import product
cmb = list(product("KRAN", repeat=3))
answer = 0
for i in cmb:
    if i.count("A") >= 1:
        answer += 1
print("№41:", answer)
