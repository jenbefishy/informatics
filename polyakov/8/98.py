from itertools import permutations
cmb = list(permutations("ГЕЛИЙ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Й" and not ("ИЕЙ" in word):
        answer += 1

print("№98:", answer)

