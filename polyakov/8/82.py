from itertools import product
cmb = list(product("АВГДИНОР", repeat=4))
count = 0
for i in cmb:
    count += 1
    if i[0] == "И" and i[1] == "Р":
        break

print("№82:", count)
