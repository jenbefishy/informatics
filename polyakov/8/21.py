from itertools import permutations, product

start_words = list(product("ЛЕТО", repeat=4))

answer_combinations = set()
for i in start_words:
    cmb = set(permutations(i))
    for j in cmb:
        if j[0] in "ТЛ":
            answer_combinations.add(j)



print("№21:", len(answer_combinations))
