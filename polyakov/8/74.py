from itertools import product
cmb = list(product("АДР", repeat=6))

count = 0

for i in cmb:
    count += 1
    if i[0] == "Р":
        break

print("№74:", count)
