from itertools import product
cmb = list(set(product("KORABLIKI", repeat=5)))

answer = 0

def is_match(word):
    if not (word[0] in "OAI"):
        return 0

    for i in range(1, len(word)):
        if word[i - 1] in "KRBL" and word[i] in "KRBL":
            return 0

        if word[i - 1] in "OAI" and word[i] in "OAI":
            return 0

        if word.count(word[i]) > 1:
            return 0
            
    return 1


for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№76:", answer)
