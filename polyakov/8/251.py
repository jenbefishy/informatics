from itertools import product

cmb = list(set(product("ОБЩЕСТВ", repeat=5)))

answer = 0

def is_match(word):
    if word[0] in "ЩБ":
        return 0
    if not word.endswith("ВВ"):
        return 0
    if word.count("ЕВ") > 0 or word.count("ВЕ") > 0:
        return 0
    if word.count("ТБ") < 1:
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№251:", answer)
