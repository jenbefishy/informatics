from itertools import product
cmb = list(product("АВИКНСТ", repeat=4))

res_list = []

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] in "ВНКСТ" and word[-1] in "ИА":
        res_list.append(word)

print("№175:", res_list.index("НИКА") + 1)

