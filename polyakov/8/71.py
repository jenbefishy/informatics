from itertools import product
cmb = list(product("ЕИКНУЧ", repeat=3))

count = 0

for i in cmb:
    count += 1
    if i[0] == "К":
        break

print("№71:", count)
