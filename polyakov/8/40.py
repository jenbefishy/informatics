from itertools import product
cmb = list(product("ABCX", repeat=5))
answer = 0
for i in cmb:
    if (i.count("X") == 1 and i[-1] == "X") or i.count("X") == 0:
        answer += 1
print("№40:", answer)
