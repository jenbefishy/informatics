from itertools import product
cmb = list(product("KATER", repeat=5))
answer = 0
for i in cmb:
    if i.count("R") >= 2:
        answer += 1
print("№51:", answer)
