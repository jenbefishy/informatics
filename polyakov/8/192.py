from itertools import product, permutations

start = list(permutations("МАРИ"))
end = list(product("ИНА", repeat=4))

final = set()

for i in start:
    word_start = ""
    for j in i:
        word_start += j

    for j in end:
        word_end = ""
        for k in j:
            word_end += k
        final.add(word_start + word_end)
        
final = sorted(final)
print("№192:", final.index("МАРИАННА") + 1)
