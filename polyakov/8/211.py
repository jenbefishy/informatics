from itertools import permutations
cmb = list(set(permutations("ТИКТОК")))

answer = 0

def is_match(word):
    if word.count("ТТ"):
        return 0
    if word.count("КК"):
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№211:", answer)
