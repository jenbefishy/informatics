
odd  = "13579"
even = "02468"

answer = 0

def is_match(n):
    number = str(n)[2:]
    found = number[0]
    for i in range(1, len(number)):
        if number[i] in odd and number[i - 1] in odd:
            return 0
        if number[i] in even and number[i - 1] in even:
            return 0
        if number[i] in found:
            return 0
        else:
            found += number[i]
    return 1

# 4096 - 0o10000, 32767 - 0o77777
for i in range(4096, 32768):
    if is_match(oct(i)):
        answer += 1

print("№160:", answer)
