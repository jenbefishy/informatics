
odd  = "13579"
even = "02468"

answer = 0

def is_match(n):
    number = str(n)[2:]
    found = number[0]
    for i in range(1, len(number)):
        if number[i] in odd and number[i - 1] in odd:
            return 0
        if number[i] in even and number[i - 1] in even:
            return 0
        if number[i] in found:
            return 0
        else:
            found += number[i]
    return 1

# 2097152 - 0o10000000,  16777215 - 0o77777777
for i in range(262144, 2097152):
    if is_match(oct(i)):
        answer += 1

print("№163:", answer)
