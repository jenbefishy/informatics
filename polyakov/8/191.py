from itertools import product
cmb = list(set(product("САЛЬСА", repeat=6)))

answer = 0

for i in cmb:
    if i.count("А") <= 1:
        answer += 1

print("№191:", answer)
