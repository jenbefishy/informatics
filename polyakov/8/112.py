from itertools import permutations
cmb = list(set(permutations("ВЕНТИЛЬ")))

answer = 0

def is_match(word):
    if word[-1] == "Ь":
        return 0

    for i in range(1, len(word) - 1):
        if word[i] == "Ь" and word[i+1] in "ИЕ" and word[i-1] in "ЕИ":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№112:", answer)
