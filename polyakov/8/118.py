from itertools import permutations
cmb = list(set(permutations("АВРОРА")))

answer = 0

def is_match(word):
    if word.count("АА") > 0 or word.count("РР") > 0:
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№118:", answer)
