numbers = "0123456789ABCDEF"
def toBase(n, base):
    result = ""
    while n:
        result = str(numbers[n%base]) + result
        n //= base
    return result

def is_match(word):
    
    return word.endswith("AB")

answer = 0

for i in range(int("200000", 16), int("1000000", 16), ):
    if is_match(toBase(i, 16)):
        answer += 1


print("№256:", answer)
