
def f(n):
    while "555" in n or "333" in n:
        if "555" in n:
            n = n.replace("555", "3", 1)
        else:
            n = n.replace("333", "5", 1)
    return n


s = f(200 * "5")
answer = 0
for i in s:
    answer += int(i)
print(answer)
