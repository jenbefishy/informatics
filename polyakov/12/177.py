
answer = 0
def f(n):
    global answer
    while "555" in n or "333" in n:
        if "555" in n:
            answer += 3
            n = n.replace("555", "3", 1)
        else:
            n = n.replace("333", "5", 1)
    return n

f(400 * "5")


print(answer)
