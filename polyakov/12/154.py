def func(n):
    while "4444" in n or "777" in n:
        if "4444" in n:
            n = n.replace("4444", "77", 1)
        else:
            n = n.replace("777", "4", 1)

    return n

print(func(204 * "4"))
