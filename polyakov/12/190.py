
def f(n):
    while "888" in n or "77" in n:
        if "888" in n:
            n = n.replace("888", "8777", 1)
        else:
            n = n.replace("77", "8", 1)
    return n


s = f(100 * "8")
print(str(s.count("8")) + "," +  str(s.count("7")))

