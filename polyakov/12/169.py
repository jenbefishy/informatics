
def f(n):
    while "63" in n or "664" in n or "6665" in n:
        if "63" in n:
            n = n.replace("63", "4", 1)
        else:
            if "664" in n:
                n = n.replace("664", "5", 1)
            else:
                if "6665" in n:
                    n = n.replace("6665", "3", 1)
    return n


print(f("4" + 79 * "6" + "4"))
