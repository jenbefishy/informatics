
def f(n):
    while "1111" in n or "000" in n:
        if "1111" in n:
            n = n.replace("1111", "10000", 1)
        else:
            n = n.replace("000", "11", 1)
    return n


s = f(90 * "1")
print(str(s.count("1")) + "," +  str(s.count("0")))

