
def f(n):
    while "111" in n:
        n = n.replace("111", "2", 1)
        n = n.replace("222", "3", 1)
        n = n.replace("333", "1", 1)
    return n


s = f("1" * 120)
print(s)

