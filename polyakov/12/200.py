
def f(n):
    while "222" in n:
        n = n.replace("222", "1", 1)
        n = n.replace("111", "2", 1)
    return n


s = f("1" * 2019 + "2" * 2019)
print(s)

