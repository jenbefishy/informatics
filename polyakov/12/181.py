
def f(n):
    while "10" in n or "1" in n:
        if "10" in n:
            n = n.replace("10", "001", 1)
        else:
            n = n.replace("1", "000")
    return n


print(f("1" + 80 * "0").count("0"))

