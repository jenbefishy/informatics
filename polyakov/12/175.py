
def f(n):
    while "555" in n or "333" in n:
        if "555" in n:
            n = n.replace("555", "3", 1)
        else:
            n = n.replace("333", "5", 1)
    return n


print((f(300 * "5")).count("5"))
