def func(n):
    while "444" in n or "888" in n:
        if "444" in n:
            n = n.replace("444", "8", 1)

        while "555" in n:
            n = n.replace("555", "8", 1)

        while "888" in n:
            n = n.replace("888", "3", 1)

    return n

print(func(9 * "4" + 12 * "5"))
