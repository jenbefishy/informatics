
def f(n):
    res = ""
    for i in n:
        if i == "0":
            res += "3"
        if i == "1":
            res += "7"
        if i == "2":
            res += "2"
        if i == "3":
            res += "1"
        if i == "4":
            res += "6"
        if i == "5":
            res += "0"
        if i == "6":
            res += "4"
        if i == "7":
            res += "5"
    return res


n = "32006"
for _ in range(13):
    n = f(n)
print(n)
