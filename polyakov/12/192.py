
def f(n):
    while "8887" in n or "77" in n:
        if "8887" in n:
            n = n.replace("8887", "8", 1)
        else:
            n = n.replace("77", "8", 1)
    return n


s = f(120 * "7")
print(s)

