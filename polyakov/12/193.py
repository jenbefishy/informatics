
def f(n):
    while "88777" in n or "7" in n:
        if "88777" in n:
            n = n.replace("88777", "8", 1)
        else:
            n = n.replace("7", "8", 1)
    return n


s = f(120 * "7")
print(s)

