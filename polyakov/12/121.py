def func(n):
    while "333" in n or "555" in n:
        if "555" in n:
            n = n.replace("555", "3", 1)
        else:
            n = n.replace("333", "5", 1)
    return n

print(func(146 * "5"))
