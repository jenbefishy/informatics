def func(n):
    while "5555" in n or "3333" in n:
        if "5555" in n:
            n = n.replace("5555", "3", 1)
        else:
            n = n.replace("3333", "5", 1)

    return n

print(func(147 * "5"))
