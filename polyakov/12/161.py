def func(n):
    while "9999" in n or "333" in n:
        if "9999" in n:
            n = n.replace("9999", "3", 1)
        else:
            n = n.replace("333", "99", 1)

    return n

print(func(207 * "3"))
