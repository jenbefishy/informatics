def func(n):
    while "222" in n or "6666" in n:
        if "222" in n:
            n = n.replace("222", "6", 1)
        else:
            n = n.replace("6666", "2", 1)

    return n

print(func(292 * "6"))
