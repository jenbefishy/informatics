
def f(n):
    while "78" in n or "7" in n:
        if "788" in n:
            n = n.replace("78", "8887", 1)
        else:
            n = n.replace("7", "8888", 1)
    return n


s = f("7" + 55 * "8")
print(s.count("8"))

