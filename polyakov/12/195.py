
def f(n):
    while "12" in n or "1" in n:
        if "12" in n:
            n = n.replace("12", "2221", 1)
        else:
            n = n.replace("1", "222222", 1)
    return n


s = f("1" + 51 * "2")
print(s.count("2"))

