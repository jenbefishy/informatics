
def f(n):
    while "56" in n or "1111" in n:
        n = n.replace("56", "1", 1)
        n = n .replace("1111", "1", 1)
    return n


print(f(102 * "561"))

