
answer = 0
def f(n):
    global answer
    while "555" in n or "333" in n:
        if "333" in n:
            n = n.replace("333", "5", 1)
        else:
            n = n.replace("555", "3", 1)
            answer += 3
    return n

f(500 * "5")


print(answer)
