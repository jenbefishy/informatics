def func(n):
    while "18" in n or "288" in n or "3888" in n:
        if "18" in n:
            n = n.replace("18", "2", 1)
        else:
            if "288" in n:
                n = n.replace("288", "3", 1)
            else:
                n = n.replace("3888", "1", 1)

    return n

print(func("1" + 80 * "8"))
