
def f(n):
    while "56" in n or "3333" in n:
        n = n.replace("56", "3", 1)
        n = n .replace("3333", "3", 1)
    return n


print(f(121 * "563"))

