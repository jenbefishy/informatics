
def f(n):
    while "68" in n or "7777" in n:
        n = n.replace("68", "7", 1)
        n = n .replace("7777", "7", 1)
    return n


print(f(143 * "687"))

