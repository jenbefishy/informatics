
def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return res

def isMatch(arr):
    s = 0
    m = 1
    for i in arr:
        s += i
        m *= i
    return (s % 2 != 0) and (m % 2 != 0)

count = 0
for i in range(800_000, 2_000_000):

    if count == 6:
        break

    divs = getDivs(i)
    if len(divs) > 10 and isMatch(divs):
        print(i, len(divs))
        count += 1

