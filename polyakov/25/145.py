
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return res


def isMatch(x):
    res = 0
    for i in str(x):
        if int(i) >= 3:
            return 0
        res += int(i)

    return res % 10 == 0

answer = [0]
for i in range(1000000, 1300000 + 1):
    if isMatch(i):
        answer.append(i)


k = 10
while k < len(answer):
    print(answer[k], len(getDivs(answer[k])))
    k += 10

