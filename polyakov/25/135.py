
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return res

def isMatch(arr):
    for i in arr:
        if i % 2 == 0:
            return 0
    return 1

for i in range(321654, 654321 + 1):
    divs = getDivs(i)
    if len(divs) > 70 and isMatch(divs):
        print(i, max(divs))
