from itertools import product

numbers = "0123456789"
oneChar    =  list("".join(c) for c in product(numbers, repeat=1))
twoChars   =  list("".join(c) for c in product(numbers, repeat=2))
threeChars =  list("".join(c) for c in product(numbers, repeat=3))
variants = [""] + oneChar + twoChars + threeChars

answer = []

for c in variants:

    n = int(f"12345{c}76")

    if n % 73 == 0:
        answer.append(n)

answer.sort()

for i in range(0, 5):
    print(answer[i], answer[i] // 73)
