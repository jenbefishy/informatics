from math import ceil

def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return sorted(res)


for i in range(ceil(525784203 ** 0.25), ceil(728943762 ** 0.25)):
    divs = getDivs(i)

    if len(getDivs(i)) == 2:
        print(i ** 4, i ** 3)

