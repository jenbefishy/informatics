from itertools import product

def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return res

numbers = "0123456789"
oneChar      = list("".join(c) for c in product(numbers, repeat=1))
twoChars     = list("".join(c) for c in product(numbers, repeat=2))
threeChars   = list("".join(c) for c in product(numbers, repeat=3))

variants = [""] + oneChar + twoChars + threeChars 

for c1 in variants:
    n = int(f"12{c1}348")
    divs = getDivs(n)

    if n > 10 ** 7:
        break

    if n % 12 == 0 and len(divs) + 2 == 12:
        print(n, max(divs))
