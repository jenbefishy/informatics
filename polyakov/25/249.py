from itertools import product

numbers = "0123456789"
oneChar    = list("".join(c) for c in product(numbers, repeat=1))
twoChars   = list("".join(c) for c in product(numbers, repeat=2))
threeChars = list("".join(c) for c in product(numbers, repeat=3))
variants = [""] + oneChar + twoChars + threeChars

for c in variants:
    for c1 in oneChar:
        n = int(f"12{c}4{c1}65")
        if n % 161 == 0 and n < 10 ** 8:
            print(n, n // 161)
