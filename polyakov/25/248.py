from itertools import product

def getDivs(x):
    res = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return res

numbers = "0123456789"
oneChar    = list("".join(c) for c in product(numbers, repeat=1))
twoChars   = list("".join(c) for c in product(numbers, repeat=2))
threeChars = list("".join(c) for c in product(numbers, repeat=3))
variants = [""] + oneChar + twoChars + threeChars

for c in variants:
    number = int(f"1234{c}7")

    if number % 141 == 0:
        print(number, number // 141)
