from math import ceil, sqrt

def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return res

for i in range(ceil(sqrt(sqrt(358633892))), ceil(sqrt(sqrt(535672891)))):
    divs = getDivs(i)
    if len(divs) == 0:
        print(i ** 4, i ** 3)

