from math import ceil

def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return sorted(res)


for i in range(ceil((100_000_000 / 2) ** 0.5), ceil((101_000_000 / 2) ** 0.5)):
    divs = getDivs(i)

    if len(divs) == 0:
        x = (i ** 2) * 2
        print(x, i)
    

