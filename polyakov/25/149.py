def getDivs(x):
    res = set()
    for i in range(2, int(x * 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return res

for i in range(55_000_000, 60_000_000 + 1):

    n = i
    while n % 2 == 0:
        n //= 2

    p = n ** 0.25
    if int(p) != p:
        continue

    if len(getDivs(p)) == 0:
        print(i, int(p) ** 4)
