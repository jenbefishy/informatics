from fnmatch import fnmatch

for i in range(100000001, 123459698 + 1000, 17):
    if fnmatch(str(i), "12345?6?8"):
        print(i, i // 17)
