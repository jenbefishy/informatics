def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)

    return sorted(res)

def isMatch(arr):
    for i in range(1, len(arr)):
        if arr[i - 1] + 10 == arr[i]:
            continue
        else:
            return 0
    return 1

for i in range(854321, 1087654 + 1):
    divs = getDivs(i)
    if len(divs) >= 2 and isMatch(divs):
        print(i, min(divs))

