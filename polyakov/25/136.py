
def getDivs(x):
    res = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            res.add(i)
            res.add(x // i)
    return res

for i in range(25317, 51237 + 1):
    divs = getDivs(i)

    answer = 0
    count = 0

    for j in divs:
        arr = getDivs(j)
        if len(arr) == 0:
            count += 1
            answer = max(j, answer)

    if count >= 6:
        print(i, answer)
