from functools import lru_cache

@lru_cache(None)
def f(n):
    if n < 2:
        return n
    if n % 2 == 0:
        return f(n // 2) + 1
    return f(3 * n + 1) + 1


answer = 0
for i in range(1, 101):
    try:
        if f(i) > 100:
            answer += 1
    except:
        pass
print(answer)
