from functools import lru_cache

@lru_cache(None)
def f(n):
    if n < 3:
        return n + 1
    if n % 2 == 0:
        return n + 2 * f(n + 2)
    else:
        return f(n - 2) + n - 2


count = 0
for i in range(1, 1000):
    if i % 2 == 0:
        continue

    if len(str(f(i))) == 3:
        count += 1


print(count)

