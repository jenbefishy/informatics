from functools import lru_cache

@lru_cache(None)
def f(n):
	if n == 1:
		return 1	
	return f(n - 1) - 2 * g(n - 1)

@lru_cache(None)
def g(n):
	if n == 1:
		return 1
	return f(n - 1) + g(n - 1) + n

s = 0
for i in str(g(36)):
	s += int(i)
	
print(s)
