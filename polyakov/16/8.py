from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 18:
		return n
	return 3 * f(n + 1) + n + 8

print(f(9))
