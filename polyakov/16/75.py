from functools import lru_cache

# Works only for n = 2**k
@lru_cache(None)
def f(n):
    if n <= 1:
        return n

    if n > 1 and n % 2 == 0:
        return 1 + f(n // 2)

    if n > 1 and n % 2 == 1:
        return 1 + f(n + 2)


print(f(2**20))
