def F(n):
  global second
  output.append(n)
  if n > 0: 
    d = (n%10 + F(n//10))
    output.append(n)
    return d
  else: 
    return 0

for i in range(1, 10000):
  output = []
  F(i)
  if output[1] > 51:
    print(i, F(i))
    break
    
