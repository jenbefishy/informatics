from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 25:
		return 2 * n * n * n + n * n
	if n <= 25:
		return f(n + 2) + 2 * f(n + 3)

s = 0
for i in str(f(2)):
	s += int(i)

print(s)
