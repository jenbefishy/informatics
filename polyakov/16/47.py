from functools import lru_cache

@lru_cache(None)
def f(n):
	if n <= 3:
		return n
	if n % 2 == 0:
		return f(n - 1) + 2 * f(n / 2)
	return f(n - 1) + f(n - 3)

count = 0
for i in range(1, 100000):
	if f(i) < 100000000:
		count += 1

print(count)
