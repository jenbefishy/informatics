from functools import lru_cache
from sys import setrecursionlimit

setrecursionlimit(10000)

@lru_cache(None)
def f(n):
    if n >= 10_000:
        return n
    if n % 2 == 0:
        return 1 + f(n // 2)
    return n * n + f(n + 2)



for i in range(10_001, 0, -1):
    f(i)

print(f(192) - f(9))

