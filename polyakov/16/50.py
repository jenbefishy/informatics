from functools import lru_cache

@lru_cache(None)
def f(n):
	if n <= 3:
		return n
	if n % 2 == 0:
		return 3 + n + f(n - 1)
	return n * n + f(n - 2)

count = 0
for i in range(1, 1000):
	if f(i) % 7 == 0:
		count += 1

print(count)
