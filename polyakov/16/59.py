from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 30:
		return n * n + 5 * n + 4
	if n % 2 == 0:
		return f(n + 1) + 3 * f(n + 4)
	return 2 * f(n + 2) + f(n + 5)

count = 0
for i in range(1, 1001):
	s = 0
	for i in str(f(i)):
		s += int(i)
	if s == 27:
		count += 1
print(count)
