from functools import lru_cache
from sys import setrecursionlimit

setrecursionlimit(10000)

@lru_cache(None)
def f(n):
    if n >= 10_000:
        return n
    if n % 6 == 0:
        return n // 6 + f(n // 6 + 2)
    return n + f(n + 2)



for i in range(10_001, 0, -1):
    f(i)

print(f(264) - f(7))

