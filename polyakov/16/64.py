
def f(n):
    if n <= 18:
        return n + 3
    if n % 3 == 0:
        return (n // 3) * f(n // 3) + n - 12
    else:
        return f(n - 1) + n * n + 5

count = 0

for i in range(1, 801):
    found = 0
    for j in str(f(i)):
        if j in "13579":
            found = 1
    if not found:
        count += 1


print(count)

