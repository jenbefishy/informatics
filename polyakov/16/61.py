
def f(n):
    if n <= 15:
        return n * n + 3 * n + 9
    if n % 3 == 0:
        return f(n - 1) + n - 2
    else:
        return f(n - 2) + n + 2

count = 0

for i in range(1, 1001):
    found = 0
    for j in str(f(i)):
        if j in "13579":
            found = 1
    if not found:
        count += 1


print(count)

