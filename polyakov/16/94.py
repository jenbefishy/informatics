
def f(n):
    if n < 2:
        return 1
    if n % 3 == 0:
        return f(n // 3) - 1
    return f(n - 1) + 17

for i in range(1, 1000000):
    try:
        if f(i) == 110:
            print(i)
            break
    except:
        pass
