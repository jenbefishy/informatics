def F(n):
  if n<10: 
    return n
  else: 
    m=F(n//10)
    d=m%10;
    if m<d: return d
    else: return m

res = ""
for i in range(1, 1000):
  if F(i) > 7:
    res = str(i) + " " + str(F(i))

print(res)

