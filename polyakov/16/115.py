from functools import lru_cache

def is_prime(n):
    k = 2
    while k < int(n ** 0.5) + 1:
        if n % k == 0:
            return 0
        k += 1
    return 1


@lru_cache(None)
def f(n):
    if n <= 1:
        return 1
    if n > 1 and n % 3 == 0:
        return 2 * f(n - 1) + f(n - 2)
    return 3 * f(n - 2) + f(n - 1)

answer = 0
for i in range(1, 36):
    s = 0

    for j in str(f(i)):
        s += int(j)

    if is_prime(s) and s != 1:
        answer += 1

print(answer)
