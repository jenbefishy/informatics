from functools import lru_cache

@lru_cache(None)
def f(n):
    if n < 4:
        return n - 1
    if n % 3 == 0:
        return n + 2 * f(n - 1)
    else:
        return f(n - 2) + f(n - 3)

r = f(25)
count = 0
for i in str(r):
    count += int(i)

print(count)

