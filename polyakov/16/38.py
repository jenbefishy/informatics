from functools import lru_cache

@lru_cache
def F(n,m):
 if m == 0: 
  d = 0
 else: 
  d = n+F(n, m-1)
 return d

out = set()
for n in range(1, 1000):
  for m in range(1, 1000):
    if F(n, m) == 30:
      out.add(n)
print(len(out))
