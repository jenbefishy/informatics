from functools import lru_cache

@lru_cache(None)
def g(n):
	if n == 1:
		return 1
	return f(n - 1) + 3 * g(n - 1) - 3 * n


@lru_cache(None)
def f(n):
	if n == 1:
		return 1
	return 3 * f(n - 1) + g(n - 1) - n + 5

print(f(14) + g(14))

