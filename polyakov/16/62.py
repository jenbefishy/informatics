
def f(n):
    if n <= 15:
        return 2 * n * n + 4 * n + 3
    if n % 3 == 0:
        return f(n - 1) + n * n + 3
    else:
        return f(n - 2) + n - 6

count = 0

for i in range(1, 1001):
    found = 0
    for j in str(f(i)):
        if j in "02468":
            found = 1
    if not found:
        count += 1


print(count)

