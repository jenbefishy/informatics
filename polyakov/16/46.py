from functools import lru_cache

@lru_cache(None)
def f(n):
	if n <= 3:
		return n
	if n % 2 == 0:
		return 2 * n * n + f(n - 1)
	return n ** 3 + n + f(n - 1)

count = 0
for i in range(1, 100000):
	if f(i) < 10000000:
		count += 1

print(count)
