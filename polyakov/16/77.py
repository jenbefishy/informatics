from functools import lru_cache

@lru_cache(None)
def f(n):
    if n <= 1:
        return n
    if n > 1 and n % 3 == 0:
        return n + f(n // 3)
    else:
        return n + f(n + 3)

# input:
# output:
i = 1
res = 0
while res < 100:
    i *= 3
    res = f(i)

print(i)

