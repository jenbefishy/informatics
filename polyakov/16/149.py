from functools import lru_cache
from sys import setrecursionlimit

setrecursionlimit(100000)

@lru_cache(None)
def f(n, m):
    result = 0
    while m <= n:
        if n % m == 0:
            result += 1
        m += 1
    return result
        

print(f(107864, 3))

