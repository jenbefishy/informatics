from functools import lru_cache

def is_prime(n):
    k = 2
    while k < int(n ** 0.5) + 1:
        if n % k == 0:
            return 0
        k += 1
    return 1


@lru_cache(None)
def f(n):
    if n == 0:
        return 1
    return 7 * (n - 1) + f(n - 1)

answer = 0
for i in range(2, 201):
    if is_prime(f(i)):
        answer += 1

print(answer)
