from functools import lru_cache

@lru_cache(None)
def f(n):
    if n == 0:
        return 1
    if n > 0:
        return 2 * f(1 - n) + 3 * f(n - 1) + 2
    else:
        return -f(-n)

count = 0
r = f(50)
for i in str(r):
    count += int(i)

print(count)

