def F( n ):
  global count
  count += n * n
  if n > 1:
    count += 2 * n + 1
    F(n-2)
    F(n//3)

for i in range(1, 100000):
  count = 0
  F(i)
  if count > 3200000:
    print(i, count)
    break
