from functools import lru_cache

@lru_cache(None)
def g(n):
    if n > 40:
        return 10
    return 30 + f(n + 600 // n)

@lru_cache(None)
def f(n):
    if n < 50:
        return n
    if n > 49:
        return 2 * g(50 - n // 2)


print(f(80))

