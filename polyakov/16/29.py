def F( n ):
  global count
  count += n - 5
  if n > 1:
    count += n + 8
    F(n-2)
    F(n-3)

for i in range(1, 10000):
  count = 0
  F(i)
  if count > 3200000:
    print(i, count)
    break
