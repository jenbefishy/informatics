from functools import lru_cache

@lru_cache(None)
def f(n):
    if n < 2:
        return 1
    if n % 3 == 0:
        return f(n // 3) + 1
    return f(n - 2) + 5


for i in range(1, 100000):
    try:
        if f(i) == 73:
            print(i)
            break
    except:
        pass
