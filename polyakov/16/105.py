from functools import lru_cache

@lru_cache(None)
def f(n):
    if n == 0:
        return 2
    if 0 < n <= 15:
        return f(n - 1)
    if 15 < n < 95:
        return 1.6 * f(n - 3)
    if n >= 95:
        return 3.3 * f(n - 2)

count = 0
value = 0
s = str(int(f(33)))
for i in s:
    if s.count(i) > count:
        count = s.count(i)
        value = i

print(value)
