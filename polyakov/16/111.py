from functools import lru_cache

@lru_cache(None)
def f(n):
    if n == 0:
        return 0
    if n % 2 == 0:
        return f(n // 2) - 2
    return 2 + f(n - 1)

answer = 0
for i in range(0, 1000):
    if f(i) == -2:
        answer += 1

print(answer)
