from functools import lru_cache

@lru_cache(None)
def f(n):
    if n <= 1:
        return 1

    if n > 1 and n % 2 == 0:
        return 3 + f(n // 2 - 1)

    if n > 1 and n % 2 == 1:
        return n + f(n + 2)

# input:  1 2 4 6 10 14 22 30 46 62 94 126 190
# output: 1 4 4 7  7 10 10 13 13 16 16 19   19

# 2  -  1 = 1
# 4  -  2 = 2
# 10 -  6 = 4
# 22 - 14 = 8
# 46 - 30 = 16
# ...

print(f(126))
