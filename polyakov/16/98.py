
def f(n):
    if n == 0:
        return 0
    if n % 2 == 0:
        return f(n // 2) + 3
    return 2 * f(n - 1) + 1

res = set()
for i in range(1, 1001):
    res.add(f(i))
        
print(len(res))
