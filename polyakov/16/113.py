from functools import lru_cache

@lru_cache(None)
def f(n):
    if n == 0:
        return 0
    if n % 2 == 0:
        return f(n // 2) - 1
    return 3 + f(n - 1)

values = set()
for i in range(0, 1000):
    values.add(f(i))
print(len(values))
