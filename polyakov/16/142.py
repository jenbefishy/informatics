from functools import lru_cache
from math import sqrt

@lru_cache(None)
def f(n):
    s = sqrt(n)
    if int(s) == s:
        return round(s)
    return f(n + 1) + 1



for i in range(1, 10000):
    f(i)


print(f(4850) + f(5000))
