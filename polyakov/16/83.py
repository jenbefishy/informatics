from functools import lru_cache

@lru_cache(None)
def f(n):
    if n <= 5:
        return n
    if n % 8 == 0:
        return n + f(n // 2 - 3)
    return n + f(n + 4)


answer = 0
for i in range(1, 1000):
    try:
        f(i)
        answer = i
    except:
        pass
print(answer)
