from functools import lru_cache
from math import log2

@lru_cache(None)
def f(n):
    if n == 0:
        return 1
    if n % 2 != 0:
        return 1 + f(n - 1)
    return f(n // 2)

right = int(log2(500000001)) + 1

count = 0
for i in range(0, right + 1):
    for j in range(i + 1, right + 1):
        if 2 ** i + 2 ** j < 500000001:
            count += 1

print(count)
