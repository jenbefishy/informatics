from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 20:
		return n * n * n + n
	if n % 2 == 0:
		return 3 * f(n + 1) + f(n + 3)
	return f(n + 2) + 2 * f(n + 3)

count = 0
for i in range(1, 1001):
	if str(f(i)).count("1") == 0:
		count += 1
print(count)
