def F(n,m):
 if m == 0:
   return n
 else:
   return F(m,n%m)

out = set()

for n in range(100, 1001):
  for m in range(100, 1001):
    if F(n, m) == 30:
      out.add(n)

print(len(out))
      
