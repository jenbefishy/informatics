from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 15:
		return n * n - 5
	if n <= 15:
		return n * f(n + 2) + n + f(n + 3)

s = 0
for i in str(f(1)):
	s += int(i)

print(s)
