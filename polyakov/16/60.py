from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 25:
		return n * n + 4 * n + 3
	if n % 3 == 0:
		return f(n + 1) + 2 * f(n + 4)
	return f(n + 2) + 3 * f(n + 5)

count = 0
for i in range(1, 1001):
	s = 0
	for i in str(f(i)):
		s += int(i)
	if s == 24:
		count += 1
print(count)
