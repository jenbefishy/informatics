from functools import lru_cache

@lru_cache(None)
def f(n):
    if n == 0:
        return 1
    if n % 2 == 0:
        return f(n - 1) + f(n - 2)
    return 1.5 * f(n - 1)

values = set()
for i in str(int(f(15))):
    values.add(i)

print(len(values))
