def F(n):
  global count
  count += n
  if n>0: 
    d=n%10+F(n//10)
    count += d
    return d
  else: return 0

for i in range(1, 100000):
  count = 0
  if F(i) > 32:
    print(i, F(i))
    break
