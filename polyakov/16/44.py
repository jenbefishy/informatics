from functools import lru_cache

@lru_cache(None)
def f(n):
	if n <= 3:
		return n
	if 3 < n and n % 3 == 0:
		return n ** 3 + f(n - 1)
	if n > 3 and n % 3 == 1:
		return 4 + f(n // 3)

	return n * n + f(n - 2)

	
print(f(100))
