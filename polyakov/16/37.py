def F(n,m):
 if m == 0: 
   d = 1
 else: 
   d = n*F(n, m-1)
 return d

count = 0
for i in range(1, 1001):
  if 100 <= F(i, 2) <= 1000:
    count += 1
print(count)
