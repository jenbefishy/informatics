
def f(n):
    if n == 0:
        return 0
    if n % 2 == 0:
        return f(n // 2) 
    return f(n - 1) + 3

res = 0
for i in range(1, 1001):
    if f(i) == 18:
        res += 1
        

print(res)
