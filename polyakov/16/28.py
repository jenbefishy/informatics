def F( n ):
  global count
  count += 2 * n + 1
  if n > 1:
    count += 3 * n - 8
    F(n-1)
    F(n-4)

for i in range(1, 10000):
  count = 0
  F(i)
  if count > 5000000:
    print(i, count)
    break
