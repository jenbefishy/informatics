from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 25:
		return n * n + 2 * n + 1
	if n % 2 == 0:
		return 2 * f(n + 1) + f(n + 3)
	return f(n + 2) + 3 * f(n + 5)

count = 0
for i in range(1, 1001):
	if str(f(i)).count("0") == 0:
		count += 1
print(count)
