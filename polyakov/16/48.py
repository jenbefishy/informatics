from functools import lru_cache

@lru_cache(None)
def f(n):
	if n <= 3:
		return n
	if n % 2 == 0:
		return n + f(n - 1)
	return n * n + f(n - 2)

count = 0
for i in range(1, 100000):
	if f(i) < 100000000:
		count += 1

print(count)
