from functools import lru_cache

@lru_cache(None)
def f(n):
    if n <= 3:
        return n + 3
    if f(n - 1) % 2 == 0:
        return f(n - 2) + n
    return f(n - 2) + 2 * n

res = 0
for i in range(40, 51):
    res += f(i)

print(res)
