from functools import lru_cache

@lru_cache(None)
def f(n):
    if n % 2 == 1 or n < 4:
        return 1
    return f(n - 1) + f(n - 2) + f(n - 3)

for i in range(1, 2000):
    f(i)

print(f(2008) - f(2006))
