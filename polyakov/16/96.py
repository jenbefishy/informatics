
def f(n):
    if n < 10:
        return n
    return n % 10 + f(n // 10)

def g(n):
    if n < 10:
        return n
    return g(f(n))


res = 0
for i in range(10, 100):
    res += g(i)

print(res)
