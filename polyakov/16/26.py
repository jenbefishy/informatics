count = 0
def F( n ):
  global count
  count += n + 1 
  if n > 1:
    count += n + 5
    F(n-1)
    F(n-2)

for i in range(1, 10000):
  count = 0
  F(i)
  if count > 1000000:
    print(i, count)
    break

