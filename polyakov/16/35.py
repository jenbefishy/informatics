def F(n,m):
 if n<m:
  n,m = m,n
 if n != m: 
   return F(n-m,m)
 else: 
   return n

res = [10000000, 100000, 0]


for n in range(1, 1000):
  for m in range(1, 1000):
    if n != m:
      out = F(n, m)
      if out > 15 and res[0] + res[1] > n + m:
        res[0] = min(n, m)
        res[1] = max(n, m)
        res[2] = out

print(*res)
      
