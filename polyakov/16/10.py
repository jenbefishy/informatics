from functools import lru_cache

@lru_cache(None)
def f(n):
	if n > 12:
		return 2 * n - 5
	return 2 * f(n + 2) + n - 4

print(f(1))
