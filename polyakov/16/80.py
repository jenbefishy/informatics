from functools import lru_cache

@lru_cache(None)
def f(n):
    if n <= 5:
        return n
    if n % 3 == 0:
        return n + f(n // 3 + 2)
    return n + f(n + 3)


for i in range(1, 10000):
    try:
        r = f(i)
        if r > 1000:
            print(i)
            break
    except:
        pass
