from functools import lru_cache
from sys import setrecursionlimit

setrecursionlimit(10000)

@lru_cache(None)
def f(n):
    if n >= 10_000:
        return n
    if n % 2 == 0:
        return f(n + 1) + n * n - 3 * (n - 1)
    return f(n + 2) + 5 * n - (n - 1)



for i in range(10_001, 0, -1):
    f(i)

print(f(9950) - f(9999))

