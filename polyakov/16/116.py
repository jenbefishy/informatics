from functools import lru_cache

def is_prime(n):
    k = 2
    while k < int(n ** 0.5) + 1:
        if n % k == 0:
            return 0
        k += 1
    return 1


@lru_cache(None)
def f(n):
    if n <= 1:
        return 1
    if n % 2 == 0:
        return 11 * n + f(n - 1)
    return 11 * f(n - 2) + n

answer = 0
for i in range(35, 51):
    if f(i) % 2  == 0:
        answer += f(i)

print(len(str(answer)))
