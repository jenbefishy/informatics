from itertools import combinations

def invert(n):
    res = ""
    for i in n:
        if i == "1":
            res += "0"
        else:
            res += "1"
    return res

def func(n):
    r = bin(n)[2:]
    r = r[0] + invert(r[1:])
    return int(r, 2) + n

answer = 0
for i in range(2, 100000):
    if func(i) > 99 and i % 2 != 0:
        answer = i
        break

print("№267:", answer)
