def func(n):
    sn = bin(n)[2::]
    sn = sn[::-1]

    r = ""
    start = 0
    for i in range(0, len(sn)):
        if sn[i] == "0":
            start += 1
        else:
            break
    r = sn[start:]
    
    r = int(r, 2)
    return r

answer = 0
for i in range(1000, 100000):
    if func(i) ==  29:
        answer = i
        break

print("№170:", answer)
