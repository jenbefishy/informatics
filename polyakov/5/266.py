from itertools import combinations

def func(n):
    if n % 3 == 0:
        n //= 3 
    else:
        n -= 1

    if n % 5 == 0:
        n //= 5
    else:
        n -= 1

    if n % 11 == 0:
        n //= 11
    else:
        n -= 1

    return n

        


values = set()
for i in range(2, 100000):
    if func(i) == 8:
        values.add(i)

print("№266:", len(values))
