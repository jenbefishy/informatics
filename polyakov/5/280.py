
def func(n):
    s1 = 0
    s2 = 0
    sn = str(n)
    for i in range(len(sn)):
        if int(sn[i]) % 2 != 0:
            s1 += int(sn[i])
    
        if i % 2 == 0:
            s2 += int(sn[i])

    return abs(s1 - s2)


answer = 0
for i in range(10000, 1000000000):
    if func(i) == 29:
        answer = i
        break

print("№280:", answer)
