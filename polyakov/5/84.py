
def func(n):
    n1 = bin(n)[2:]
    
    if n1.count("1") % 2 == 0:
        n1 += "1"
    else:
        n1 += "0"
    n1 += str(n1.count("1") % 2)

    return n1

answer = 0
for i in range(1, 1000):
    if int(func(i), 2) > 31:
        answer = func(i)
        break

print("№84:", int(answer, 2))
