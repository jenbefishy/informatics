def func(n):
    r = bin(n)[2:]
    r += r[-1]
    if bin(n)[2:].count("1") % 2 == 0:
        r += "0"
    else:
        r += "1"

    if r.count("1") % 2 == 0:
        r += "0"
    else:
        r += "1"

    return r

answer = 0
for i in range(0, 10000):
    if int(func(i), 2) > 136:
        answer = i
        break

print("№149:", answer)
