
def func(n):
    s1 = n % 4
    s2 = n % 3
    s3 = n % 2

    r = str(s1) + str(s2) + str(s3)
    return r

answer = 0
for i in range(10, 100):
    if func(i) == "101":
        answer = i
    
print("№105:", answer)
