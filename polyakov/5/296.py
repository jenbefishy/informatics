def sumNumbers(n):
    res = 0
    for i in str(n):
        res += int(i)
    return res
        
    

def func(n):
    numbers = []
    sn = str(n)[::-1]

    for i in range(len(sn)):
        el = int(sn[i])
        
        if i % 2 != 0:
            el *= 2

            if el >= 10:
                el = sumNumbers(el)
                
        numbers.append(el)

    return sum(numbers) % 10


    
answer = 0
for i in range(1234567891011121, 10000000000000000):
    if func(i) == 0:
        answer = str(i)
        break

print("№296:", answer[8:])

