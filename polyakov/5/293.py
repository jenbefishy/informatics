from itertools import permutations
def func(n, m):
    sn = str(n) + str(m)

    p1 = 0
    p2 = 0

    for i in sn:
        if int(i) % 2 == 0 and int(i) != 0:
            if p1 == 0:
                p1 += int(i)
            else:
                p1 *= int(i)

        elif int(i) != 0:
            if p2 == 0:
                p2 += int(i)
            else:
                p2 *= int(i)
            
    return abs(p1 - p2)

answer = 0
for i in range(1, 10000):
    if func(120, i) == 29:
        answer = i
        break


print("№293:", answer)
