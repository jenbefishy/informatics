
def func(n):
    n1 = bin(n)[2:]
    
    if n1.count("1") % 2 == 0:
        n1 += "1"
        n1 += "1"
    else:
        n1 += "0"
        n1 += "0"

    return n1

answer = 0
for i in range(1, 1000):
    if int(func(i), 2) > 54:
        answer = func(i)
        break

print("№85:", int(answer, 2))
