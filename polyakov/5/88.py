
def func(n):
    sn = bin(n)[2::]

    if sn.count("1") % 2 == 0:
        sn += "00"
    else:
        sn += "10"

    return sn

answer = 0
for i in range(1, 1000):
    if int(func(i), 2) > 103:
        answer = i
        break
    
print("№88:", answer)
