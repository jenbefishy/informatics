def func(n):
    r = bin(n)[2:]
    r += r[-1]
    if bin(n)[2:].count("1") % 2 != 0:
        r += "1"
    else:
        r += "0"

    if r.count("1") % 2 == 1:
        r += "1"
    else:
        r += "0"

    return r

answer = 0
for i in range(0, 10000):
    if int(func(i), 2) > 80:
        answer = int(func(i), 2)
        break

print("№145:", answer)
