from itertools import combinations

def func(n):
    r = bin(n)[2:]
    r += str(r.count("1") % 2)

    if bin(n)[2:].count("1") > bin(n)[2:].count("0"):
        r += "0"
    else:
        r += "1"

    return int(r, 2)


values = set()
for i in range(1, 10000):
    if 50 <= func(i) <= 80:
        values.add(func(i))

print("№258:", len(values))
