from itertools import combinations

def func(n):
    r = bin(n)[2:]
    if r.count("1") > r.count("0"):
        r += "0"
    else:
        r = "11" + r

    if r.count("1") > r.count("0"):
        r += "0"
    else:
        r = "11" + r
    return int(r, 2)

answer = 0
for i in range(10, 100000):
    if func(i) > 500:
        answer = i
        break

print("№235:", answer)
