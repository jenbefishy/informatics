from itertools import combinations

def func(n):
    sn = str(n)
    sn += sn[-1]
    r = bin(int(sn))[2:]
    if r.count("1") % 2 == 0:
        r += "0"
    else:
        r += "1"
    
    return int(r, 2)

answer = 0
for i in range(1, 100000):
    if func(i) > 413:
        answer = i
        break

print("№238:", answer)
