def func(n):
    sn = bin(n)[2:]
    sn = (8 - len(sn)) * "0" + sn
    sn = sn[0:len(sn) - 1]
    sn = sn[::-1]
    return int(sn, 2)

answer = 0
for i in range(0, 100):
    if func(i) == i:
        answer = i
    

print("№173:", answer)
