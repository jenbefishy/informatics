def func(n, m):
    n = bin(n)[2:]
    m = bin(m)[2:]

    sn = n.count("1") ** 2
    sm = m.count("1") ** 2
    
    return sn - sm

    

answer = 1000000
for i in range(1, 500):
    for j in range(1, 500):
        if func(i, j) == 33:
            answer = min(answer, i + j)

print("№295:", answer)
