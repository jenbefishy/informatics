from itertools import combinations

def func(n):
    r = bin(n)[2:]
    r += r[-2]  + r[1]
    return int(r, 2)

answer = 0
for i in range(10, 1000):
    if 100 <= func(i) <= 150:
        answer += 1
    elif func(i) > 150:
        break

print("№229:", answer)
