def func(n):
    sn = bin(n)[2:]
    sn = (8 - len(sn)) * "0" + sn
    r = ""
    for i in range(0, len(sn)):
        if sn[i] == "0":
            r += "1"
        else:
            r += "0"


    return int(r, 2) + 1

answer = 0
for i in range(1, 1000):
    if func(i) == 211:
        answer = i
        break

print("№186:", answer)
