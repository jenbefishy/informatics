def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r += bin(r.count("1"))[2:]
    else:
        r = "1" + r + "00"
    return int(r, 2)


answer = 0

for i in range(1, 10000):
    if 500 <= func(i) <= 700:
        answer += 1

print("№284:", answer)
