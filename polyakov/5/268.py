from itertools import combinations

def func(n):
    r = bin(n)[2:]
    for _ in range(2):
        if r.count("1") > r.count("0"):
            r += "1"
        else:
            r += "0"
    return int(r, 2)


answer = 0
for i in range(2, 100000):
    if func(i) < 57:
        answer = func(i)

print("№268:", answer)
