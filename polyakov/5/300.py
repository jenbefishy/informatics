
def delZero(n):
    res = ""
    found_1 = 0
    for i in n:
        if i != "0":
            found_1 = 1

        if found_1:
            res += i

    return res

def func(n):
    r = bin(n)[2:]
    r = r[1:]
    r = delZero(r)

    if r.count("1") % 2 == 0:
        r = "10" + r
    else:
        r = "1" + r + "0"

    return int(r, 2)


answer = 0
for i in range(1, 10000):
    if func(i) < 450:
        answer = max(func(i), answer)

print("№300:", answer)

