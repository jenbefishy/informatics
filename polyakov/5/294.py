def func(n):

    numbers = []
    for i in str(n):
        numbers.append(int(i))

    if n % 2 == 0:
        numbers.sort(reverse=True)
        n = ""
        for i in numbers:
            n += str(i)

        n = int(n) // 2

    else:
        numbers.sort()
        n = ""
        for i in numbers:
            n += str(i)

        n = int(n) * 2

    return n

    

answer = 1000000
for i in range(1000, 100000):
    if func(i) - i == 1:
        answer = min(func(i), answer)


print("№294:", answer)
