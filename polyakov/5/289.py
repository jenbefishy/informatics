def func(n):
    r = bin(n)[2:]
    if r.count("1") % 2 == 0:
        r = "1" + r + "00"
    else:
        r = "11" + r
    return int(r, 2)
        

answer = 0
for i in range(1, 100000):
    if func(i) >= 412:
        answer = i
        break

print("№289:", answer)
