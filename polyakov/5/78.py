
def func(n):
    sn = str(n)
    s1 = int(sn[0]) + int(sn[1])
    s2 = int(sn[2]) + int(sn[3])
    res = str(max(s1, s2)) + str(min(s1, s2))
    return res

answer = 0
for i in range(1000, 10000):
    if func(i) == "1512":
        answer = i
        break

print("№78:", answer)
