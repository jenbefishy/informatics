
def func(n):
    sn = str(n)
    s1 = int(sn[0]) * int(sn[1])
    s2 = int(sn[1]) * int(sn[2])
    r = str(max(s1, s2)) + str(min(s1, s2))
    return r

answer = 0
for i in range(100, 1000):
    if func(i) == "205":
        answer = i
        break
        
print("№113:", answer)
