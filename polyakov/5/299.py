
def delZero(n):
    res = ""
    found_1 = 0
    for i in n:
        if i != "0":
            found_1 = 1

        if found_1:
            res += i

    return res

def func(n):
    r = bin(n)[2:]
    for _ in range(2):
        if r.count("1") % 2 == 0:
            r = r[1:]
            r = delZero(r)
        else:
            r = "1" + r + "00"



    return int(r, 2)


answer = 0
for i in range(1, 100000):
    if func(i) > 100:
        answer = i
        break

print("№299:", answer)

