answer65 = 0
answer66 = 0
answer67 = 0
answer68 = 0

for i in range(100, 1000):
    s1 = int(str(i)[0]) + int(str(i)[1])
    s2 = int(str(i)[1]) + int(str(i)[2])
    res = str(max(s1, s2)) + str(min(s1, s2))

    if res == "157" and answer65 == 0:
        answer65 = i

    if res == "1412" and answer66 == 0:
        answer66 = i
    if res == "148":
        answer67 = i

    if res == "1513":
        answer68 = i

print("№65:", answer65)
print("№66:", answer66)
print("№67:", answer67)
print("№68:", answer68)
