from itertools import combinations

def toBase(n, base, lenght):
    res = ""
    while n:
        res = str(n % base) + res
        n //= base
    if len(res) < lenght:
        res = "0" * (lenght - len(res)) + res
    return res

def invert(n):
    res = ""
    for i in n:
        if i == "1":
            res += "0"
        else:
            res += "1"
    return res

def func(n):
    sn = str(n)
    r = ""
    for i in sn:
        r += toBase(int(i), 2, 4)
    
    r = invert(r)
    return int(r, 2)


answer = 0
for i in range(1, 10000):
    if func(i) == 151:
        answer = i

print("№240:", answer)
