from itertools import combinations

def func(n):
    if n % 2 == 0:
        n //= 2 
    else:
        n -= 1

    if n % 5 == 0:
        n //= 5
    else:
        n -= 1

    if n % 7 == 0:
        n //= 7
    else:
        n -= 1

    return n

        


values = set()
for i in range(2, 100000):
    if func(i) == 6:
        values.add(i)

print("№264:", len(values))
