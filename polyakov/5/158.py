def func(n):
    sn = bin(n)[2:]
    sn = (8 - len(sn)) * "0" + sn
    r = ""
    for i in sn:
        if i == "0":
            r += "1"
        else:
            r += "0"
    r = int(r, 2) - n
    return r

answer = 0
for i in range(0, 10000):
    if func(i) ==  99:
        answer = i
        break

print("№158:", answer)
