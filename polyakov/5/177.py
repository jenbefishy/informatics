def func(n):
    sn = bin(n)[2:]
    sn = (8 - len(sn)) * "0" + sn
    r = ""
    for i in range(0, len(sn)):
        if sn[i] == "0":
            r += "1"
        else:
            r += "0"


    return int(r, 2) + 1

print("№177:", func(80))
