
def func(n):
    sn = str(n)
    s1 = int(sn[0]) + int(sn[1])
    s2 = int(sn[1]) + int(sn[2])
    s3 = int(sn[2]) + int(sn[3])
    numbers = sorted([s1, s2, s3])
    r = str(numbers[1]) + str(numbers[2])
    return r

answer = 0
for i in range(1000, 10000):
    if func(i) == "1316":
        answer = i
        break
        
print("№117:", answer)
