
def func(n):
    r = bin(n)[2:]
    r += str(r.count("1") % 2)
    r += str(r.count("1") % 2)
    return int(r, 2)

answer = 0
for i in range(1, 1000):
    if 90 <= func(i) <= 160:
        answer += 1
    elif func(i) > 160:
        break

print("№209:", answer)
