from itertools import combinations

def toBase(n, base):
    res = ""
    while n:
        res = str(n % base) + res
        n //= base
    return res

def func(n):
    r = toBase(n, 6)
    r += r[-1]
    r = bin(int(r, 6))[2:]
    r += r[-1]
    return int(r, 2)

answer = 0
for i in range(1, 100000):
    if func(i) < 344:
        answer = func(i)

print("№239:", answer)
