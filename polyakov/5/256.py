from itertools import combinations

def func(n):
    r = bin(n)[2:]

    for _ in range(3):
        if r.count("0") == r.count("1"):
            r += r[-1]
        elif r.count("0") > r.count("1"):
            r += "1"
        else:
            r += "0"

    return int(r, 2)


answer = 0
for i in range(100, 100000):
    if func(i) % 4 == 0 and func(i) % 8 != 0:
        answer = i
        break

print("№256:", answer)
