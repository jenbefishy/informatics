def func(n):
    r = bin(n)[2:]
    if r.count("1") % 2 == 0:
        r += "00"
    else:
        r += "10"
    return r
    

answer = 0
for i in range(0, 10000):
    if 64 <= int(func(i), 2) < 72:
        answer += 1
    
    if int(func(i), 2) > 72:
        break

print("№137:", answer)
