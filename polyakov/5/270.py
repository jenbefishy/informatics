from itertools import combinations

def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r = "1" + r + "00"
    else:
        r = "10" + r + "11"
    return int(r, 2)


answer = 10000000
for i in range(1, 100000):
    if func(i) > 1023:
        answer = min(func(i), answer)

print("№270:", answer)

