def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r = "1" + r + "11"
    else:
        r = "11" + r + "0"
    return int(r, 2)

answer = 0
for i in range(1, 100000):
    if 500 <= func(i) <= 1000:
        answer += 1

print("№287:", answer)
