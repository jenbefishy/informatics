
def func(n):
    sn = str(n)
    s1 = int(sn[0]) + int(sn[2]) + int(sn[4])
    s2 = int(sn[1]) + int(sn[3])

    r = str(min(s1, s2)) + str(max(s1, s2))
    return r

answer = 0
for i in range(10000, 1000000):
    if func(i) == "621":
        answer = i
        break
    
print("№99:", answer)
