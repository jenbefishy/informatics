numbers = "012345600B"

values = {
    "0" : 0,
    "1" : 1,
    "2" : 2,
    "3" : 3,
    "4" : 4,
    "5" : 5,
    "6" : 6,
    "B" : 9
}

def to_base(n, base):
    res = ""
    while n:
        if not (n % base) in [1, 2, 3, 4, 5, 6, 9]:
            return 0

        res = str(numbers[n % base]) + res
        n //= base
    return res

def func(n):
    sn = str(n)
    s1 = values[sn[0]] + values[sn[2]]
    s2 = values[sn[1]] + values[sn[3]]
    r = str(max(s1, s2)) + str(min(s1, s2))
    return r

answer = 0
for i in range(int("1000", 12), int("10000", 12)):
    n = to_base(i, 12)
    if n != 0 and func(n) == "115":
        answer = n
    
print("№111:", answer)
