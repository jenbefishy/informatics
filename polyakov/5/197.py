def func(n):
    sn = bin(n)[2:]
    sn += str(sn.count("1") % 2)
    sn += str(sn.count("1") % 2)
    return int(sn, 2)

answer = 0
for i in range(1, 1000):
    if func(i) < 86:
        answer = func(i)

print("№197:", answer)
