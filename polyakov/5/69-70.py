answer69 = 0
answer70 = 0

for i in range(100, 1000):
    s1 = int(str(i)[0]) + int(str(i)[1])
    s2 = int(str(i)[1]) + int(str(i)[2])
    res = str(max(s1, s2)) + str(min(s1, s2))
    if res == "86":
        answer69 = i
    if res == "43" and answer70 == 0:
        answer70 = i

print("№69:", answer69)
print("№70:", answer70)
        
