from itertools import combinations

def toBase(n, base, lenght):
    res = ""
    while n:
        res = str(n % base) + res
        n //= base
    if len(res) < lenght:
        res = "0" * (lenght - len(res)) + res
    return res

def func(n):
    r = toBase(n, 2, 8)
    return int(r, 2) - int(r[::-1], 2)


answer = 0
for i in range(1, 256):
    if func(i) > answer:
        answer = func(i)

print("№241:", answer)
