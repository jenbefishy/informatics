
def func(n):
    r = hex(n // 2)[2:]
    if n % 4 == 0:
        r = "15" + r + "C"
    else:
        r = "F" + r + "A0"
    return int(r, 16)


answer = 10000000
for i in range(1, 100000):
    if func(i) < 65536:
        answer = i

print("№272:", answer)

