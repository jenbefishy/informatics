
def func(n):
    r = bin(n)[2:]
    r += str(r.count("1") % 2)
    r += str(r.count("1") % 2)
    return int(r, 2)

answer = 0
for i in range(1, 1000):
    if 20 <= func(i) <= 50:
        answer += 1
    elif func(i) > 50:
        break

print("№208:", answer)
