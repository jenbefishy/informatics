from itertools import permutations
def func(n):
    r = bin(n)[2:]

    if r.count("1") % 2 == 0:
        r = "10" + r[2:] + "0"
    else:
        r = "11" + r[2:] + "1"

    return int(r, 2)

answer = 0
for i in range(1, 10000):
    if func(i) < 35:
        answer = i


print("№292:", answer)
