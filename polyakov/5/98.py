
def func(n):
    sn = n
    s1 = int(str(n)[0]) * int(str(n)[1])
    s2 = int(str(n)[1]) * int(str(n)[2])
    r = str(min(s1, s2)) + str(max(s1, s2))
    return r

answer = 0
for i in range(100, 1000):
    if func(i) == "621":
        answer = i
    
print("№98:", answer)
