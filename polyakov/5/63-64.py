answer63 = 0
answer64 = 0

for i in range(1000, 9999):
    s1 = int(str(i)[0]) + int(str(i)[2])
    s2 = int(str(i)[1]) + int(str(i)[3])
    res = str(min(s1, s2)) + str(max(s1, s2))

    if res == "35":
        answer63 = i

    if res == "58" and answer64 == 0:
        answer64 = i

print("№63:", answer63)
print("№64:", answer64)
