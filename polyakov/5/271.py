from itertools import combinations

def toBase(n, base):
    res = ""
    while n:
        res = str(n % base) + res
        n //= base
    return res

def func(n):
    r = toBase(n, 4)
    if n % 2 == 0:
        r = "13" + r + "02"
    else:
        r = "2" + r + "11"
    return int(r, 4)


answer = 10000000
for i in range(1, 100000):
    if func(i) > 1000:
        answer = min(func(i), answer)

print("№271:", answer)

