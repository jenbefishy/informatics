def func(n):
    sn = bin(n)[2:]
    sn = sn.count("1") * "1"
    return int(sn, 2)

values = set()
for i in range(10, 2500):
    values.add(func(i))
    

print("№174:", len(values))
