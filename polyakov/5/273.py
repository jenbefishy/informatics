
def func(n):
    r = bin(n)[2:]
    if n % 2 != 0:
        r = "1" + r + "0"
    else:
        r = "11" + r + "11"
    return int(r, 2)


answer = 0
for i in range(1, 100000):
    if func(i) < 126:
        answer = max(func(i), answer)

print("№273:", answer)
