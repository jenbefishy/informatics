
def func(n1, n2):
    s1 = int(str(n1)[0]) + int(str(n2)[0])
    s2 = int(str(n1)[1]) + int(str(n2)[1])
    s3 = int(str(n1)[2]) + int(str(n2)[2])
    numbers = sorted([s1, s2, s3])
    res = ""
    for i in numbers:
        res += str(i)
    return res

answer = 0
for i in range(100, 1000):
    if func(i, 497) == "71113":
        answer = i

print("№76:", answer)
