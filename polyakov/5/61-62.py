answer61 = 0
answer60 = 0

for i in range(1000, 9999):
    s1 = int(str(i)[0]) + int(str(i)[2])
    s2 = int(str(i)[1]) + int(str(i)[3])
    res = str(min(s1, s2)) + str(max(s1, s2))

    if res == "1113" and answer61 == 0:
        answer61 = i

    if res == "1315":
        answer62 = i

print("№61:", answer61)
print("№62:", answer62)
