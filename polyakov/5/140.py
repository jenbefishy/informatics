def func(n):
    r = bin(n)[2:]
    r += r[-1]
    if r.count("1") % 2 == 0:
        r += "00"
    else:
        r += "10"
    return r
    

answer = 0
for i in range(0, 10000):
    if int(func(i), 2) > 66:
        answer = int(func(i), 2)
        break

print("№140:", answer)
