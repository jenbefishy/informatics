
def func(n):
    sn = str(n)
    s1 = int(sn[0]) + int(sn[1])
    s2 = int(sn[2]) + int(sn[3])
    res = str(min(s1, s2)) + str(max(s1, s2))
    return res

answer = 0
for i in range(1000, 10000):
    if func(i) == "117":
        answer = i

print("№79:", answer)
