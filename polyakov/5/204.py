
def func(n):
    r = bin(n)[2:]
    r += str(r.count("1") % 2)
    r += str(r.count("1") % 2)
    return int(r, 2)

answer = 0
for i in range(1, 1000):
    if func(i) > 80:
        answer = func(i)
        break

print("№204:", answer)
