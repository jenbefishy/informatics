from itertools import combinations

def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r = "1" + r
    else:
        r += "0"

    if r.count("1") % 2 == 0:
        r += "1"
    else:
        r += "0"

    return r 

answer = 0
for i in range(1, 100000):
    if int(func(i), 2) > 228:
        answer = i
        break

print("№237:", answer)
