from itertools import combinations

def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r += "1"
    else:
        r += "0"

    if int(r, 2) % 2 == 0:
        r += "1"
    else:
        r += "0"

    return int(r, 2)

answer = 0
for i in range(1, 100000):
    if func(i) < 171:
        answer = i

print("№236:", answer)
