answer59 = 0
answer60 = 0

for i in range(1000, 9999):
    s1 = int(str(i)[0]) + int(str(i)[1])
    s2 = int(str(i)[2]) + int(str(i)[3])
    res = str(min(s1, s2)) + str(max(s1, s2))

    if res == "912":
        answer59 = i

    if res == "79" and answer60 == 0:
        answer60 = i

print("№59:", answer59)
print("№60:", answer60)
