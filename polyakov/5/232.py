from itertools import combinations

def func(n):
    r = bin(n)[2:]
    if r.count("1") > r.count("0"):
        r += "0"
    else:
        r += "1"
    mid = len(r) // 2
    if len(r) % 2 == 0:
        r = r[0:mid-1] + r[mid+1:]
    else:
        r = r[0:mid-1] + r[mid+2:]
    return int(r, 2)

answer = 0
for i in range(10, 1000):
    if func(i) == 55:
        answer = i

print("№232:", answer)
