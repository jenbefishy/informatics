def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r += bin(r.count("1"))[2:]
    else:
        r = "1" + r + "00"
    return int(r, 2)


answer = 0

for i in range(1, 10000000):
    if func(i) > 215:
        answer = i
        break

print("№282:", answer)
