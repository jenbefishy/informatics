from itertools import combinations

def func(n):
    r = bin(n)[2:]
    r += r[-2]  + r[1]
    return int(r, 2)

answer = 0
for i in range(10, 1000):
    if 150 <= func(i) <= 200:
        answer += 1
    elif func(i) > 200:
        break

print("№230:", answer)
