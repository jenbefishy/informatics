from itertools import combinations

def func(n):
    if n % 2 == 0:
        n //= 2 
    else:
        n -= 1

    if n % 3 == 0:
        n //= 3
    else:
        n -= 1

    if n % 5 == 0:
        n //= 5
    else:
        n -= 1

    return n

        


values = set()
for i in range(2, 100000):
    if func(i) == 3:
        values.add(i)

print("№262:", len(values))
