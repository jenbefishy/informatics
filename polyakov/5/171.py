def func(n):
    sn = bin(n)[2:]
    sn = (8 - len(sn)) * "0" + sn
    sn = sn[0:2] + sn[6:]
    return int(sn, 2)

answer = 0
for i in range(131, 100000):
    if func(i) == 10:
        answer = i
        break
    

print("№171:", answer)
