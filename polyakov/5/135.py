def func(n):
    r = bin(n)[2:]
    if r.count("1") % 2 == 0:
        r += "10"
    else:
        r += "00"
    return r
    

answer = 0
for i in range(0, 10000):
    if 16 <= int(func(i), 2) <= 32:
        answer += 1
    
    if int(func(i), 2) > 32:
        break

print("№135:", answer)
