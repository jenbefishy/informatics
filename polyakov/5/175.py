def func(n):
    sn = bin(n)[2:]
    sn = sn[0:len(sn) - 2]
    return int(sn, 2)

values = set()
for i in range(20, 601):
    values.add(func(i))
    

print("№175:", len(values))
