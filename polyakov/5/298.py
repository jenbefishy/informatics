def func(n):
    r = bin(n)[2:]
    for _ in range(2):
        if r.count("1") % 2 == 0:
            r = r[r.index("1")+1:]
        else:
            r = (r.count("1") + 1) * "1"


    return int(r, 2)


answer = 0
for i in range(1, 1000):
    if func(i) == 7:
        answer += 1

print("№298:", answer)

