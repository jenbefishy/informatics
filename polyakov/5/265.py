from itertools import combinations

def func(n):
    if n % 3 == 0:
        n //= 3 
    else:
        n -= 1

    if n % 7 == 0:
        n //= 7
    else:
        n -= 1

    if n % 11 == 0:
        n //= 11
    else:
        n -= 1

    return n

        


values = set()
for i in range(2, 100000):
    if func(i) == 6:
        values.add(i)

print("№265:", len(values))
