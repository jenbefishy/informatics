from itertools import combinations

def func(n):
    r = bin(n)[2:]
    r += str(r.count("1") % 2)

    if bin(n)[2:].count("1") > bin(n)[2:].count("0"):
        r += "0"
    else:
        r += "1"

    return int(r, 2)


answer = 1000000
for i in range(1, 100000):
    if  80 < func(i) < answer:
        answer = func(i)

print("№257:", answer)
