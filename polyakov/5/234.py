from itertools import combinations

def func(n):
    r = bin(n)[2:]
    if r.count("1") > r.count("0"):
        r += "0"
    else:
        r += "1"
    mid = len(r) // 2
    if len(r) % 2 == 0:
        r = r[:mid-1] + r[mid+1:]
    else:
        r = r[:mid-1] + r[mid+2:]
    return int(r, 2)


values = set()
for i in range(10, 100000):
    if 50 <= func(i) <= 100:
        values.add(func(i))

print("№234:", len(values))
