from itertools import combinations


def func(n):
    r = bin(n)[2:]
    r += r[-2]  + r[1]
    return int(r, 2)

answer = 0
for i in range(10, 1000):
    if func(i) > 170:
        answer = i
        break

print("№224:", answer)
