def func(n):
    sn = bin(n)[2:]
    
    if sn.count("1") > sn.count("0"):
        sn += "1"
    else:
        sn += "0"

    return int(sn, 2)

answer = 0
for i in range(1, 1000):
    if func(i) < 43:
        answer = func(i)

print("№192:", answer)
