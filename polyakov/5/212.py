from itertools import combinations


def func(n):
    cmb = list("".join(number) for number in combinations(str(n), 2))
    max_value = 0
    min_value = n
    for i in cmb:
        if i[0] != "0":
            max_value = max(max_value, int(i))
            min_value = min(min_value, int(i))

        if i[-1] != "0":
            max_value = max(max_value, int(i[::-1]))
            min_value = min(min_value, int(i[::-1]))
        
    return max_value - min_value

answer = 0
for i in range(100, 1000):
    if func(i) == 50:
        answer = i

print("№212:", answer)
