answer57 = 0
answer58 = 0

for i in range(1000, 9999):
    s1 = int(str(i)[0]) + int(str(i)[1])
    s2 = int(str(i)[2]) + int(str(i)[3])
    res = str(max(s1, s2)) + str(min(s1, s2))

    if res == "1412":
        answer57 = i

    if res == "1412" and answer58 == 0:
        answer58 = i

print("№57:", answer57)
print("№58:", answer58)
