def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r = r + "10"
    else:
        r = "1" + r + "01"
    return int(r, 2)

answer = 0
for i in range(1, 100000):
    if func(i) > 516:
        answer = i
        break

print("№285:", answer)
