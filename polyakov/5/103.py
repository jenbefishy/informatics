
def func(n):
    s1 = n % 4
    s2 = n % 2
    s3 = n % 5

    r = str(s1) + str(s2) + str(s3)
    return r

answer = 0
for i in range(10, 100):
    if func(i) == "313":
        answer = i
        break
    
print("№103:", answer)
