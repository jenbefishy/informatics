from itertools import permutations
def func(n):
    sn = str(n)
    s = 0
    for i in sn:
        s += int(i)
    s = bin(s)[2:]

    if s.count("1") % 2 == 0:
        s = "1" + s + "00"
    else:
        s = "10" + s + "1"

    return int(s, 2)

"""

 10101 (bin)  - 21
 10 + 10 + 1 = 10101
 10 (bin)     - 2
 2 = 1 + 1 = 2 + 0 

"""

variants = \
    list(set("".join(number) for number in permutations("200000000"))) + \
    list(set("".join(number) for number in permutations("000000011")))

answer = 0
for i in variants:
    if i[0] == "0":
        continue

    if func(int(i)) == 21:
        answer += 1

print("№290:", answer)
