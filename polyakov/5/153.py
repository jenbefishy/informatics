def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r += "01"
    else:
        r += "10"
    return r

answer = 0
for i in range(0, 10000):
    if int(func(i), 2) > 130:
        answer = int(func(i), 2)
        break

print("№153:", answer)
