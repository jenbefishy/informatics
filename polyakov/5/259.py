from itertools import combinations

def func(n):
    r = bin(n)[2:]
    if r.count("0") == 0:
        return -1

    index = 0
    for i in range(len(r)):
        if r[i] == "0":
            index = i
            
    
    r = r[:index] + r[0] + r[1] + r[index+1:]
    r = r[::-1]

    return int(r, 2)
        


answer = 0
for i in range(2, 10000):
    if func(i) == 119:
        answer = i

print("№259:", answer)
