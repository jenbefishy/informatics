

def func(n):
    sn = str(n)
    s1 = int(sn[0]) + int(sn[1])
    s2 = int(sn[1]) + int(sn[2])
    s3 = int(sn[2]) + int(sn[3])
    numbers = sorted([s1, s2, s3])[1::]

    r = str(max(numbers)) + str(min(numbers))
    return r

answer = 0
for i in range(1000, 10000):
    if func(i) == "157":
        answer = i
    

print("№121:", answer)



