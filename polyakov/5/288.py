def func(n):
    r = bin(n)[2:]
    if n % 2 == 0:
        r = "1" + r + "10"
    else:
        r = "11" + r + "0"
    return int(r, 2)

values = set()
for i in range(1, 100000):
    if 800 <= func(i) <= 1500:
        values.add(func(i))

print("№288:", len(values))
