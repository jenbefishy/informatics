
def func(n):
    r = bin(n)[2::]

    if r.count("1") % 2 == 0:
        r += "00"
    else:
        r += "10"

    return r

answer = 0
for i in range(1, 1000):
    if int(func(i), 2) > 116:
        answer = int(func(i), 2)
        break
    
print("№94:", answer)
