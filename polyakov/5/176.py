def func(n):
    sn = bin(n)[2:]
    r = sn[0]
    for i in range(1, len(sn)):
        if sn[i] == "0":
            r += "1"
        else:
            r += "0"
    return int(r, 2) + n

answer = 0
for i in range(1, 10000):
    if func(i) <= 123:
        answer = i
    

print("№176:", answer)
