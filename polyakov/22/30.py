from csv import reader

file = open("22-30.csv")

rdr = reader(file)
next(rdr)

finished  = {}
processes = {}

for line in rdr:

    id      = int(line[0])
    time    = int(line[1])
    depends = list(map(int, line[2].split(";")))

    if depends == [0]:
        finished[id]  = time
    else:
        processes[id] = (time, depends)


while processes:
    ids = list(processes.keys())
    for id in ids:
        time, depends = processes[id]

        countOfDepends = len(depends)
        finishedDependsTimes = []

        for i in depends:
            if i in finished:
                finishedDependsTimes.append(finished[i])

        finishedDependsTimes.sort()
        count = 0
        dependsTime = 0
        for i in finishedDependsTimes:
            count += 1
            dependsTime = i

            if count >= countOfDepends * 0.5:
                break

       


        if count >= countOfDepends * 0.5:
            finished[id] = dependsTime + time
            del processes[id]

print(max(finished.values()))

