from csv import reader

file = open("22-39.csv")
rdr = reader(file)
next(rdr)

finished = {}
processes = {}

for line in rdr:
    line.pop()

    id      = int(line[0])
    time    = int(line[1])
    depends = list(map(int, line[2].split(";")))

    if depends == [0]:
        finished[id] = time
    else:
        processes[id] = (time, depends)


while processes:
    ids = list(processes.keys())
    for id in ids:
        time, depends = processes[id]

        delay = 7
        
        if all(x in finished for x in depends):
            startTime = max(finished[x] for x in depends)
            finished[id] = startTime + time + delay
            del processes[id]


print(max(finished.values()))

file.close()
