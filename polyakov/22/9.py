from csv import reader
file = open("22-6.csv")

rdr = reader(file)
next(rdr)


finished  = {}
processes = {}

firstTime = 0

for line in rdr:

    id      = int(line[0])
    time    = int(line[1])
    depends = list(map(int, line[2].split(";")))

    if depends == [0]:
        finished[id] = time
    else:
        processes[id] = (time, depends)


    firstTime = max(firstTime, time)

while processes:
    ids = list(processes.keys())
    for id in ids:
        time, depends = processes[id]
        if all(x in finished for x in depends):
            startTime = max(finished[x] for x in depends)
            finished[id] = startTime + time
            del processes[id]

secondTime = max(finished.values())
print(abs(secondTime - firstTime))




