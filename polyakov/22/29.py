from csv import reader

file = open("22-29.csv")

rdr = reader(file)
next(rdr)

finished  = {}
processes = {}

for line in rdr:

    id      = int(line[0])
    time    = int(line[1])
    depends = list(map(int, line[2].split(";")))

    if depends == [0]:
        finished[id]  = time
    else:
        processes[id] = (time, depends)


while processes:
    ids = list(processes.keys())
    for id in ids:
        time, depends = processes[id]

        dependsId   = -1
        dependsTime = 1e6

        for i in depends:
            if i in finished and dependsTime > finished[i]:
                dependsId   = i
                dependsTime = finished[i]

        if dependsId != -1:
            finished[id] = dependsTime + time
            del processes[id]

print(max(finished.values()))

