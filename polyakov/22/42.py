from csv import reader

with open("22-42.csv") as F:
    rdr = reader(F)
    next(rdr)

    data = {}
    finished = {}


    for i in rdr:
        id, time = int(i[0]), int(i[1])
        dep = list(map(int, i[2].split(";")))

        if dep == [0]:
            finished[id] = time
        else:
            data[id] = (time, dep)


while data:
    ids = list(data.keys())
    for id in ids:
        time, dep = data[id]

        if all(x in finished for x in dep):
            startTime = max(finished[x] for x in dep)
            finished[id] = startTime + time
            del data[id]

print(max(finished.values()))
