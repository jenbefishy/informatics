from csv import reader

def getTime(data, finished):
    while data:
        ids = list(data.keys())
        for id in ids:
            time, dep = data[id]
            if all(i in finished for i in dep):
                startTime = max(finished[x] for x in dep)
                finished[id] = startTime + time
                del data[id]
    return max(finished.values())



with open("22-58.csv") as F:
    rdr = reader(F)
    next(rdr)

    finished = {}
    data = {}
    xProcess = 0
    xProcessTime = 0

    for i in rdr:
        id, time = int(i[0]), int(i[1])

        if i[2] == "x":
            xProcess = id
            xProcessTime = time
            dep = -1
            continue 

        dep = list(map(int, i[2].split(";")))

        if dep == [0]:
            finished[id] = time
        else:
            data[id] = (time, dep)


    for x in range(min(data.keys()), max(data.keys())):
        data[xProcess] = (xProcessTime, [x])
        if getTime(data, finished) == 17:
            print(x)
            break

