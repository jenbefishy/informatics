from csv import reader
file = open("22-5.csv")
rdr = reader(file)

for _ in range(2): next(rdr)

finished  = {}
processes = {}


# Read the first table
count = 0
for line in rdr:
    id      = int(line[0])
    time    = int(line[1])
    depends = list(map(int, line[2].split(";")))

    if depends == [0]:
        finished[id]  = time
    else:
        processes[id] = (time, depends)

    count += 1
    if count == 12:
        break

while processes:
    ids = list(processes.keys())
    for id in ids:
        time, depends = processes[id]

        if all(x in finished for x in depends):
            startTime = max(finished[x] for x in depends)
            finished[id] = startTime + time
            del processes[id] 

P1 = max(finished.values())

processes = {}
finished  = {}

for _ in range(4): next(rdr)

# Read the second table
for line in rdr:
    id      = int(line[0])
    time    = int(line[1])
    depends = list(map(int, line[2].split(";")))

    if depends == [0]:
        finished[id]  = time
    else:
        processes[id] = (time, depends)

while processes:
    ids = list(processes.keys())
    for id in ids:
        time, depends = processes[id]

        if all(x in finished for x in depends):
            startTime = max(finished[x] for x in depends)
            finished[id] = startTime + time
            del processes[id] 

P2 = max(finished.values())

print(abs(P1 - P2))
file.close()
