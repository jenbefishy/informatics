from csv import reader

file = open("22-55.csv")
rdr = reader(file)
next(rdr)

finished = {}
processes = {}

for line in rdr:
    line.pop()

    id      = int(line[0])
    time    = int(line[1])
    depends = list(map(int, line[2].split(";")))

    if depends == [0]:
        finished[id] = time
    else:
        processes[id] = (time, depends)


while processes:
    ids = list(processes.keys())
    for id in ids:
        time, depends = processes[id]

        if all(x in finished for x in depends):
            startTime = max(finished[x] for x in depends)
            finished[id] = startTime + time
            del processes[id]


answer = 0
for i in finished.values():
    if i < 60:
        answer += 1
print(answer)


file.close()
