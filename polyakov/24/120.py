
file = open("24-2.txt")
line = file.readline()


maxLen = 0
count = 0
start = 0
answer = 0


for i in range(len(line) - 1):
    a = line[i]
    b = line[i + 1]
    if b < a:
        count += 1
    else:
        if count > maxLen:
            answer = start
            maxLen = count

        start = i + 1
        count = 1

print(answer + 1)
