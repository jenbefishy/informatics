from fnmatch import fnmatch

file = open("24-s1.txt")

count = 0
for line in file.readlines():
    for a, b, c in zip(line, line[1:], line[2:]):
        if fnmatch(a + b + c, "F*O"):
            count += 1
            break

print(count)
