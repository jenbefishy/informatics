from string import ascii_uppercase

file = open("24-s2.txt")
line = file.readline()
file.close()

data = {x: 0 for x in ascii_uppercase}

for a, b, c in zip(line, line[1:], line[2:]):
    if a == "X" and c == "Z":
        data[b] += 1

maxCount = max(data.values())
answer = []
for i in data.keys():
    if data[i] == maxCount:
        answer += [i]
answer.sort()
print(answer[0], maxCount, sep="")



