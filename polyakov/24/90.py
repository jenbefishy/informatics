
file = open("24-1.txt")
line = file.readline()

answer = 10 ** 10
number = ""

for i in line:
    if i in "0123456789":
        number += i
    else:
        if len(number) != 0 and int(number) % 2 == 0:
            answer = min(int(number), answer)
        number = ""

print(answer)
