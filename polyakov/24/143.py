from fnmatch import fnmatch
file = open("24-s1.txt")
lines = file.readlines()
file.close()

count = 0
for line in lines:
    for a, b, c, d in zip(line, line[1:], line[2:], line[3:]):
        if fnmatch(a + b + c + d, "Z*RO"):
            count += 1
print(count)
