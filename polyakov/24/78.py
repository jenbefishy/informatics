
file = open("k8-2.txt")
line = file.readline()

count = 0
maxLen = 0

for a, b in zip(line, line[1:]):
    if a != b:
        count += 1
    else:
        maxLen = max(count, maxLen)
        count = 1

print(maxLen)
