
file = open("24-j7.txt")
line = file.readline()
file.close()

prev = "0"
answer = 0
count = 1
for i in line:
    if i == "\n":
        continue

    prev = int(prev)
    i = int(i)
    if prev % 2 == i % 2:
        count += 1
    else:
        answer = max(answer, count)
        count = 1
    prev = i

print(answer)
