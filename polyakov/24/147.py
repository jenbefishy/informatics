from string import ascii_uppercase

file = open("24-s2.txt")
line = file.readline()
file.close()

prev = ""
data = {x: 0 for x in ascii_uppercase}

for i in line:
    if prev == "A":
        data[i] += 1
    prev = i

answer = []
maxCount = max(data.values())
for i in data.keys():
    if data[i] == maxCount:
        answer += [i]
answer.sort()
print(answer[0], maxCount, sep="")
