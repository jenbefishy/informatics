from string import ascii_uppercase

file = open("24-164.txt")
lines = file.readlines()
file.close()

maxLinesLen = 0
answerLine = ""

for line in lines:

    maxLineLen = 0
    len = 0
    prev = ""
    for i in line:
        if i == prev:
            len += 1
        else:
            maxLineLen = max(len, maxLineLen)
            len = 1
        prev = i

    if maxLinesLen < maxLineLen:
        maxLinesLen = maxLineLen
        answerLine = line

letter = ""
count = 10e10
for c in ascii_uppercase:
    if answerLine.count(c) <= count:
        count = answerLine.count(c)
        letter = c

answer = 0
for line in lines:
    answer += line.count(letter)

print(letter, answer, sep="")

