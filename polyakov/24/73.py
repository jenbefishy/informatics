
file = open("k8-4.txt")
line = file.readline()

count = 0
maxLen = 0

maxChars = []
current = 0

for i in line:
    if current != i:
        if count > maxLen:
            maxLen = count
            maxChars = [current]
        elif count == maxLen:
            maxChars.append(current)

        current = i
        count = 1
    else:
        count += 1

for i in maxChars:
    print(i, maxLen)
