
file = open("24-153.txt")
line = file.readline()
file.close()

start = 0
opened = False
answer = 10e6
for i in range(len(line)):
    c = line[i]
    if c == "D":
        if opened == False:
            start = i
            opened = True
        else:
            if i - start != 1:
                answer = min(i - start + 1, answer)
            opened = False


print(answer)

