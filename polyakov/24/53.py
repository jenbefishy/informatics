
file = open("k8-4.txt")
line = file.readline()

current = ""
count = 0

maxLen = 0
maxChar = ""

for i in line:
    if i == current:
        count += 1
    else:

        if count > maxLen:
            maxLen = count
            maxChar = current

        current = i
        count = 1

print(maxChar, maxLen)
