file = open("k7b-6.txt")
line = file.readline()


word = "DAF"
while word in line:
    if word[-1] == "D": word += "A"
    elif word[-1] == "A": word += "F"
    elif word[-1] == "F": word += "D"

print(len(word) - 1)
