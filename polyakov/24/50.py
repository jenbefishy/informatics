
file = open("k7-m25.txt")
line = file.readline()

count = 0
answer = 0

for i in range(1, len(line) - 1):
    a = line[i - 1]
    b = line[i]
    c = line[i + 1]
    if b > a and b > c:
        count += 1
        answer = i - 1

print(count, answer)
