
file = open("24-4.txt")
line = file.readline()


count = 1
answer = 0
pos = 0

for i in range(1, len(line)):
    if line[i - 1] > line[i]:
        count += 1
        if count > answer:
            pos = i
            answer = count
    else:
        count = 1


print(pos - answer + 2)
