
file = open("24-153.txt")
line = file.readline()
file.close()

count = 0
answer = 10e10
prev = ""
for i in line:
    if prev == "D" and i == "D":
        count += 1
    else:
        if count > 1:
            answer = min(count, answer)
        count = 1
    prev = i

print(answer)

