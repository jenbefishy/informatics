from string import ascii_uppercase

file = open("24-s1.txt")
lines = file.readlines()
file.close()

answerLine = ""
aCount = 10e10

for line in lines:
    if line.count("A") < aCount:
        answerLine = line
        aCount = line.count("A")

count = 0
letter = ""
for i in ascii_uppercase:
    if answerLine.count(i) >= count:
        count = answerLine.count(i)
        letter = i

answer = 0
for line in lines:
    answer += line.count(letter)

print(letter, answer, sep="")
