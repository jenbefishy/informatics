
file = open("24-1.txt")
line = file.readline()


answer = 0
number = ""


for i in line:
    if i in "0123456789":
        number += i
    else:
        if len(number) != 0 and int(number) % 2 != 0:
            answer = max(answer, int(number))
        number = ""

print(answer)

