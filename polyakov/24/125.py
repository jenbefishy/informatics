
file = open("24-5.txt")
line = file.readline()


answer = 0

for a, b in zip(line, line[1:]):
    if a + b == "()":
        answer += 1

print(answer)
