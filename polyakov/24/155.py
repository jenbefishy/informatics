
file = open("24-153.txt")
line = file.readline()
file.close()

opened = False
answer = 10e10

start = 0

for i in range(len(line)):
    c = line[i]

    if c == "A":
        if opened == False:
            opened = True
            start = i
        else:
            start = i
    elif c == "F":
        if opened == True:
            opened = False
            if i - start != 1:
                answer = min(answer, i - start + 1)

print(answer)
