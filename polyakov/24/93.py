
file = open("24.txt")
line = file.readline()

count = 0
answer = 0

for a, b in zip(line, line[1:]):
    if b > a:
        count += 1
    else:
        answer = max(count, answer)
        count = 1

print(answer)

