file = open("k7c-1.txt")
line = file.readline()

count = 0

for a, b, c in zip(line, line[1:], line[2:]):
    if a in "BCD" and \
       b in "BDE" and b != a and \
       c in "BCE" and c != b:
        count += 1



print(count)
