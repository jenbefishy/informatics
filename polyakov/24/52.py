
file = open("k8-0.txt")
line = file.readline()

current = ""
count = 0

maxLen = 0
maxChar = ""

for i in line:
    if i != current:
        if count > maxLen:
            maxLen = count
            maxChar = current

        count = 1
        current = i
    else:
        count += 1

print(maxChar, maxLen)
