
file = open("24-1.txt")
line = file.readline()

last = 0
answer = 0

for i in range(1, len(line) - 1):
    if line[i] > line[i - 1] and line[i] > line[i + 1]:
        if last != 0:
            answer = max(i - last, answer)
        last = i


print(answer)


