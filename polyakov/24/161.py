from string import ascii_uppercase

file = open("24-s1.txt")
lines = file.readlines()
file.close()

answerLine = ""
qCount = 0

for line in lines:
    if line.count("Q") >= qCount:
        qCount = line.count("Q")
        answerLine = line

letter = ""
count = 10e10
for i in ascii_uppercase:
    if answerLine.count(i) < count and answerLine.count(i) != 0:
        letter = i
        count = answerLine.count(i)

answer = 0
for line in lines:
    answer += line.count(letter)

print(letter, answer, sep="")


