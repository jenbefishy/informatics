
file = open("24-2.txt")
line = file.readline()

answer = ""
maxLen = 0
current = ""
count = 0

for a, b in zip(line, line[1:]):
    if b > a:
        count += 1
        current += b
    else:

        if count > maxLen:
            answer = current
            maxLen = count

        count = 1
        current = b

print(answer)
