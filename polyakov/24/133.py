
file = open("24-j2.txt")
line = file.readline()

current = ""
count = 1
answer = 0
for i in line:
    if i == current:
        count += 1
        answer = max(count, answer)
    else:
        count = 1
        current = i

print(answer)
