
file = open("24-j9.txt")
line = file.readline()
file.close()

left = 0
right = len(line) - 1

count = 0
while left != right:
    if line[left] == line[right]:
        count += 1
    left += 1
    right -= 1

print(count)
