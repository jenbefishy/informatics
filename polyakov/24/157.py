from string import ascii_uppercase
file = open("24-157.txt")
line = file.readline()
file.close()

data = {x: 0 for x in ascii_uppercase}

for a, b, c in zip(line, line[1:], line[2:]):
    if a == b:
        data[c] += 1

answer = []
maxCount = max(data.values())
for i in data.keys():
    if data[i] == maxCount:
        answer.append(i)
answer.sort()
print(answer[0], maxCount, sep="")
