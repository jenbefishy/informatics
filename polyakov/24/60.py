
file = open("k8-48.txt")
line = file.readline()

current = ""
count = 0

maxChar = ""
maxLen = 0

for i in line:
    if current == i:
        count += 1
    else:

        if count > maxLen:
            maxLen = count
            maxChar = current

        current = i
        count = 1

print(maxChar, maxLen)
