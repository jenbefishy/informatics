file = open("k7b-2.txt")
line = file.readline()


word = "DBAC"
while word in line:
    if word[-1] == "D": word += "B"
    if word[-1] == "B": word += "A"
    if word[-1] == "A": word += "C"
    if word[-1] == "C": word += "D"


print(len(word) - 1)
