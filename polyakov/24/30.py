file = open("k7b-4.txt")
line = file.readline()


word = "EBCF"
while word in line:
    if word[-1] == "E": word += "B"
    elif word[-1] == "B": word += "C"
    elif word[-1] == "C": word += "F"
    elif word[-1] == "F": word += "E"


print(len(word) - 1)
