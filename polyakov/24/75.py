
file = open("k8-12.txt")
line = file.readline()

maxLen = 0
count = 0
current = 0
maxChars = []

for i in line:
    if current == i:
        count += 1
    else:
        if count > maxLen:
            maxLen = count
            maxChars = [current]
        elif count == maxLen:
            maxChars.append(current)

        current = i
        count = 1

for i in maxChars:
    print(i, maxLen)
