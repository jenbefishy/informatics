from string import ascii_uppercase

file = open("24-s1.txt")
lines = file.readlines()
file.close()


pairCountMin = 10e10
answerLine = ""

for line in lines:
    pairCount = 0
    for a, b in zip(line[0:], line[1:]):
        if a + b in ascii_uppercase:
            pairCount += 1

    if pairCount < pairCountMin:
        answerLine = line
        pairCountMin = pairCount


count = 0
letter = 0
for c in ascii_uppercase:
    if answerLine.count(c) >= count:
        count = answerLine.count(c)
        letter = c

answer = 0
for line in lines:
    answer += line.count(letter)

print(letter, answer, sep="")




