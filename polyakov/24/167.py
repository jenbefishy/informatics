
file = open("24-164.txt")
lines = file.readlines()
file.close()

answer = 0

for line in lines:
    if line.count("E") >= 20:
        continue

    for i in range(len(line)):
        for j in range(len(line)):
            if line[j] == line[i]:
                answer = max(j - i, answer)

print(answer)
