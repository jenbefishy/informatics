from functools import lru_cache

@lru_cache(None)
def game(x, y):
    if x + y >= 30:
        return 0

    positions = [game(x + 1, y), game(x * 2, y),
                 game(x, y + 1), game(x, y * 2)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)

ans1 = 0
ans2 = [10000, 0]
ans3 = 0

for x in range(1, 30):
    for y in range(1, 30):
        res = game(x, y)

        if res == -1:
            ans1 += 1

        if x == 6 and res == 2:
            ans2[0] = min(ans2[0], y)
            ans2[1] = max(ans2[1], y)

        if res == -2:
            ans3 += 1
        
print(ans1)
print(ans2)
print(ans3)


