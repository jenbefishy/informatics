from functools import lru_cache

@lru_cache(None)
def game(x, y):

    if x + y >= 40:
        return 0

    positions = [game(x + 1, y), game(x * 2, y),
                 game(x, y + 1), game(x, y * 2)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for x in range(1, 31):
    print(x, ":", game(9, x))
