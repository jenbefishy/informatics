from functools import lru_cache

@lru_cache(None)
def game(x):
    if x >= 51:
        return 0

    positions = []

    if (x + 1) % 2 != 0:
        positions.append(game(x + 1))

    if (x + 3) % 2 != 0:
        positions.append(game(x + 3))

    if (x * 3) % 2 != 0:
        positions.append(game(x * 3))

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for x in range(1, 51):
    print(x, ":", game(x))
