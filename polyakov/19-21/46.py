from functools import lru_cache

@lru_cache(None)
def game(x):
    if 45 <= x <= 112 :
        return 0

    if x > 112:
        return 1

    positions = [game(x + 2), game(x * 3)]

    negative = []

    for c in positions:
        if c <= 0:
            negative.append(c)


    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)

for x in range(1, 45):
    print(x, ":", game(x))

