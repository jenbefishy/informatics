from functools import lru_cache

@lru_cache(None)
def game(x):
    if x >= 1000:
        return 0

    positions = [game(x + 100), game(x * 2)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for x in range(1, 1000):
    print(x, ":", game(x))

