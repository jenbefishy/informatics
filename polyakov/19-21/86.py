from functools import lru_cache

@lru_cache(None)
def game(x, y):
    if x + y >= 45:
        return 0

    positions = [game(x + y, y), game(x, y + x)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


ans2 = []
ans3 = 0

for s in range(1, 44):
    res = game(6, s)
    if res == 2:
        ans2.append(s)

for s in range(1, 45):
    res = game(s, s)
    if res == -2:
        ans3 = s
        break

print(min(ans2), max(ans2))
print(ans3)
    
