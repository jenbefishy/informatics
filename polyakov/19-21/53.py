from functools import lru_cache

@lru_cache(None)
def game(x):
    if x < 10:
        return 0
    positions = [game(x - 1), game(x - 2), game(x - 3),\
            game(x - 4), game(x - 5)]

    if x % 2 == 0:
        positions.append(game(x // 2))

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for x in range(10, 50):
    print(x, ":", game(x))
