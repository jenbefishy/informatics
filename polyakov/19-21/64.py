from functools import lru_cache

@lru_cache(None)
def game(x, y):
    if x + y >= 45:
        return 0

    positions = [game(x + 2, y), game(x * 3, y),
                game(x, y + 2), game(x, y * 3)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions) 



ans1 = 0
ans2 = [10000, 0]
ans3 = 0

for k in range(1, 45):
    for s in range(1, 43 - k + 1):
        res = game(k, s)

        if res == -1:
            ans1 += 1

        if k == 4 and res == 2:
            ans2[0] = min(ans2[0], s)
            ans2[1] = max(ans2[1], s)

        if k == 13 and res == -2:
            ans3 = s


print(ans1)
print(ans2)
print(ans3)



