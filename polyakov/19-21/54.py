from functools import lru_cache

@lru_cache(None)
def game(x, y):
    if (x + y) >= 83:
        return 0

    positions = [game(x + 1, y), game(x * 4, y),
                 game(x, y + 1), game(x, y * 4)]

    negative = []
    for i in positions:
        if i <= 0:
            negative.append(i)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)

for x in range(1, 78):
    print(x, ":", game(5, x))
