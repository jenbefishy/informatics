from functools import lru_cache

@lru_cache(None)
def game(x, y):
    if x + y >= 70:
        return 0

    positions = [game(x + 1, y), game(x * 3, y),
                game(x, y + 1), game(x, y * 3)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for s in range(64, 0, -1):
    print(s, ":", game(6, s))

