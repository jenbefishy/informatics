from functools import lru_cache

@lru_cache(None)
def game(x):
    if x >= 25:
        return 0

    positions = [game(x + 2), game(x * 2)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for x in range(1, 25):
    print(x, ":", game(x))

