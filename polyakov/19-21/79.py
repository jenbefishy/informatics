from functools import lru_cache

@lru_cache(None)
def game(x):
    if 20 <= x <= 30:
        return 0

    if x >= 30:
        return 1

    positions = [game(x + 1), game(x * 2)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)

    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for s in range(19, 0, -1):
    print(s, ":", game(s))

