from functools import lru_cache

@lru_cache(None)
def game(x, y):
    if x + y >= 79:
        return 0

    positions = [game(x + 1, y), game(x + y, y),
                game(x, y + 1), game(x, y + x)]

    negative = []
    for c in positions:
        if c <= 0:
            negative.append(c)


    if negative:
        return -max(negative) + 1
    else:
        return -max(positions)


for s in range(69, 0, -1):
    print(s, ":", game(9, s))
