a = set()

def toBase(n, base):
    res = ""
    while n:
        res = str(n % base) + res
        n //= base
    return res

def fill(x):
    return "0" * max(8 - len(x), 0) + x

def f(x):
    P = x.startswith("11")
    Q = x.endswith("0")
    A = x in a

    return (not A) <= (P or (not Q))

for x in range(0, int("100000000", 2)):
    number = fill(toBase(x, 2))

    if f(number) == 0:
        a.add(number)

print(a)
print(len(a))

