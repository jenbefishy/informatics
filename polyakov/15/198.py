from itertools import permutations

def f(x):
    P = 12 <= x <= 28
    Q = 15 <= x <= 30
    A = a1 <= x <= a2

    return (P <= A) and ((not Q) or A)

Ox = [x / 4 for x in range(11 * 4, 32 * 4)]

minLen = 1e6
for a1, a2 in permutations(Ox, 2):
    if all(f(x) for x in Ox):
        minLen = min(a2 - a1, minLen)

print(int(minLen))
