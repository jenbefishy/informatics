a = set()
b = {3, 5, 7, 11, 12, 15}
c = {5, 6, 12, 15}

def mulSet(a):
    res = 1
    for i in a:
        res *= i
    return res

def f(x):
    A = x in a
    B = x in b
    C = x in c

    return (B <= C) or A

for x in range(1000):
    if f(x) == 0:
        a.add(x)

print(a)
print(mulSet(a))
