
def f(x):
    return ((x % a != 0) and (x % 6 != 0)) <= (x % 3 != 0)

answer = 0
for a in range(2, 1000):
    if all(f(x) for x in range(1, 1000)):
        answer = a

print(answer)
        

