a = set()
b = {2, 4, 8, 12, 15}
c = {3, 6, 8, 15}

def mulSet(a):
    res = 1
    for i in a:
        res *= i
    return res

def f(x):
    A = x in a
    B = x in b
    C = x in c

    return (B <= (not C)) or A

for x in range(1000):
    if f(x) == 0:
        a.add(x)

print(a)
print(mulSet(a))
