a = set()
b = {1, 2, 3, 4, 5, 6}
c = {3, 5, 15}

def mulSet(a):
    res = 1
    for i in a:
        res *= i
    return res

def f(x):
    A = x in a
    B = x in b
    C = x in c

    return (not A) <= (((not B) and C) or (not C))

for x in range(1000):
    if f(x) == 0:
        a.add(x)

print(a)
print(len(a))
