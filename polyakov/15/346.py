
def f(k, n):
    return (7 * k + 2 * n > 17) or ((k < A) and (n <= A))


for A in range(1, 1000):
    if all(f(k, n) for k in range(1, 1000) for n in range(1, 1000)):
        print(A)
        break
