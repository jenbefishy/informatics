a = set()
p = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20}
q = {3, 6, 9, 12, 15, 18, 21, 24, 27, 30}


def f(x):
    A = x in a
    P = x in p
    Q = x in q

    return (A <= (not P)) and ((not Q) <= (not A))

for x in range(1000):
    a.add(x)

for x in range(1000):
    if f(x) == 0:
        a.remove(x)

print(a)
print(len(a))
