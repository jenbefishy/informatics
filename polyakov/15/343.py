
def f(x, y):
    return (50 > x) and (144 >= (4 * y - 3 * x)) \
            and (A ** 2 < ((x - 25) ** 2 + (y - 25) ** 2))


for A in range(0, 10000):
    isMatch = 1
    for x in range(0, 1000):
        for y in range(0, 1000):
            if f(x, y):
                isMatch = 0
                break
        if isMatch == 0:
            break

    if isMatch:
        print(A)
        break

