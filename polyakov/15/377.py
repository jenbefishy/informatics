
def f(x):
    return (((x % 12 == 0) or (x % 36 == 0)) <= (x % A == 0)) and (A **2 - A - 90 < 0)


answer = 0
for A in range(1, 1000):
    if all(f(x) for x in range(1, 1000)):
        answer = A

print(answer)
