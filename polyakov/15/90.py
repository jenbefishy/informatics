a = set()
b = {1, 2, 3, 4, 5, 6}
c = {3, 6, 9, 12, 15}

def f(x):
    A = x in a
    B = x in b
    C = x in c

    return (not B) or ((not C) <= A)

for x in range(1000):
    if f(x) == 0:
        a.add(x)

print(sum(a))
