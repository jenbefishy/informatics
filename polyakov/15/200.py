from itertools import permutations
    
def f(x):
    P = 8  <= x <= 16
    Q = 25 <= x <= 40
    A = a1 <= x <= a2

    return (P or Q) <= A

Ox = [x / 4 for x in range(7 * 4, 42 * 4)]

minLen = 1e6
for a1, a2 in permutations(Ox, 2):
    if all(f(x) for x in Ox):
        minLen = min(a2 - a1, minLen)

print(int(minLen))


