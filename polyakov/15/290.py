
def f(x, y):
    return (2 * y + 4 * x < A) or (x + 2 * y > 80)


for A in range(1, 1000):
    if all(f(x, y) for x in range(0, 1000) for y in range(0, 1000)):
        print(A)
        break
