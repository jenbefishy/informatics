from itertools import combinations

def f(x):
    P = 25 <= x <= 37
    Q = 32 <= x <= 47
    A = a1 <= x <= a2
    return (A and (not P)) <= ((not P) and Q)

Ox = [i / 4 for i in range(24 * 4, 48 * 4)]
lens = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        lens.append(a2 - a1)

print(max(lens))

