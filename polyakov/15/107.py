from itertools import combinations

def f(x):
    P = 12 <= x <= 26
    Q = 30 <= x <= 53
    A = a1 <= x <= a2

    return (A <= P) or Q

Ox = [i / 4 for i in range(11 * 4, 54 * 4)]
lens = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        lens.append(a2 - a1)

print(max(lens))
