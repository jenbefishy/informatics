
def f(x, y):
    return ((x <= 9) <= (x * x <= a)) and ((y * y <= a) <= (y <= 10))

answer = 0

for a in range(1000):
    if all(f(x, y) for x in range(1, 100) for y in range(1, 100)):
        answer = a

print(answer)
