from itertools import permutations

def f(x):
    D = 133 <= x <= 177
    B = 144 <= x <= 190
    A = a1 <= x <= a2

    return D <= (((not B) and (not A)) <= (not D))

Ox = [x / 4 for x in range(132 * 4, 192 * 4)]

minLen = 1e6

for a1, a2 in permutations(Ox, 2):
    if all(f(x) for x in Ox):
        minLen = min(a2 - a1, minLen)

print(round(minLen))
