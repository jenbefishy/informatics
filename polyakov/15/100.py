a = set()
b = {1, 3, 7}
c = {1, 2, 4, 5, 6}

def mulSet(a):
    res = 1
    for i in a:
        res *= i
    return res

def f(x):
    A = x in a
    B = x in b
    C = x in c

    return (not A) <= (not B) or (not C) and B

for x in range(1000):
    if f(x) == 0:
        a.add(x)

print(a)
print(len(a))
