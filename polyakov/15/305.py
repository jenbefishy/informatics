
def f(x):
    A = 30 <= x <= 50
    B = 40 <= x <= 46
    C = N <= x <= 61

    return ((not B) <= (not A)) and ((not C) <= B)


answer = 0
for N in range(1, 1000):
    count = 0
    for x in range(0, 100):
        if f(x):
            count += 1

    if count > 25:
        answer = N
    else:
        break


print(answer)
