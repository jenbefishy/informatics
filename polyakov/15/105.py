from itertools import combinations

def f(x):
    P = 44 <= x <= 49
    Q = 28 <= x <= 53
    A = a1 <= x <= a2

    return (A <= P) or Q

Ox = [i / 4 for i in range(27 * 4, 54 * 4)]
lens = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        lens.append(a2 - a1)

print(max(lens))
