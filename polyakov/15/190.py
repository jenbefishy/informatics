from itertools import permutations

def f(x):
    P = 12 <= x <= 24
    Q = 18 <= x <= 30
    A = a1 <= x <= a2

    return (not A) <= (P <= (not Q))

Ox = [x / 4 for x in range(11 * 4, 31 * 4)]

minLen = 1e6
start = 0
end = 0

for a1, a2 in permutations(Ox, 2):
    if all(f(x) for x in Ox):
        if a2 - a1 < minLen:
            minLen = a2 - a1
            start = a1
            end = a2

answer = 0
for i in range(int(start), int(end)):
    if i % 2 != 0:
        answer += 1



print(answer)
    
