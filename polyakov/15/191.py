from itertools import combinations

def f(x):
    A = a1 <= x <= a2
    P = 10 <= x <= 18
    Q = 8 <= x <= 30
    return (not A) <= (P <= (not Q))


Ox = [x / 4 for x in range(0 * 4, 50 * 4)]

answer = 10e10

for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        answer = min(a2 - a1, answer)

print(answer // 2)

