
def f(x):
    return (x & A == 0) <= ((x & 31 != 0) <= (x & 35 != 0))


answer = 0
for A in range(50, 121):
    if all(f(x) for x in range(1, 1000)):
        answer = A

print(answer)
