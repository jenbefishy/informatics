
def f(x):
    return (120 % A == 0) and (((x % 70 == 0) and (x % 30 == 0)) <= (x % A == 0))


answer = 0
for A in range(1, 1000):
    if all(f(x) for x in range(1, 1000)):
        answer = A

print(answer)
