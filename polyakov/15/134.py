p = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20}
q = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50}
a = {int(x) for x in range(1, 1000)}

def f(x):
    P = x in p
    Q = x in q
    A = x in a
    return (A <= P) or ((not Q) <= (not A))

for x in range(1, 1000):
    if f(x) == 0:
        a.remove(x)

print(a)
print(len(a))
