
def f(x, a):
    return (x % a != 0 and x % 6 == 0) <= (x % 3 != 0)

res = []
for a in range(1, 1000):
    if all(f(x, a) for x in range(1, 1000)):
        res.append(a)

print(max(res))

