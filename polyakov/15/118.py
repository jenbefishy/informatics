from itertools import combinations

def f(x):
    P = 3 <= x <= 15
    Q = 14 <= x <= 25
    A = a1 <= x <= a2
    return (P == Q) <= (not A)

Ox = [i / 4 for i in range(2 * 4, 26 * 4)]
lens = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        lens.append(a2 - a1)

print(round(max(lens)))

