
def f(x, y):
    return (y <= abs(x ** 2 - 4 * x - 5)) == ((y <= x ** 2 - 4 * x - 5) or \
            (y <= -(x - 2) ** 2 + A))


for A in range(0, 10000):
    if all(f(x, y) for x in range(1, 1000) for y in range(1, 1000)):
        print(A)
        break
