from itertools import combinations

def f(x):
    P = 15 <= x <= 39
    Q = 44 <= x <= 57
    A = a1 <= x <= a2

    return (A <= P) or Q

Ox = [i / 4 for i in range(14 * 4, 58 * 4)]
lens = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        lens.append(a2 - a1)

print(max(lens))
