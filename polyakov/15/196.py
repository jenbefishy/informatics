from itertools import permutations

def f(x):
    P = 21 <= x <= 25
    Q = 8  <= x <= 35
    A = a1 <= x <= a2

    return (P or (not Q)) <= (not A)

Ox = [x / 4 for x in range(7 * 4, 36 * 4)]

maxLen = 0
start = 0
end = 0

for a1, a2 in permutations(Ox, 2):
    if all(f(x) for x in Ox):
        if a2 - a1 > maxLen:
            maxLen = a2 - a1
            start = a1
            end = a2

start = round(start)
end = round(end)
answer = 0

for i in range(start, end):
    if i % 2 == 0:
        answer += 1


print(answer)
