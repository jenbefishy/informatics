from itertools import combinations

def f(x):
    P = 5  <= x <= 30
    Q = 14 <= x <= 23
    A = a1 <= x <= a2

    return (P == Q) <= (not A)

Ox = [i / 4 for i in range(4 * 4, 31 * 4)]
lens = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        lens.append(a2 - a1)

print(round(max(lens)))
