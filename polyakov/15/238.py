from itertools import combinations

def f(x):
    D = 155 <= x <= 177
    B = 111 <= x <= 160
    A = a1  <= x <= a2

    return (D <= (((not B) and (not A)) <= (not D)))


Ox = [x / 4 for x in range(120 * 4, 200 * 4)]

answer = 10e10
for a1, a2 in combinations(Ox, 2):
    if all(f(x) for x in Ox):
        answer = min(a2 - a1, answer)

print(round(answer))

