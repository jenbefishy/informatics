
def f(x, y):
    return (y + 2 * x < A) or (x > 20) or (y > 40)


for A in range(1, 1000):
    if all(f(x, y) for x in range(0, 1000) for y in range(0, 1000)):
        print(A)
        break
