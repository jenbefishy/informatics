
def mulSet(a):
    res = 1
    for i in a:
        res *= i
    return res

b = {2, 4, 9, 10, 15}
c = {3, 8, 9, 10, 20}
a = set()


def f(x):
    B = x in b
    C = x in c
    A = x in a
    return (not (B == A)) <= (C == A)


for x in range(0, 1000):
    if f(x) == 0:
        a.add(x)

print(a)
print(mulSet(a))
