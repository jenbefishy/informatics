
def f(x):
    return (x % 18 == 0) <= ((x % a != 0) <= (x % 12 != 0))

answer = 0
for a in range(2, 1000):
    if all(f(x) for x in range(1, 1000)):
        answer = a

print(answer)
        

