
def f(x, y):
    return ((y + 5 * x) < A) or ((3 * x + 2 * y) > 81)


answer = 0
for A in range(0, 1000):
    if all(f(x, y) for x in range(0, 1000) for y in range(0, 1000)):
        answer = A
        break

print(answer)

