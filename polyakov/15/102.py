a = set()
b = {1, 12}
c = {12, 13, 14, 15, 16}

def mulSet(a):
    res = 1
    for i in a:
        res *= i
    return res

def f(x):
    A = x in a
    B = x in b
    C = x in c

    return (not A) <= ((not B) and (not C))

for x in range(1000):
    if f(x) == 0:
        a.add(x)

print(a)
print(len(a))
