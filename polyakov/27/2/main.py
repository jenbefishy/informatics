
def solve(file):
    n = int(file.readline())
    lines = file.readlines()

    maxValues = []
    diffs1 = []
    diffs2 = []

    for i in lines:
        a, b = map(int, i.split())

        maxValues.append(max(a, b))
        diff = max(a, b) - min(a, b)
        if diff % 3 == 1:
            diffs1.append(diff)
        elif diff % 3 == 2:
            diffs2.append(diff)

    diffs1.sort()
    diffs2.sort()


    s = sum(maxValues)

    if s % 3 == 1:
        a = -10 ** 10
        b = -10 ** 10

        if len(diffs1) >= 1:
            a = s - diffs1[0]

        if len(diffs2) >= 2:
            b = s - diffs2[0] - diffs2[1]
        print(max(a, b))

    elif s % 3 == 2:
        a = -10 ** 10
        b = -10 ** 10

        if len(diffs1) >= 2:
            a = s - diffs1[0] - diffs1[1]

        if len(diffs2) >= 1:
            b = s - diffs2[0]

        print(max(a, b))

    else:
        print(s)





file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()


file = open("27-2a.txt")
print("A:", end="")
solve(file)
file.close()


file = open("27-2b.txt")
print("B:", end="")
solve(file)
file.close()
