

def solve(file):
    n = int(file.readline())

    k2 = 0
    k3 = 0
    k6 = 0
    k  = 0

    for i in file.readlines():
        number = int(i)

        if number % 6 == 0:
            k6 += 1
        elif number % 2 == 0:
            k2 += 1
        elif number % 3 == 0:
            k3 += 1
        else: 
            k += 1
    print(getAnswer(k, k2, k3, k6))



def getAnswer(k, k2, k3, k6):
    return k * k6 + k2 * k3 + k6 * k2 + k3 * k6 + (k6 * (k6 - 1)) // 2


file = open("27-12a.txt")
print("A:", end = "")
solve(file) 
file.close()

file = open("27-12b.txt")
print("B:", end = "")
solve(file)
file.close()
