def solve(file):
    n = int(file.readline())
    last = [0]
    for line in file.readlines():
        pair = list(map(int, line.split()))

        arr = []
        for i in last:
            for j in pair:
                arr.append(i + j)
        arr.sort(reverse=True)
        last = {x % 5 : x for x in arr}.values()

    answer = 10e9
    for i in last:
        if i % 5 == 0:
            answer = min(i, answer)
    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-5a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-5b.txt")
print("B:", end="")
solve(file)
file.close()
