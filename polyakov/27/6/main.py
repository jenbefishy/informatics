def solve(file):
    n = int(file.readline())

    data = {
            0: [],
            2: [],
            3: [],
            1: [],
            }

    for line in file.readlines():
        a = int(line)
        if a % 6 == 0:
            data[0] = data[0] + [a]
        elif a % 2 == 0:
            data[2] = data[2] + [a]
        elif a % 3 == 0:
            data[3] = data[3] + [a] 
        else:
            data[1] = data[1] + [a]

    for i in data.keys():
        data[i] = sorted(data[i])


    s1 = -10 ** 10
    s2 = -10 ** 10
    s3 = -10 ** 10
    if len(data[0]) >= 2:
        s1 = data[0][-1] * data[0][-2]
    if len(data[3]) >= 1 and len(data[2]) >= 1:
        s2 = data[3][-1] * data[2][-1]
    if len(data[1]) >= 1 and len(data[0]) >= 1:
        s3 = data[0][-1] * data[1][-1]
    print(max(s1, s2, s3))


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-6a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-6b.txt")
print("B:", end="")
solve(file)
file.close()
