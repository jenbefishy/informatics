
def solve(file):
    n = int(file.readline())

    last = [0]
    for line in file.readlines():
        trio = list(map(int, line.split()))

        arr = []
        for i in trio:
            for j in last:
                arr.append(i + j)

        arr.sort()
        last = {x % 8 : x for x in arr}.values()

    answer = 0
    for i in last:
        if i % 8 == 0:
            answer = max(answer, i)
    print(answer)

file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-11a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-11b.txt")
print("B:", end="")
solve(file)
file.close()
