
def solve(file):
    n = file.readline()
    answer = 0

    k13_2 = 0
    k13_1 = 0
    k_2 = 0
    k_1 = 0

    for i in file:
        a = int(i)
        if a % 2 == 0:
            if a % 13 == 0:
                answer += k_1
            else:
                answer += k13_1

            if a % 13 == 0:
                k13_2 += 1
            k_2 += 1
        else:
            if a % 13 == 0:
                answer += k_2
            else:
                answer += k13_2

            if a % 13 == 0:
                k13_1 += 1
            k_1 += 1


    
    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-16a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-16b.txt")
print("B:", end="")
solve(file)
file.close()
