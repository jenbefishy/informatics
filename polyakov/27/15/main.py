
def solve(file):
    n = int(file.readline())
    lines = file.readlines()
    queue = []

    answer = 0

    for i in range(5):
        queue.append(int(lines[i]))

    k = [0] * 14
    for i in range(5, len(lines)):
        a = int(lines[i])

        el = queue.pop(0)
        k[el % 14] += 1
            
        if a % 14 == 0:
            answer += k[0]
        else:
            answer += k[14 - a % 14]
        queue.append(a)

    print(answer)



file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-15a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-15b.txt")
print("B:", end="")
solve(file)
file.close()
