def solve(file):
    file.readline()
    last = [0]
    for line in file.readlines():
        pair = list(map(int, line.split()))
        arr = []
        for i in last:
            for j in pair:
                arr.append(i + j)
        arr.sort(reverse=True)
        last = {x % 10:x for x in arr}.values()

    answer = 0
    for i in last:
        if i % 10 == 4:
            answer = max(i, answer)
    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-22a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-22b.txt")
print("B:", end="")
solve(file)
file.close()
