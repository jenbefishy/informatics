
def solve(file):
    n = int(file.readline())
    last = [0]
    for line in file.readlines():
        trio = list(map(int, line.split()))

        arr = []
        for i in last:
            for j in trio:
                arr.append(i + j)

        arr.sort()
        last = {x % 4 : x for x in arr}.values()

    answer = 0
    for i in last:
        if i % 4 != 0:
            answer = max(i, answer)
    print(answer)



file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-10a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-10b.txt")
print("B:", end="")
solve(file)
file.close()
