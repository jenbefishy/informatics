
def solve(file):
    answer = 0
    n = int(file.readline())
    minValues = []
    diffs = []

    for i in file.readlines():
        a, b = map(int, i.split())
        minValues.append(min(a, b))

        diff = max(a, b) - min(a, b)
        if diff % 3 != 0:
            diffs.append(max(a, b) - min(a, b))

    minValues.sort()
    diffs.sort()


    if sum(minValues) % 3 == 0:
        print(sum(minValues) + diffs[0])
    else:
        print(sum(minValues))

file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()


file = open("27-1a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-1b.txt")
print("B:", end="")
solve(file)
file.close()
