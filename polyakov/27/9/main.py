def solve(file):
    n = int(file.readline())
    lines = file.readlines()
    queue = []

    minValue = 10e6
    answer = 10e6

    for i in range(6):
        queue.append(int(lines[i]))

    for i in range(6, n):
        el = queue.pop(0)

        if el % 2 != 0:
            minValue = min(el, minValue)

        a = int(lines[i])
        if a % 2 != 0:
            answer = min(a * minValue, answer)
        queue.append(a)

    if answer == 10e6:
        print(-1)
    else:
        print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-9a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-9b.txt")
print("B:", end="")
solve(file)
file.close()
