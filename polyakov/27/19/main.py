
def mulArr(arr):
    res = 1
    for i in arr:
        res *= i
    return res

def getAnswer(arr):
    answer = mulArr(arr)
    if mulArr(arr) > 0:
        return answer
    else:
        return max(getAnswer(arr[1:]), getAnswer(arr[:-1]))

def solve(file):
    n = int(file.readline())
    answer = 0
    arrs = []
    current = []

    for i in file.readlines():
        a = int(i)
        if a != 0:
            current.append(a)
        else:
            arrs.append(current)
            current = []

    arrs.append(current)

    for i in arrs:
        answer = max(answer, getAnswer(i))

    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-19a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-19b.txt")
print("Test:", end="")
solve(file)
file.close()

