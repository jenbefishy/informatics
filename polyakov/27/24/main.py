
def solve(file):
    file.readline()

    last = [0]
    for line in file.readlines():
        pair = list(map(int, line.split()))
        arr = []
        for i in pair:
            for j in last:
                arr.append(i + j)
        arr.sort(reverse=True)

        last = {x % 10: x for x in arr}.values()

    answer = 10e10
    for i in last:
        if i % 10 != 6:
            answer = min(i, answer)
    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-24a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-24b.txt")
print("B:", end="")
solve(file)
file.close()
