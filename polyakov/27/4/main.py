
def solve(file):
    n = int(file.readline())
    lines = file.readlines()

    last = [0]
    for line in lines:
        pair = list(map(int, line.split()))

        arr = []
        for i in last:
            for j in pair:
                arr.append(i + j)
        arr.sort()
        arr = {x % 5:x for x in arr}.values()
        last = arr

    answer = 0
    for i in last:
        if i % 5 == 0:
            answer = max(i, answer)
    print(answer)
                
                



        



file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-4a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-4b.txt")
print("B:", end="")
solve(file)
file.close()
