
def solve(file):
    file.readline()

    last = [0]
    for line in file.readlines():
        pair = list(map(int, line.split()))

        arr = []
        for i in pair:
            for j in last:
                arr.append(i + j)
        arr.sort()
        last = {x % 10: x for x in arr}.values()

    answer = 0
    for i in last:
        if i % 10 == 8:
            answer = max(answer, i)
    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-21a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-21b.txt")
print("B:", end="")
solve(file)
file.close()
