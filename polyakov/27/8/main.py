def solve(file):

    n = int(file.readline())
    lines = file.readlines()
    minValue = 10e10
    queue = []

    answer = 10e6
    for i in range(5):
        queue.append(int(lines[i]))

    for i in range(5, n):
        el = queue.pop(0)
        minValue = min(minValue, el)
        a = int(lines[i])

        answer = min(a ** 2 + minValue ** 2, answer)

        queue.append(a)



    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-8a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-8b.txt")
print("B:", end="")
solve(file)
file.close()

