
def solve(file):
    answer = 0
    n = int(file.readline())
    lines = file.readlines()

    k7 = []
    k  = []

    for line in lines:
        a = int(line)
        if a % 7 == 0 and a % 49 != 0:
            k7.append(a)
        elif a % 7 != 0:
            k.append(a)

    answer = 0
    answer = max(k7) * max(k)
    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-7a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-7b.txt")
print("B:", end="")
solve(file)
file.close()
