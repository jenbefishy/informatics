

def solve(file):
    k = 0
    k2 = 0
    k7 = 0
    k14 = 0

    queue = []

    answer = 0

    n = int(file.readline())
    lines = file.readlines()

    for i in range(7):
        queue.append(int(lines[i]))

    for i in range(7, len(lines)):
        a = int(lines[i])
        el = queue.pop(0)

        if el % 14 == 0:
            k14 += 1
        if el % 7 == 0:
            k7 += 1
        if el % 2 == 0:
            k2 += 1
        k += 1


        if a % 14 == 0:
            answer += k 
        elif a % 7 == 0:
            answer += k2
        elif a % 2 == 0:
            answer += k7
        else:
            answer += k14

        queue.append(a)

    print(answer) 



file = open("27-13a.txt")
print("A:", end="")
solve(file)

fileB = open("27-13b.txt")
print("B:", end="")
solve(fileB)

