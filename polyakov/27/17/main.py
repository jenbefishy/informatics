
def solve(file):
    n = int(file.readline())
    lines = file.readlines()

    queue = []
    answer = 0

    for i in range(5):
        queue.append(int(lines[i]))

    k13_2 = 0
    k13_1 = 0
    k_2 = 0
    k_1 = 0

    for i in range(5, len(lines)):
        el = queue.pop(0)

        if el % 2 == 0:
            if el % 13 == 0:
                k13_2 += 1
            k_2 += 1
        else:
            if el % 13 == 0:
                k13_1 += 1
            k_1 += 1

        a = int(lines[i])

        if a % 2 == 0:
            if a % 13 == 0:
                answer += k_1
            else:
                answer += k13_1
        else:
            if a % 13 == 0:
                answer += k_2
            else:
                answer += k13_2

        queue.append(a)




    print(answer)


file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-17a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-17b.txt")
print("B:", end="")
solve(file)
file.close()
