
def solve(file):
    n = int(file.readline())

    k = [0] * 13

    answer = 0
    for i in file.readlines():
        a = int(i)
        if a % 12 == 0:
            answer += k[0]
        else:
            answer += k[12 - a % 12]
        k[a % 12] += 1

    print(answer)



file = open("27.txt")
print("Test:", end="")
solve(file)
file.close()

file = open("27-14a.txt")
print("A:", end="")
solve(file)
file.close()

file = open("27-14b.txt")
print("B:", end="")
solve(file)
file.close()
