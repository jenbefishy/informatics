from itertools import product, permutations


def f(x, y, z, w):
    return ((not y) and (z <= ((not x) and w)))


for a in product([0, 1], repeat=6):
    table = [
            (1, a[0], a[1], 1),
            (a[2], a[3], 1, a[4]),
            (0, 0, 0, a[5])
            ]

    F = [1, 1, 0]
    if len(table) != len(set(table)):
        continue

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
            exit(0)

