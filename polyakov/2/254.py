from itertools import product, permutations

def f(x, y, z, w):
    return (w and ((z or y) == (z and x)))

for a in product([0, 1], repeat=5):
    table = [
            (0, a[0], 0, a[1]),
            (a[2], a[3], 1, 0),
            (0, 1, a[4], 0)
            ]

    F = [1, 1, 0]

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)

