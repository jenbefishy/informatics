from itertools import permutations

def f(a, b, c, d):
    return ((a <= b) and (c <= d) or (not c))

table = [(1, 0, 1, 0),
         (0, 0, 1, 1),
         (0, 1, 1, 1),
         (1, 0, 1, 1)]

F = [0, 0, 0, 0]

for i in permutations("abcd"):
    if [f(**dict(zip(i, row))) for row in table] == F:
        print(i)

    
