from itertools import product, permutations

def f(x, y, z):
    return ((not (z or x)) or y and (not x) and (z or y <= z))

for a in product([0, 1], repeat=5):
    table = [(a[0], a[1], a[2]),
             (1, a[3], 1),
             (1, a[4], 0)]

    F = [1, 1, 1]

    if len(table) != len(set(table)):
        continue

    for i in permutations("xyz"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
