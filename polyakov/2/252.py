from itertools import product, permutations


def f(x, y, z, w):
    return (((z <= w) <= (not y)) and x)


for a in product([0, 1], repeat=5):
    table = [
            (0, 0, 1, a[0]),
            (a[1], a[2], 0, a[3]),
            (0, a[4], 1, 1)
            ]
    F = [1, 1, 0]

    if len(table) != len(set(table)):
        continue

    for i in permutations("xyzw"):
        if [f(**dict(zip(i, row))) for row in table] == F:
            print(i)
            exit(0)


