
def f(n):
	if n > 46:
		return 0
	if n == 46:
		return 1

	newN = 0

	for i in str(n):
		if i == "9":
			newN = newN * 10 + 9
		else:
			newN = newN * 10 + int(i) + 1
	return f(n + 1) + f(newN)

print(f(24))
