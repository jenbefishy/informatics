
def func(n):
	if n > 55:
		return 0
	if n == 55:
		return 1
	return func(n + 1) + func(n * 4)

print(func(1))
