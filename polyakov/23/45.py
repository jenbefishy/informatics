
def f(n, e):
	if n > e:
		return 0
	if n == e:
		return 1
	return f(n + 1, e) + f(n * 2, e) + f(n + 3, e)

print(f(4, 10) * f(10, 20))
