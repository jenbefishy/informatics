
end = 22

def f(n):
	if n > end:
		return 0
	if n == end:
		return 1
	
	if n % 2 != 0:
		return f(n + 1)
	else:
		return f(n + 1) + f(n * 1.5)

print(f(2))
