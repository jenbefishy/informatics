
def f(n):
	if n > 27:
		return 0
	if n == 27:
		return 1
	return f(n + 1) + f(n + 10)

print(f(11))
