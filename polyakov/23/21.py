
def f(n):
	if n > 51:
		return 0
	if n == 51:
		return 1
	
	newN = 0
	for i in str(n):
		if i == "9":
			newN = newN * 10 + int(i)
		else:
			newN = newN * 10 + int(i) + 1
	return f(n + 1) + f(newN)



print(f(25))
