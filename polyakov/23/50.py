
def f(n, e):
	if n > e:
		return 0
	if n == e:
		return 1
	return f(n + 1, e) + f(n * 2, e)

print(f(3, 18) + f(3, 9))
