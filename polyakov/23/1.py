
def func(n):
	if n > 16:
		return 0
	if n == 16:
		return 1
	return func(n + 1) + func(n * 2)

print(func(1))
