
file = open("26-k4.txt")
n, k = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]
data.sort(reverse=True)

count = 0
s1 = 0
s2 = 0

while count < k:
    s1 += data.pop(0)
    count += 1

count = 0
while count < k:
    s2 += data.pop(0)
    count += 1

print(int(s2 / k), int(s1 / k))
