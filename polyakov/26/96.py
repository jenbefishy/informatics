
file = open("26-96.txt")
n = int(file.readline())
data = {}

for line in file.readlines():
    w, d = map(int, line.split())

    if d in data:
        data[d] = data[d] + [w]
    else:
        data[d] = [w]

count = 0
maxD = 0
answer = 0


for key in sorted(data.keys()):
    if len(data[key]) >= count:
        count = len(data[key])
        maxD = key
        answer = 0
        s = set()
        for i in data[key]:
            if -90 <= int(i / 10) <= 90:
                s.add(int((i) / 10))
        answer = len(s)


print(maxD, answer)
