
file = open("26-k6.txt")
n, k = map(int, file.readline().split())

data = {}

for line in file.readlines():
    weight, price = map(int, line.split())
    val = weight / price

    if val in data:
        tmp = data[val] + [weight]
        tmp.sort(reverse=True)
        data[val] = tmp
    else:
        data[val] = [weight]

keys = list(data.keys())
keys.sort(reverse=True)

count = 0
summary = 0

maxWeight = 0
answer = 0

for key in keys:
    for i in data[key]:

        if maxWeight < i:
            maxWeight = i
            answer = i / key

        summary += i
        count += 1

        if count == k:
            print(summary, int(answer))
            exit(0)
