file = open("26-99.txt")
n, m = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]
data.sort(reverse=True)

filled = []
count = 0
while len(data):
    count += 1
    s = 0
    while len(data) and (s + data[0] <= m):
        el = data.pop(0)
        s += el

    for i in range(len(data)):
        if s + data[i] <= m:
            s += data[i]
            data[i] = -1

    while -1 in data:
        data.remove(-1)
        
    filled.append(s)

print(count, filled[-2])
