
file = open("26-j9.txt")
s, n = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]

data.sort()

used = 0
count = 0
last = 0

while used < s:
    isFound = False
    if count % 2 == 0:
        for i in range(-1, -len(data), -1):
            if used + data[i] <= s:
                last = data[i]
                used += data.pop(i)
                count += 1
                isFound = True
                break
    else:
        for i in range(0, len(data)):
            if used + data[i] <= s:
                last = data[i]
                used += data.pop(i)
                count += 1
                isFound = True
                break
    if not isFound:
        break

print(count, last)
