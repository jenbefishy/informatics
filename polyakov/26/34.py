
file = open("26-j6.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]

data.sort(reverse=True)

s = sum(data) * 0.9
used = []

while sum(data) + sum(used) * 0.8 > s:
    el = data.pop(0)
    used.append(el)

el = data.pop(0)
answer = max(data)
for i in used:
    if (sum(used) - i + el) * 0.8 + sum(data) + i <= s:
        answer = max(i, answer)

print(n - len(used), answer)
