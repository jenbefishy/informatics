
file = open("26-J3.txt")
s, n = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]
data.sort(reverse=True)

used = 0
answer = 0
count = 0


while used + data[0] <= s:
    el = data.pop(0)
    answer = el
    used += el
    count += 1

avaliable = s - used
for i in data:
    if i <= avaliable:
        avaliable -= i
        answer = i
        count += 1

print(count, answer)
