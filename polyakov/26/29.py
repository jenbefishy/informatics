
file = open("26-j4.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]
data.sort()

for _ in range(n // 10):
    data.pop(0)

data.sort(reverse=True)
for _ in range(n // 10):
    data.pop(0)


total = sum(data)
answer = data[0]

print(total, answer)
