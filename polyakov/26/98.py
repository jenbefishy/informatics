
file = open("26-98.txt")
n, s = map(int, file.readline().split())


data = []
for line in file.readlines():
    price, category = line.split()
    price = int(price)

    if len(category) == 2:
        if category[0] == "A":
            price *= 0.9
        else:
            price *= 0.8

        data.append([price, 1])
    else:
        data.append([price, 0])

data.sort()

bought = 0
count = 0
answer = 0

while bought + data[0][0] <= s:
    el = data.pop(0)

    if el[1] == 1:
        answer = el[0]
        
    bought += el[0]
    count += 1

print(int(bought), end=" ")

avaliable = s - (bought - el[0])
data.sort(reverse=True)

for i in data:
    if i[0] <= avaliable:
        print(int(i[0]))
        exit(0)
    
