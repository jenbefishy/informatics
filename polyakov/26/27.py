
file = open("26-J2.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]
data.sort()

average = sum(data) / len(data)
median = data[(len(data) + 1) // 2 - 1]

left = min(average, median)
right = max(average, median)

count = 0
for i in data:
    if left <= i <= right:
        count += 1

print(count)
