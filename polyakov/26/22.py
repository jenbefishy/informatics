
file = open("26-k2.txt")
n, k = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]
data.sort()

for _ in range(k):
    data.pop(0)

data.sort(reverse=True)

for _ in range(k):
    data.pop(0)

print(max(data), int(sum(data)/len(data)))
