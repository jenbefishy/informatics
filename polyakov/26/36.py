
file = open("26-j8.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]

data.sort()
tmp = [int(x) for x in data]

s1 = 0
s2 = 0
answer = 0

count = 0
while count < int(n * 0.7):
    s1 += data.pop(0) * 0.7
    count += 1
s1 += sum(data) * 0.6
answer = max(data) * 0.6


data = tmp

count = 0
while count < int(n * 0.5):
    s2 += data.pop(0) * 0.6
    count += 1

s2 += sum(data) * 0.65

if s1 > s2:
    print(int(s1 - s2), int(answer))
else:
    print(int(s2 - s1), int(max(data) * 0.65))
