
file = open("26-2.txt")
s, n = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]

data.sort()

used = 0
answer = 0
count = 0
while used + data[0] <= s:
    el = data.pop(0)
    used += el
    answer = el
    count += 1

avaliable = s - (used - el)
for i in data:
    if i <= avaliable:
        answer = i
    else:
        break

print(count, answer)
