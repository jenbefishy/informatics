file = open("26-1.txt")

s, n = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]

data.sort()

count = 0
used = 0
answer = 0
while used + data[0] <= s:
    el = data.pop(0)
    used += el
    answer = el
    count += 1

avaliable = s - (used - el)

for i in data:
    used -= el
    if i <= avaliable:
        answer = max(i, answer)
    else:
        break

print(count, answer)
