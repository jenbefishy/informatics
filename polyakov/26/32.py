
file = open("26-s1.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]

data.sort()

s = 0
while data[0] <= 150:
    s += data.pop(0)

data.sort(reverse=True)
answer = 0
while data:
    fullPrice = data.pop(0)

    if data:
        discount = data.pop(-1)
        answer = discount
    else:
        discount = 0

    s += fullPrice + discount * 0.8

print(round(s), round(answer))
