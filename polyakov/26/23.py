
file = open("26-k3.txt")
n, k, m = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]
data.sort(reverse=True)

count = 0
ans1 = 10e10
ans2 = 10e10
while count < k:
    ans2 = data.pop(0)
    count += 1

count = 0
while count < m:
    ans1 = data.pop(0)
    count += 1

print(ans1, ans2)
