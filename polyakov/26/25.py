
file = open("26-k5.txt")
n, k, m = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]
data.sort(reverse=True)

ans1 = 10e10
ans2 = 0

count = 0
while count < m:
    ans1 = data.pop(0)
    count += 1

data.sort()
count = 0
while count < k:
    ans2 += data.pop(0)
    count += 1

print(ans1, int(ans2 / k))
