
file = open("26-J5.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]

res = [data[0]]

diff = 0
for i in range(1, n - 1):
    arr = [data[i - 1], data[i], data[i + 1]]
    arr.sort()
    res += [arr[1]]

res += [data[-1]]


for i in range(n):
    if data[i] < res[i]:
        continue
    diff += data[i] - res[i]


print(res.count(min(res)), diff)
