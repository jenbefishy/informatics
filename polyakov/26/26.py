
file = open("26-J1.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]

count = 0
for i in range(len(data)):
    for j in range(i + 1, len(data)):
        if data[i] + data[j] == 100:
            count += 1
            data[i] = -1
            data[j] = -1


print(count)
