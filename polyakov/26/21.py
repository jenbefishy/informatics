
file = open("26-k1.txt")

n, k = map(int, file.readline().split())
data = [int(x) for x in file.readlines()]
data.sort(reverse=True)

s = 0
answer = 0
for i in range(k):
    s += data[i] * 0.2

print(data[k], int(s))
