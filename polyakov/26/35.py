

file = open("26-j7.txt")
n = int(file.readline())
data = [int(x) for x in file.readlines()]


data.sort(reverse=True)

total = sum(data) * 0.6
s = 0

count = 0
while count < int(n * 0.2):
    s += data.pop(0) * 0.8
    count += 1


percent = 0.01
while s + sum(data) * percent < total:
    percent += 0.01

print(int(s), int(min(data)  * percent))
