
data = [int(x) for x in open("17-4.txt")]

answer = 0
maxNumber = 0

for i in data:
	if i % 3 == 0 and i % 7 != 0 and i % 17 != 0 \
	and i % 19 != 0 and i % 27 != 0:
		answer += 1
		maxNumber = max(maxNumber, i)
		
		

print(answer, maxNumber)
	
