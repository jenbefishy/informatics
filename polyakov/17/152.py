
data = [int(x) for x in open("17-1.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for x, y in zip(data[0:], data[1:]):
	if (abs(x) % 9 == 0 and abs(y) % 9 != 0 and toBase(abs(y), 8)[-1] == "3") \
	or (abs(y) % 9 == 0 and abs(x) % 9 != 0 and toBase(abs(x), 8)[-1] == "3"):
		answer += 1
		minNumber = min(minNumber, x, y)
		maxNumber = max(maxNumber, x, y)

print(answer, maxNumber)
	
