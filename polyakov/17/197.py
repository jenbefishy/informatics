
data = [int(x) for x in open("17-10.txt")]

answer = 0
minSum = 1e6
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res

for x, y in zip(data[0:], data[1:]):
	if len(str(x + y)) == 3 and (x + y) % 10 > (x + y) // 10 % 10:
		answer += 1
		minSum = min(minSum, x + y)

print(answer, minSum)
