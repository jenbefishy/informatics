
data = [int(x) for x in open("17-8.txt")]

answer = 0
minNumber = 0
maxNumber = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for x, y in zip(data[0:], data[1:]):
	a = toBase(x, 2).count("1")
	b = toBase(y, 2).count("1")
	if (a > 5 and a % 2 != 0) or (b > 5 and b % 2 != 0):
		answer += 1
		maxNumber = max(maxNumber, x + y)

print(answer, maxNumber)
