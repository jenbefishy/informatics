
data = [int(x) for x in open("17-205.txt")]

count = 0
maxSum = -1e6



for a, b in zip(data[0:], data[1:]):
	if abs(a - b) % 37 == 0 and abs(a - b) % 2 == 0:
        	count += 1
        	maxSum = max(maxSum, a + b)

print(count, maxSum)
