
data = [int(x) for x in open("17-257.txt")]

minValue = 10e10
maxValue = 0

for i in data:
    if i % 10 == 4:
        minValue = min(minValue, i)
        maxValue = max(maxValue, i)


count = 0
answer = 0

for a, b in zip(data, data[1:]):
    if a + b < minValue + maxValue:
        count += 1
        answer = max(answer, a + b)

print(count, answer)
