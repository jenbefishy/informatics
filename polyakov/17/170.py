
data = [int(x) for x in open("17-4.txt")]

answer = 0
maxNumber = 0

for i in data:
	if i % 13 == 4 and i % 8 == 1:
		maxNumber = max(maxNumber, i)
		answer += i
		
		

print(maxNumber, answer)
	
