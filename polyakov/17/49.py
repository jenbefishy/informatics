
answer = 0
minNumber = 1e6
maxNumber = -1e6

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in range(127, 9852 + 1):
	if len(toBase(i, 8)) == len(str(i)) and i % 3 == 0 and i % 9 != 0:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, maxNumber)
