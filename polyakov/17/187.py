
data = [int(x) for x in open("17-6.txt")]

answer = 0
minSum = 1e6
maxSum = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res


for x, y, z in zip(data[0:], data[1:], data[2:]):
	if toBase(x, 2).count("1") == 3 and toBase(y, 2).count("1") == 3 \
	and toBase(z, 2).count("1") == 3:
		answer += 1
		maxSum += max(x, y, z)
		

print(answer, maxSum)
