
answer = 0
number = 1e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(1000, 9999 + 1):
	if len(toBase(i, 6)) <= 5 and \
	(toBase(i, 6).endswith("13") or toBase(i, 6).endswith("14")):
		answer += 1
		number = i
		
		
	
print(answer, number)
