from fnmatch import fnmatch
data = [int(x) for x in open("17-346.txt")]

count = 0
maxResult = 0
numbers = []

def mulDigits(n):
    mul = 1
    while n:
        if n % 2 == 0:
            mul *= n % 10
        n //= 10
    return mul

for i in data:
    numbers.append(i)

    if len(numbers) == 4:
        numbers.pop(0)
        res = mulDigits(numbers[0]) \
            * mulDigits(numbers[1]) \
            * mulDigits(numbers[2])

        if res < 2_000_000_000 and fnmatch(str(res), "25*2*"):
            count += 1
            maxResult = max(res, maxResult)
    


print(count, maxResult)
