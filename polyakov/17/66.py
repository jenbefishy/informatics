
answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

def mulNumbers(n):
	res = 1
	for i in str(n):
		res *= int(i)
	return res

for i in range(333666, 666999 + 1):
	if i % 17 == 0 and str(i).count("7") == 2:
		answer += 1
		maxNumber = max(maxNumber, i)
		minNumber = min(minNumber, i)
		sum += i
		
	
print(maxNumber, answer)
