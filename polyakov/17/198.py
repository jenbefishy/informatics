
data = [int(x) for x in open("17-10.txt")]

answer = 0
minSum = 1e6
maxSum = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res

def isMatch(n):
	n = str(n)
	mid = (len(n) + 1) // 2
	if len(n) % 2 == 0 and n[0:mid] == n[mid:][::-1]:
		return 1
		
	if len(n) % 2 != 0 and n[0:mid - 1] == n[mid:][::-1]:
		return 1
	return 0
	
for x, y in zip(data[0:], data[1:]):
	if isMatch(toBase(x + y, 7)):
		answer += 1
		maxSum = max(x + y, maxSum)

print(answer, toBase(maxSum, 7))
