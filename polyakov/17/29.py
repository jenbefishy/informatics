
answer = 0
number = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(2807, 8558 + 1):
	if toBase(i, 2).endswith("11") and toBase(i, 9)[-1] == "5":
		answer += i
		number = max(number, i)
		
		
	
print(number, answer)
