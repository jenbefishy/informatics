
answer = 0
number = 1e6

for i in range(1107, 9504 + 1):
	if i % 9 == 0 and i % 7 != 0 and i % 15 != 0 and i % 17 != 0 and i % 19 != 0:
		answer += 1
		number = min(number, i)
		
	
print(answer, number)
