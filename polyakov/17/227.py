
data = [int(x) for x in open("17-1.txt")]

count = 0
minSum = 1e6
maxSum = -1e6

average = sum(data) / len(data)


for a, b, c in zip(data[0:], data[1:], data[2:]):
	if (a > average or b > average or c > average):
		count += 1
		minSum = min(minSum, a + b + c)
		maxSum = max(maxSum, a + b + c)

print(count, maxSum)
