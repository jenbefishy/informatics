
data = [int(x) for x in open("17-3.txt")]

answer = 0
maxSum = 0
		
for x, y, z in zip(data[0:], data[1:], data[2:]):
	if (x * y * z) % 7 == 0 and str(x + y + z)[-1] == "5":
		answer += 1
		maxSum = max(maxSum, x + y + z)
	

print(answer, maxSum)
	
