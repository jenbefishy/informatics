from fnmatch import fnmatch
numbers = [int(x) for x in open("17-346.txt")]

count = 0
maxResult = 0

def mulDigits(n):
    mul = 1
    while n:
        if n % 2 == 0:
            mul *= n % 10
        n //= 10
    return mul


for a, b, c in zip(numbers, numbers[1:], numbers[2:]):

    res = mulDigits(a) * mulDigits(b) * mulDigits(c)
    if res < 2_000_000_000 and fnmatch(str(res), "11*6*"):
        count += 1
        maxResult = max(res, maxResult)

print(count, maxResult)
