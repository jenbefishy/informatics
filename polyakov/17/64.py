
answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

for i in range(4563, 7912 + 1):
	if i % 7 == 0 and int(str(i)[0]) + int(str(i)[-1]) > 10:
		answer += 1
		maxNumber = max(maxNumber, i)
		minNumber = min(minNumber, i)
		sum += i
		
	
print(maxNumber, answer)
