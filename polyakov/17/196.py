
data = [int(x) for x in open("17-10.txt")]

answer = 0
maxNumber = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for x, y, z in zip(data[0:], data[1:], data[2:]):
	a = min(x, y, z)
	c = max(x, y, z)
	b = x + y + z - a - c
	if a ** 2 + b ** 2 == c ** 2:
		answer += 1
		sum += c
	

print(answer, sum)
