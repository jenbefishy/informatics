
data = [int(x) for x in open("17-4.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base	
	return res

def getDel(n):
	k = 2
	res = []
	while k * k < n + 1:
		if n % k == 0:
			res.append(k)
		k += 1
	return res

for i in data:
	dels = getDel(i)
	count = 0
	for j in dels:
		if j in [2, 3, 5, 7]:
			count += 1

	if count == 2:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		sum += i

print(answer, minNumber + maxNumber)
