
data = [int(x) for x in open("17-7.txt")]

answer = 0
minSum = 0
maxSum = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res


for x, y, z in zip(data[0:], data[1:], data[2:]):
	count = 0
	if toBase(x, 3).endswith("2"):
		count += 1

	if toBase(y, 3).endswith("2"):
		count += 1

	if toBase(z, 3).endswith("2"):
		count += 1


	if count >= 1:
		answer += 1
		minSum += min(x, y, z)
		

print(answer, minSum)
