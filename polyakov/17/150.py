
data = [int(x) for x in open("17-1.txt")]

answer = 0
minSum = 1e6

for x, y in zip(data[0:], data[1:]):
	if (x % 7 == 0 and y % 17 != 0) or (x % 17 != 0 and y % 7 == 0):
		answer += 1
		minSum = min(minSum, x + y)

print(answer, minSum)
	
