
data = [int(x) for x in open("17-3.txt")]

answer = 0
minSum = 1e6
		
for x, y, z, d in zip(data[0:], data[1:], data[2:], data[3:]):
	arr = [x, y, z, d]
	arr.sort(reverse=True)
	if arr == [x, y, z, d] and x - d > 1000:
		answer += 1
		minSum = min(minSum, x + y + z + d)
		
	

print(answer, minSum)
	
