
data = [int(x) for x in open("17-1.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for x, y in zip(data[0:], data[1:]):
	if y > x:
		answer += 1
		minNumber = min(minNumber, abs(y - x))

print(answer, minNumber)
	
