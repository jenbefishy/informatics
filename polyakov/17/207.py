
data = [int(x) for x in open("17-205.txt")]

count = 0
maxSum = -1e6



for a, b in zip(data[0:], data[1:]):
	if (abs(a) % 10 == 7 or abs(b) % 10 == 7) and abs(a + b) % 12 == 0:
        	count += 1
        	maxSum = max(maxSum, a + b)

print(count, maxSum)
