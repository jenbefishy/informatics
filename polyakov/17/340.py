
data = [ int(x) for x in open("17-340.txt")]

count = 0
maxResult = 0

average = 0
for i in data:
    if i % 22 == 0:
        average += i
        count += 1

average /= count
count = 0

def isMatch(n):
    sn = oct(n)[2:]
    return sn.index(max(sn)) < sn.index(min(sn))


for a, b in zip(data, data[1:]):
    if isMatch(a) and isMatch(b) and (a + b < average):
        count += 1
        maxResult = max(a + b, maxResult)

print(count, maxResult)

