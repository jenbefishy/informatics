
data = [int(x) for x in open("17-3.txt")]

answer = 0
minSum = 1e6
		
for x, y, z in zip(data[0:], data[1:], data[2:]):
	if (abs(x) % 12 == 0  or abs(y) % 12 == 0 or abs(z) % 12 == 0) \
	and (abs(x) % 3 == 0 and abs(y) % 3 == 0 and abs(z) % 3 == 0):
		answer += 1
		minSum = min(minSum, (x + y + z) // 3)
	

print(answer, minSum)
	
