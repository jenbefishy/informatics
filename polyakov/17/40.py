
answer = 0
minNumber = 1e6
maxNumber = 0

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

for i in range(1871, 9197 + 1):
	if (i % 9 == 2 or i % 9 == 4) and len(toBase(i, 16)) != len(str(i)):
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, minNumber)
