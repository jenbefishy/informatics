
answer = 0
minNumber = 1e6
maxNumber = -1e6
mul = 1

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in range(2055, 9414 + 1):
	if sumOfNumbers(i % 100) != 5 and i % 4 != 0 and i % 5 != 0 and i % 41 != 0:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		mul *= i
		
		
	
print(minNumber, mul % 1000)
