
data = [int(x) for x in open("17-1.txt")]

count = 0
minSum = 1e6
maxSum = -1e6

average = sum(data) / len(data)


for a, b in zip(data[0:], data[1:]):
	if (a < average or b < average) and (\
	(a % 3 == 0 and a % 5 != 0 and a % 11 != 0 and a % 19 != 0) or \
	(b % 3 == 0 and b % 5 != 0 and b % 11 != 0 and b % 19 != 0)):
		count += 1
		minSum = min(minSum, a + b)
		maxSum = max(maxSum, a + b)

print(count, maxSum)
