
answer = 0
minNumber = 1e6
maxNumber = 0

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

for i in range(2371, 9432 + 1):
	if (toBase(i, 8).endswith("15") or toBase(i, 8).endswith("17")) and i % 3 != 0 and i % 5 != 0:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, maxNumber)
