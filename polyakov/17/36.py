
answer = 0
minNumber = 1e6
maxNumber = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(2476, 7857 + 1):
	if i % 2 == 0 and i % 8 != 0 and (i // 100) % 10 <= 7:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, (minNumber + maxNumber) // 2)
