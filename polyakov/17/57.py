
answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in range(1346, 7996 + 1):
	if i % 3 == 0 and i % 13 != 0:
		answer += 1
		maxNumber = max(maxNumber, i)
		minNumber = min(minNumber, i)
		sum += i
		
	
print(answer, sum)
