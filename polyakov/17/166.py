
data = [int(x) for x in open("17-3.txt")]

answer = 0
minDiff = 1e6
		
for x, y, z in zip(data[0:], data[1:], data[2:]):
	if max(x, y, z) == z and min(x, y, z) == x:
		answer += 1
		minDiff = min(minDiff, z - x)
		
	

print(answer, minDiff)
	
