
def getSum(n):
    res = 0
    for i in str(abs(n)):
        res += int(i)
    return res

data = [int(x) for x in open("17-272.txt")]

average = 0
count = 0
for i in data:
    if i > 0:
        average += i
        count += 1

average /= count

count = 0
answer = 0

for a, b in zip(data, data[1:]):
    if (a > average or b > average):
        count += 1
        answer = max(answer, getSum(a), getSum(b))

print(count, answer)
