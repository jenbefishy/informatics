
data = [int(x) for x in open("17-5.txt")]

answer = 0
minSum = 1e6
maxSum = -1e6
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res


for x, y in zip(data[0:], data[1:]):
	if (abs(x) % 10 == 5 and abs(y) % 10 == 5):
		answer += 1
		maxSum = max(maxSum, x + y)
		

print(answer, maxSum)
