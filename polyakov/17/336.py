
data = [ int(x) for x in open("17-336.txt")]

count = 0
minResult = 1e9
maxNumber = 0
minNumber = 1e9

for i in data:
    if i % 8 == 0 and i != 8:
        minNumber = min(i, minNumber)


def isMatch(n):
    sn = oct(n)[2:]
    return sn.index(max(sn)) < sn.index(min(sn))


for a, b in zip(data, data[1:]):
    if a % minNumber == 0 and b % minNumber == 0:
        count += 1
        if a + b < minResult:
            minResult = a + b
            maxNumber = max(a, b)

            

print(count, maxNumber)

