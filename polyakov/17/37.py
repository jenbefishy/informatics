
answer = 0
minNumber = 1e6
maxNumber = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(3905, 7998 + 1):
	if (i // 10) % 10 not in [0,5] and 2 <= (i // 100) % 10 <= 6:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, minNumber)
