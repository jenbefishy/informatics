
data = [int(x) for x in open("17-9.txt")]

answer = 0
maxNumber = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for x, y, z in zip(data[0:], data[1:], data[2:]):
	
	count = 0
	a = toBase(x, 2)
	b = toBase(y, 2)
	c = toBase(z, 2)

	if a.count("1") >= 3 and "0" in a:
		count += 1

	if b.count("1") >= 3 and "0" in b:
		count += 1
	
	if c.count("1") >= 3 and "0" in c:
		count += 1

	if count >= 2:
		answer += 1
		maxNumber = max(maxNumber, x, y, z)

print(answer, maxNumber)
