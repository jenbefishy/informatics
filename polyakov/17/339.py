
data = [ int(x) for x in open("17-339.txt")]

count = 0
maxResult = 0

minNumber = 1e9
for i in data:
    if i > 0 and i % 19 == 0:
        minNumber = min(minNumber, i)



def isMatch(n):
    sn = oct(n)[2:]
    return sn.index(max(sn)) < sn.index(min(sn))


for a, b in zip(data, data[1:]):
    if a + b < minNumber:
        maxResult = max(a + b, maxResult)
        count += 1

print(count, maxResult)

