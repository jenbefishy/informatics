
data = [int(x) for x in open("17-4.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res

for i in data:
	if toBase(i, 16).endswith("B") and i % 7 == 0 \
	and i % 6 != 0 and i % 13 != 0 and i % 19 != 0:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		sum += i

print(sum, answer)
