
data = [int(x) for x in open("17-3.txt")]

answer = 0
maxSum = 0
		
for x, y in zip(data[0:], data[1:]):
	if (x % 2 == 0 and y % 2 != 0 and x % 4 == 0 and y % 11 == 0) \
	or (x % 2 != 0 and y % 2 == 0 and y % 4 == 0 and x % 11 == 0):
		answer += 1
		maxSum = max(maxSum, x + y)
	

print(answer, maxSum)
	
