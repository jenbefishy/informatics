
answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in range(10, 1178 + 1, 2):
	if i % 10 not in [0, 2, 6, 8] and not(str(i).endswith("14")):
		answer += 1
		maxNumber = max(maxNumber, i)
		minNumber = min(minNumber, i)
		sum += i
		
	
print(sum, minNumber)
