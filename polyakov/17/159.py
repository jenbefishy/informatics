
data = [int(x) for x in open("17-3.txt")]

answer = 0
maxSum = 0 
		
for x, y in zip(data[0:], data[1:]):
	if (x + y) % 3 == 0 and (x + y) % 6 != 0 and str(x * y)[-1] == "8":
		answer += 1
		maxSum = max(maxSum, x + y) 
	

print(answer, maxSum)
	
