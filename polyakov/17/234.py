
data = [int(x) for x in open("17-1.txt")]

average = sum(data) / len(data)

answer = 0
count  = 0

for a, b, c in zip(data[0:], data[1:], data[2:]):
    if (a < average or b < average or c < average) and \
       ("9" in str(a) and "9" in str(b) and "9" in str(c)):
        count += 1
        answer = max(a + b + c, answer)

print(count, answer)
        
    
