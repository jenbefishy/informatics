
answer = 0
number = 1e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(1000, 9999 + 1):
	if len(toBase(i, 5)) >= 6 and \
	(toBase(i, 5).endswith("21") or toBase(i, 5).endswith("23")):
		answer += 1
		number = min(number, i)
		
		
	
print(answer, number)
