from fnmatch import fnmatch
data = [int(x) for x in open("17-342.txt")]

count = 0
minResult = 1e9
minNumber = 1e9
maxNumber = 0

for i in data:
    if i % 37 == 0:
        minNumber = min(i, minNumber)

    if i % 73 == 0:
        maxNumber = max(i, maxNumber)


minNumber, maxNumber = min(minNumber, maxNumber), max(minNumber, maxNumber)

for a, b in zip(data, data[1:]):
    if (minNumber < a < maxNumber and (b <= minNumber or b >= maxNumber)) or \
        (minNumber < b < maxNumber and (a <= minNumber or a >= maxNumber)):
        count += 1
        minResult = min(a + b, minResult)
        

print(count, minResult)
