from fnmatch import fnmatch
data = [int(x) for x in open("17-345.txt")]

count = 0
maxNumber = 0
minNumber = 1e9
maxResult = 0

for i in data:
    if str(i).endswith("52"):
        maxNumber = max(i, maxNumber)
        minNumber = min(i, minNumber)

diff = maxNumber - minNumber

for a, b in zip(data, data[1:]):
    if (a < diff and b >= diff) or (b < diff and a >= diff):
        count += 1
        maxResult = max(maxResult, a + b)
        

print(count, maxResult)
