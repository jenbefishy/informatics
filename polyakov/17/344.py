from fnmatch import fnmatch
data = [int(x) for x in open("17-344.txt")]

count = 0
minNumber = 1e9
maxResult = 0

for i in data:
    if i % 103 == 0:
        minNumber = min(i, minNumber)


for a, b in zip(data, data[1:]):
    if (a + b) % 2 == 0 and abs(a - b) % minNumber == 0:
        count += 1
        maxResult = max(a + b, maxResult)
        

print(count, maxResult)
