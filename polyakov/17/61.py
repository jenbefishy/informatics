
answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

for i in range(2738, 7514 + 1):
	if i % 7 == 0 and i % 19 != 0:
		answer += 1
		maxNumber = max(maxNumber, i)
		minNumber = min(minNumber, i)
		sum += i
		
	
print(answer, sum)
