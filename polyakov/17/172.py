
data = [int(x) for x in open("17-4.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base	
	return res

for i in data:
	if toBase(i, 2).endswith("1001") and toBase(i, 5).endswith("11"):
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		sum += i

print(maxNumber, sum)
