
data = [ int(x) for x in open("17-335.txt")]

count = 0
maxResult = 0
minNumber = 1e9

for i in data:
    if i % 43 == 0:
        minNumber = min(i, minNumber)


def isMatch(a, b):
    s1 = (a + b) % minNumber == 0
    s2 = str(a)[-1] == str(minNumber)[-1]
    s3 = str(b)[-1] == str(minNumber)[-1]

    return (s1 and not (s2 or s3)) or (not s1 and (s2 or s3))
    

for a, b in zip(data, data[1:]):
    if isMatch(a, b):
        count += 1
        maxResult = max(maxResult, a, b,)
        

            

print(count, maxResult)

