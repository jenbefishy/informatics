from string import ascii_uppercase

numbers = "0123456789" + ascii_uppercase

def toBase(x, base):
    res = ""
    while x:
        res = numbers[x % base] + res
        x //= base
    return res


data = [int(x) for x in open("17-243.txt")]

s = 0

for i in data:
    if i % 35 == 0:
        for j in str(i):
            s += int(j)


count = 0
answer = 10e10
for a, b in zip(data, data[1:]):
    isMatch = 0

    if a > s and b <= s and toBase(b, 16).endswith("EF"):
        count += 1
        answer = min(a + b, answer)

    elif b > s and a <= s and toBase(a, 16).endswith("EF"):
        count += 1
        answer = min(a + b, answer)

print(count, answer)
    
