
data = [int(x) for x in open("17-1.txt")]

count = 0
maxSum = -1e6

average = sum(data) / len(data)


for a, b in zip(data[0:], data[1:]):
	if (a > average or b > average):
        	count += 1
        	maxSum = max(maxSum, a + b)

print(count, maxSum)
