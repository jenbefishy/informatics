
data = [ int(x) for x in open("17-341.txt")]

count = 0
maxResult = 0

average = sum(data) / len(data)
    

for a, b, c, d in zip(data, data[1:], data[2:], data[3:]):
    if b * c > a * d and (b > average or c > average):
        count += 1
        maxResult = max(maxResult, b + c)

    

print(maxResult, count)

