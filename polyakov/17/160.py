
data = [int(x) for x in open("17-3.txt")]

answer = 0
minMul = 1e6 
		
for x, y in zip(data[0:], data[1:]):
	if (x * y) > 0 and (x + y) % 7 == 0:
		answer += 1
		minMul = min(minMul, x * y)
	

print(answer, minMul)
	
