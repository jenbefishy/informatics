
data = [int(x) for x in open("17-1.txt")]

answer = 0
minNumber = 1e6

for x, y in zip(data[0:], data[1:]):
	if (abs(x) % 3 == 0 and abs(x) % 10 == 6) or (abs(y) % 3 == 0 and abs(y) % 10 == 6):
		answer += 1
		minNumber = min(minNumber, x, y)

print(answer, minNumber)
	
