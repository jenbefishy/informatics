from fnmatch import fnmatch
data = [int(x) for x in open("17-343.txt")]

count = 0
minResult = 1e9

def isMatch(n):
    s1 = 0
    s2 = 0
    current = 0
    while n:
        if current % 2 == 0:
            s1 += n % 10
        else:
            s2 += n % 10
        current += 1
        n //= 10
        
    return s1 and (s2 % s1 == 0)



for a, b, c  in zip(data, data[1:], data[2:]):
    if isMatch(a) and isMatch(b) and isMatch(c):
        count += 1
        minResult = min(a + b + c, minResult)
        

print(count, minResult)
