
answer = 0
minNumber = 1e6
maxNumber = -1e6

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in range(int("1000000", 7), int("10000000", 7)):
	if toBase(i, 3)[-1] == "2" and toBase(i, 8)[-1] != "3" and toBase(i, 12)[-1] != "5":
		answer += 1
		maxNumber = i
		
	
print(answer, maxNumber)
