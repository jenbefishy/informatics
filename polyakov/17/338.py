
data = [ int(x) for x in open("17-338.txt")]

count = 0
maxResult = 0

minNumber = min(data)

def isMatch(n):
    sn = oct(n)[2:]
    return sn.index(max(sn)) < sn.index(min(sn))


for a, b in zip(data, data[1:]):
    if a % 117 == minNumber or b % 117 == minNumber:
        maxResult = max(a + b, maxResult)
        count += 1

print(count, maxResult)

