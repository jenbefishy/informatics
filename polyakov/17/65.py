
answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

def mulNumbers(n):
	res = 1
	for i in str(n):
		res *= int(i)
	return res

for i in range(8800, 55535 + 1):
	if mulNumbers(i) > 35 and "7" in str(i):
		answer += 1
		maxNumber = max(maxNumber, i)
		minNumber = min(minNumber, i)
		sum += i
		
	
print(maxNumber, answer)
