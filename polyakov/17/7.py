
answer = 0
number = 1e6

for i in range(200, 9120 + 1):
	if i % 8 == 0 and i % 7 != 0 and i % 11 != 0 and i % 17 != 0 and i % 19 != 0:
		answer += 1
		number = min(number, i)
		
	
print(answer, number)
