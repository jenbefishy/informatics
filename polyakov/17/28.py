
answer = 0
number = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(3439, 7410 + 1):
	if toBase(i, 2)[-1] != toBase(i, 6)[-1] and \
	(i % 9 == 0 or i % 10 == 0 or i % 11 == 0):
		answer += 1
		number = max(number, i)
		
		
	
print(answer, number)
