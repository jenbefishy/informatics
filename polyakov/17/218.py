
data = [int(x) for x in open("17-4.txt")]

count = 0
minSum = 1e6
maxSum = -1e6

average = sum(data) / len(data)


for a, b in zip(data[0:], data[1:]):
	if (a < average and b < average) and (abs(a) % 100 == 19 or abs(b) % 100 == 19):
		count += 1
		minSum = min(minSum, a + b)
		maxSum = max(maxSum, a + b)

print(count, maxSum)
