
answer = 0
minNumber = 1e6
maxNumber = -1e6
mul = 1

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in range(1985, 8528 + 1):
	if sumOfNumbers(i % 100) == 6 and i % 2 != 0 and i % 7 != 0 and i % 47 != 0:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		mul *= i
		
		
	
print(maxNumber, mul % 1000)
