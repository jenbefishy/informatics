
data = [int(x) for x in open("17-1.txt")]

count = 0
minSum = 1e6
maxSum = -1e6

average = sum(data) / len(data)


for a, b in zip(data[0:], data[1:]):
	if a > average and b > average and abs(a + b) % 100 == 31:
		count += 1
		minSum = min(minSum, a + b)
		maxSum = max(maxSum, a + b)

print(count, maxSum)
