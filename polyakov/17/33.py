
answer = 0
minNumber = 1e6
maxNumber = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(1000, 9999 + 1):
	if i % 5 != 0 and i % 7 != 0 and i % 11 != 0 and len(toBase(i, 3)) == 8:
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(minNumber, maxNumber)
