
data = [int(x) for x in open("17-3.txt")]

answer = 0
minSum = 1e6
		
for x, y in zip(data[0:], data[1:]):
	if (x * y) % 2 != 0 and ((x + y) // 2) % 7 == 0:
		answer += 1
		minSum = min(minSum, (x + y) // 2)
	

print(answer, minSum)
	
