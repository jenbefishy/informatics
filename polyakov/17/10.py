
answer = 0
number = 1e6

for i in range(1098, 13765 + 1):
	if i % 2 == 0 and i % 7 != 0 and i % 11 != 0 and i % 13 != 0 and i % 23 != 0:
		answer += 1
		number = min(number, i)
		
	
print(answer, number)
