
answer = 0
number = 10e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(3712, 8432 + 1):
	if toBase(i, 2)[-1] == toBase(i, 4)[-1] and \
	(i % 13 == 0 or i % 14 == 0 or i % 15 == 0):
		answer += 1
		number = min(number, i)
		
		
	
print(answer, number)
