
answer = 0
minNumber = 1e6
maxNumber = 0

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

for i in range(2495, 7083 + 1):
	if (toBase(i, 16).endswith("1A") or toBase(i, 16).endswith("1F")) and i % 5 != 0 and i % 9 != 0:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, minNumber)
