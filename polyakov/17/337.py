
data = [ int(x) for x in open("17-336.txt")]

count = 0
minResult = 1e9
maxNumber = 0

for i in data:
    if i % 37 == 0:
        maxNumber = max(i, maxNumber)



def isMatch(n):
    sn = oct(n)[2:]
    return sn.index(max(sn)) < sn.index(min(sn))


for a, b in zip(data, data[1:]):
    if (a % maxNumber == 0 or b % maxNumber == 0) and (a + b % maxNumber > 30):
        count += 1
        minResult = min(a + b, minResult)

print(count, minResult)

