
data = [int(x) for x in open("17-243.txt")]

s = 0
for i in data:
    if i % 51 == 0:
        for j in str(i):
            s += int(j)

count = 0
answer = 0
for a, b in zip(data, data[1:]):
    if (a < s or b < s):
        count += 1
        answer = max(a + b, answer)

print(count, answer)
