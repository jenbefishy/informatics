
data = [int(x) for x in open("17-243.txt")]

maxValue = 0
for i in data:
    if i % 71 == 0:
        maxValue = max(i, maxValue)

answer = 10e10
count = 0

for a, b in zip(data, data[1:]):
    if (a < maxValue and b < maxValue) and (a * b % 13 == 0):
        count += 1
        answer = min(answer, a + b)

print(count, answer)
        

