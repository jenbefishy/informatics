
data = [int(x) for x in open("17-4.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base	
	return res

for i in data:
	if i > 100 and (i // 10) % 10 <= 4 and 3 <= (i // 100) % 10 <= 7:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		sum += i

print(answer, minNumber)
