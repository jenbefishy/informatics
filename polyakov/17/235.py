
data = [int(x) for x in open("17-1.txt")]

average = sum(data) / len(data)
count = 0
answer = 0

for a, b, c in zip(data[0:], data[1:], data[2:]):
    isMatch = 0
    if a > average:
        isMatch += 1

    if b > average:
        isMatch += 1

    if c > average:
        isMatch += 1

    if isMatch >= 2:
        count += 1
        answer = max(answer, a + b + c)


print(count, answer)
        
