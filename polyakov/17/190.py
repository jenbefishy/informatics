
data = [int(x) for x in open("17-7.txt")]

answer = 0
minNumber = 0
maxNumber = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res


for i in data:
	if toBase(i, 8).endswith("7") and not toBase(i, 8).endswith("27"):
		answer += 1
		maxNumber = max(maxNumber, i)
print(answer, maxNumber)
