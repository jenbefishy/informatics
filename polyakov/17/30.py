
answer = 0
number = 1e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(1529, 9482 + 1):
	if toBase(i, 2).endswith("01") and toBase(i, 5)[-1] == "3":
		answer += i
		number = min(number, i)
		
		
	
print(number, answer)
