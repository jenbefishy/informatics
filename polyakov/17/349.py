from fnmatch import fnmatch
data = [int(x) for x in open("17-346.txt")]

count = 0
maxResult = 0
numbers = []

def mulDigits(n):
    mul = 1
    while n:
        mul *= n % 10
        n //= 10
    return mul

for a, b, c in zip(data, data[1:], data[2:]):
    res = mulDigits(a) * mulDigits(b) * mulDigits(c)
    if res < 2_000_000_000 and fnmatch(str(res), "83*8*"):
        count += 1
        maxResult = max(res, maxResult)

print(count, maxResult)
