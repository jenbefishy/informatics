
data = [int(x) for x in open("17-243.txt")]

maxValue = 0
for i in data:
    if i % 171 == 0:
        maxValue = max(maxValue, i)
    

answer = 10e10
count = 0

for a, b in zip(data, data[1:]):
    if (a > maxValue or b > maxValue) and \
       ("11" in str(a) or "11" in str(b)):
        count += 1
        answer = min(answer, a + b)

print(count, answer)
