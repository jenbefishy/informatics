
data = [int(x) for x in open("17-4.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6
sum = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base	
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in data:
	if sumOfNumbers(i % 100) >= 15 and i % 3 != 0 \
	and i % 4 != 0 and i % 7 != 0:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		sum += i

print(minNumber, sum)
