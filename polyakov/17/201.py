
data = [int(x) for x in open("17-199.txt")]

count = 0
maxSum = -1e6


def isMatch(a):
    return a > 0 and len(str(a)) == 3 and a % 2 != 0

for a, b, c in zip(data[0:], data[1:], data[2:]):
    if isMatch(b) and not isMatch(a) and not isMatch(c):
        count += 1
        maxSum = max(maxSum, a + b + c)

print(count, maxSum)
