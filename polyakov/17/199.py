
data = [int(x) for x in open("17-199.txt")]

answer = 0
maxSum = -1e6
sum = 0

def isMatch(n):
	return n > 0 and len(str(n)) == 2 and n % 2 == 0

for x, y, z in zip(data[0:], data[1:], data[2:]):
	if isMatch(y) and not isMatch(x) and not isMatch(z):
		answer += 1
		maxSum = max(maxSum, x + y + z)

print(answer, maxSum)
