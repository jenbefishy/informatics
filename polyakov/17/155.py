
data = [int(x) for x in open("17-1.txt")]

answer = 0
minNumber = 1e6
maxNumber = -1e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for x, y, z in zip(data[0:], data[1:], data[2:]):
	if min(x, y, z) == y:
		answer += 1
		maxNumber = max(maxNumber, y)
		
		

print(answer, maxNumber)
	
