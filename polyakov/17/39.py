
answer = 0
minNumber = 1e6
maxNumber = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(3466, 9081 + 1):
	if (i % 7 == 1 or i % 7 == 5) and len(toBase(i, 8)) != len(str(i)):
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, maxNumber)
