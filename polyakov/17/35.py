
answer = 0
minNumber = 1e6
maxNumber = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(1476, 7039 + 1):
	if i % 2 == 0 and i % 16 != 0 and (i // 10) % 10 >= 4:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, (minNumber + maxNumber) // 2)
