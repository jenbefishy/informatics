
data = [int(x) for x in open("17-4.txt")]

answer = 0
minNumber = 1e6

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base	
	return res

for i in data:
	if toBase(i, 3)[-1] == toBase(i, 5)[-1] \
	and (i % 31 == 0 or i % 47 == 0 or i % 53 == 0):
		answer += 1
		minNumber = min(minNumber, i)

print(answer, minNumber)
