
answer = 0
minNumber = 1e6
maxNumber = 0

for i in range(3232, 8299 + 1):
	if (i % 2 == 0 or i % 7 == 0) and i % 15 != 0 and i % 28 != 0 and i % 41 != 0:
		answer += 1
		minNumber = min(i, minNumber)
		maxNumber = max(i, maxNumber)
		
		
	
print(minNumber, maxNumber)
