
data = [int(x) for x in open("17-3.txt")]

answer = 0
maxSum = 0
		
for x, y, z, d in zip(data[0:], data[1:], data[2:], data[3:]):
	if (x % 2 != y % 2 and y % 2 != z % 2 and z % 2 != d % 2):
		answer  += 1
		maxSum = max(maxSum, x + y + z + d)
		
	

print(answer, maxSum)
	
