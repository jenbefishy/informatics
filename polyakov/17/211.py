
data = [int(x) for x in open("17-4.txt")]

count = 0
minSum = 1e6

average = sum(data) / len(data)


for a, b in zip(data[0:], data[1:]):
	if (a > average or b > average) and abs(a + b) % 7 == 0:
        	count += 1
        	minSum = min(minSum, a + b)

print(count, minSum)
