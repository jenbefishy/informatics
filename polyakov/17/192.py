
data = [int(x) for x in open("17-7.txt")]

answer = 0
minNumber = 0
maxNumber = 0
sum = 0

numbers = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base	
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res


for i in data:
	if sumOfNumbers(i) % 3 == 0:
		answer += 1
		maxNumber = max(maxNumber, i)
print(answer, maxNumber)
