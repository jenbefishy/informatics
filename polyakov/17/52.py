
answer = 0
minNumber = 1e6
maxNumber = -1e6

numbers="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def toBase(n, base):
	res = ""
	while n:
		res = numbers[n % base] + res
		n //= base
	return res

def sumOfNumbers(n):
	res = 0
	for i in str(n):
		res += int(i)
	return res

for i in range(1000, 70000 + 1):
	if len(toBase(i, 8)) == 5 and len(toBase(i, 5)) == 6 and toBase(i, 16).endswith("FA"):
		answer += 1
		maxNumber = max(maxNumber, i)
		minNumber = min(minNumber, i)
		
	
print(answer, maxNumber)
