
answer = 0
minNumber = 1e6
maxNumber = 0

def toBase(n, base):
	res = ""
	while n:
		res = str(n % base) + res
		n //= base
	return res

for i in range(2461, 9719 + 1):
	if (i // 100) % 10 not in [1,9] and 3 <= (i // 10) % 10 <= 7:
		answer += 1
		minNumber = min(minNumber, i)
		maxNumber = max(maxNumber, i)
		
		
	
print(answer, maxNumber)
