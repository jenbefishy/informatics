
data = [int(x) for x in open("17-271.txt")]

average = 0

for i in data:
    average += i
average /= len(data)
    

count = 0
answer = -10e10

for a, b in zip(data, data[1:]):
    if abs(a) % 10 + abs(b) % 10 == 7:
        count += 1

        if a < average and b < average:
            answer = max(a + b, answer)

print(count, answer)
